<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ChatAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Playfair+Display%7COpen+Sans:400,600',
        '/themes/agency/css/fonts.css',
        '/themes/agency/css/style.css',
        '/css/chat.css',
        '/css/main.css',
    ];
    public $js = [
        '/themes/agency/js/core.min.js',
        '/themes/agency/js/script.js',
        '/js/Client.js',
        '//www.gstatic.com/firebasejs/3.6.8/firebase.js',
        '/js/firebase_subscribe.js'
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\cdn\AssetBundle',
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
