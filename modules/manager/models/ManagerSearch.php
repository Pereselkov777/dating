<?php

namespace app\modules\manager\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\manager\models\Manager;
//use yii\db\Query;

/**
 * ManagerSearch represents the model behind the search form of `app\modules\manager\models\Manager`.
 */
class ManagerSearch extends Manager
{   

    public $first_name;
    public $facebook_link;
    public $public_email;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'agency_id'], 'integer'],
            [['price'], 'number'],
            [['about','first_name','facebook_link','public_email',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   
        $query = Manager::find()
        ->select('manager.*, profile.first_name, profile.facebook_link,
         profile.public_email')
        ->innerJoin('profile', '`profile`.`user_id` = `manager`.`id`')
        ->with('profile');
        
               //->with('profile')->where(['id'=>'profile.user_id'])->one();

                
        
        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        //exit();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        /*var_dump($dataProvider);
        exit();*/

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'agency_id' => $this->agency_id,
            'price' => $this->price,
            //'profile.first_name' => $this->profile->first_name,
            //'fist_name'=>$this->first_name,
            
        ]);



        $query->andFilterWhere(['like', 'about', $this->about]);
        $query->andFilterWhere(['LIKE', 'first_name', $this->first_name]);
        $query->andFilterWhere(['LIKE', 'facebook_link', $this->facebook_link]);
        $query->andFilterWhere(['LIKE', 'public_email', $this->public_email]);

        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        //exit();

        return $dataProvider;
    }
}
