<?php

namespace app\modules\manager\models;

use Yii;
use app\models\Profile;

/**
 * This is the model class for table "manager".
 *
 * @property int $id ID
 * @property int $agency_id Агенство
 * @property float $price Цена
 * @property string|null $about Описание
 * @property Profile $profile 
 */
class Manager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agency_id', 'price', 'name'], 'required'],
            ['name', 'unique'],
            [['agency_id'], 'integer'],
            [['price'], 'number'],
            [['about'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'agency_id' => Yii::t('app', 'Агенство'),
            'price' => Yii::t('app', 'Цена'),
            'about' => Yii::t('app', 'Описание'),
        ];
    }

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    
}
