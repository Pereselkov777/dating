<?php

namespace app\modules\manager\controllers;


use Yii;
use app\models\Profile;
use app\models\ProfileSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

class ReferralController extends \yii\web\Controller
{
    public function actionIndex($id)

    {   
        
        $searchModel = new ProfileSearch();
        $searchModel->manager_id = Yii::$app->user->id;
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$user = Profile::findOne(Yii:: $app->user->id);
        //$query = Profile::find()->where(['agency_id' => $user->agency_id]);
        //$q='SELECT photo, user_id FROM profile WHERE agency_id = ' . $user->agency_id . ' AND LENGTH (photo)';
        //$data = Yii::$app->db->createCommand($q)->queryAll();
        //$sql='SELECT * FROM photo INNER JOIN profile ON profile.agency_id=' .(int)$user->agency_id .  ' AND profile.sex=0 WHERE photo.user_id=profile.user_id';
        
        //var_dump($_GET["ProfileSearch"]['user_id']);
       
        if(isset($_GET["ProfileSearch"])
         && isset($_GET["ProfileSearch"]['user_id'])
         ){
            //1.)user_id = string title = string(o)

           if(array_key_exists('manager_id', $_GET["ProfileSearch"])){
            $manager_id=$_GET["ProfileSearch"]['manager_id'];
            $query = Profile::find()->where(['agency_id' => (int)$id, 'sex'=>2, 'manager_id'=>(int)$manager_id]);
            //var_dump($query);
            //exit();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'user_id' => SORT_DESC,
                    ]
                ],
            ]);
            //var_dump($dataProvider);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            
           }
            $girl_id = $_GET["ProfileSearch"]['user_id'];
           
            //var_dump($girl_id);
            $query = Profile::find()->where(['agency_id' => (int)$id, 'sex'=>2, 'user_id'=>(int)$girl_id]);
            //var_dump($query);
            //exit();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'user_id' => SORT_DESC,
                    ]
                ],
            ]);
            //var_dump($dataProvider);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            //exit();
        }
        //var_dump($_GET);
        //exit();
      
        //echo('3');
        //exit();
        
        $query = Profile::find()->where(['agency_id' => (int)$id, 'sex'=>2,'manager_id' =>Yii::$app->user->id])->andWhere(['not', ['manager_id' =>0]]);
    
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'user_id' => SORT_DESC,
                ]
            ],
        ]);

        //var_dump($dataProvider);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {   
       //var_dump($this->view);exit();
       return $this->redirect(['/manager/referal-view/index/', 'id' => $id]);
       
    }

}
