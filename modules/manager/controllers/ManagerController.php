<?php

namespace app\modules\manager\controllers;

use Yii;
use app\modules\manager\models\Manager;
use app\modules\manager\models\ManagerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dektrium\user\models\User;
use dektrium\user\traits\EventTrait;
use dektrium\user\Finder;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\Profile;
use yii\web\ForbiddenHttpException;


/**
 * ManagerController implements the CRUD actions for Manager model.
 */
class ManagerController extends Controller

{   
    use EventTrait;

    /**
     * Event is triggered before creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CREATE = 'beforeCreate';

    /**
     * Event is triggered after creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * Event is triggered before updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';

    /**
     * Event is triggered after updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';

    /**
     * Event is triggered before impersonating as another user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_IMPERSONATE = 'beforeImpersonate';

    /**
     * Event is triggered after impersonating as another user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_IMPERSONATE = 'afterImpersonate';

    /**
     * Event is triggered before updating existing user's profile.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_PROFILE_UPDATE = 'beforeProfileUpdate';

    /**
     * Event is triggered after updating existing user's profile.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_PROFILE_UPDATE = 'afterProfileUpdate';

    /**
     * Event is triggered before confirming existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CONFIRM = 'beforeConfirm';

    /**
     * Event is triggered after confirming existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CONFIRM = 'afterConfirm';

    /**
     * Event is triggered before deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';

    /**
     * Event is triggered after deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_DELETE = 'afterDelete';

    /**
     * Event is triggered before blocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_BLOCK = 'beforeBlock';

    /**
     * Event is triggered after blocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_BLOCK = 'afterBlock';

    /**
     * Event is triggered before unblocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UNBLOCK = 'beforeUnblock';

    /**
     * Event is triggered after unblocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UNBLOCK = 'afterUnblock';

    /**
     * Name of the session key in which the original user id is saved
     * when using the impersonate user function.
     * Used inside actionSwitch().
     */
    const ORIGINAL_USER_SESSION_KEY = 'original_user';

    /** @var Finder */
    protected $finder;

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Manager models.
     * @return mixed
     */
    public function actionIndex($id)
    {   
        Yii :: $app-> session->set('agency_id', $id);
        $searchModel = new ManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $q = 'SELECT COUNT(user_id) FROM profile WHERE profile.manager_id =' .(int) Yii::$app->user->id . '  ';
        $my_girl_count = Yii::$app->db->createCommand($q)->queryScalar();
        $q = 'SELECT COUNT(user_id) FROM profile';
        $total_user_count = Yii::$app->db->createCommand($q)->queryScalar();

       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'my_girl_count'=>$my_girl_count,
            'total_user_count'=> $total_user_count,

            
        ]);
    }

    /**
     * Displays a single Manager model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Manager model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()


    {   

        if(Yii:: $app->user->can('administrator')){
            $agency_id = Yii::$app->session->get('agency_id');
            $user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
            ]);
    
            $event = $this->getUserEvent($user);
    
            $this->performAjaxValidation($user);
            $this->trigger(self::EVENT_BEFORE_CREATE, $event);
            if ($user->load(\Yii::$app->request->post()) && $user->create()) {
                //var_damp($user); // user created
                //exit();
                $this->trigger(self::EVENT_AFTER_CREATE, $event);
                $model = Profile::findOne($user->id);
                if($model){
                    //var_damp($user); 
                    //var_damp($model);//пустой Profile
                    //exit();
                    $model->sex = 1;
                    $model->agency_id=$agency_id;
                    $model->kind_id = 3;
                    //$q = 'INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('Manager', '40', NULL)';
                    
                    $q = 'INSERT IGNORE INTO auth_assignment SET item_name = "Manager",
                     user_id=' . (int)$user->id . '';
                     //var_dump($q);
                     //exit();
                    Yii::$app->db->createCommand($q)->execute();
    
    
    
                    if($model->save()){
                        \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User && Profile has been created'));
                        $model = new Manager();
                        $model->id=$user->id;
                        $model->agency_id=$agency_id;
                        //$model->name="Your name";
                       // echo('Profile записан');
                        //exit();
                        if ($model->save(false)) {
                            //echo('Manager');//Обновляем созданный Manager
                            return $this->redirect(['update', 'id' => $model->id]);
                        }
                    }
                    
                }       
            }
            return $this->render('create', [
                    'user' => $user,
                ]);

        }else{
            throw new  ForbiddenHttpException('Access denied');

        }
       
        
        

       
    }

    /**
     * Updates an existing Manager model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)

    {  
        $model = $this->findModel($id);
        if(Yii::$app->user->can('administrator')||(int)$model->id === (int)Yii::$app->user->id){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if(!$model->name){
                    //var_dump($model->first_name);exit();
                    \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'Please choose your name'));
                    return $this->redirect(['/update', 'id' => $model->id]);
                    //return false;
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
    
            return $this->render('update', [
                'model' => $model,
            ]);
        } else{
            throw new  ForbiddenHttpException('Access denied');

        }
       
    }

    /**
     * Deletes an existing Manager model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Manager model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Manager the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Manager::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && !\Yii::$app->request->isPjax) {
            if ($model->load(\Yii::$app->request->post())) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                \Yii::$app->response->data = json_encode(ActiveForm::validate($model));
                \Yii::$app->end();
            }
        }
    }
}
