<?php

namespace app\modules\manager\controllers;
use Yii;
use yii\web\ForbiddenHttpException;
use app\models\Profile;

class GirlController extends \yii\web\Controller
{
    public function actionGetGirl()
    {
        if(Yii:: $app->user->can('administrator')||Yii:: $app->user->can('Manager') ){
            
            $data = Yii::$app->request->post();
            if(isset($data)){
                $a = Yii::$app->session->get('agency_id');
                    if($a){
                    $user_id =(int)$data['user_id'];
                    $model = Profile::findOne(['user_id' => (int)$user_id]);
                    $model->agency_id=((int)$a);
                    $model->manager_id = Yii::$app->user->id;
                    $model->kind_id = (int)3;
                    $model->save();
                    //Сюда добавить вставку
                    $q = 'INSERT IGNORE INTO manager_vs_girl SET manager_id = ' .(int) Yii::$app->user->id . ',
                    girl_id=' .(int) $user_id . '';
                    Yii::$app->db->createCommand($q)->execute();
                    //\Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Role has been added'));
                    //return $this->render('index');

                    return $this->redirect(['/agency/profile/index', 'id' => $a]);
                }else{
                    throw new  ForbiddenHttpException('Access denied');

                }
                
            }
            
        }else{
            throw new  ForbiddenHttpException('Access denied');
        };
    
        return $this->render('index');
    }

    public function actionDropGirl($id)
    {   
        $model = Profile::findOne(['user_id' => (int)$id]);
        if(Yii:: $app->user->can('administrator')||(int)Yii:: $app->user->id === (int)$model->manager_id){
            if(Yii::$app->request->post()){
                    $a = Yii::$app->session->get('agency_id');
                    if($a){
                    $user_id =(int)$id;
                    
                    $q = 'DELETE FROM `manager_vs_girl` WHERE
                    `manager_vs_girl`.`manager_id` = ' .(int) Yii::$app->user->id . ' AND
                    `manager_vs_girl`.`girl_id` = ' .(int) $id . '
                     ';
                     //var_dump($q);exit();
                    Yii::$app->db->createCommand($q)->execute();
                    $model->agency_id=(int)0;
                    $model->manager_id = (int)0;
                    $model->save();
                    return $this->redirect(['/agency/profile/index', 'id' => $a]);    
                    }else{
                        throw new  ForbiddenHttpException('Access denied');

                    }
                    
                
                

            }
         
        }else{
            throw new  ForbiddenHttpException('Access denied');
        };
    
        return $this->render('index');
    }

}
