<?php

namespace app\modules\manager\controllers;

use yii\web\ForbiddenHttpException;
use Yii;
use app\models\Profile;

class AddRoleController extends \yii\web\Controller
{
    public function actionIndex()
    {   
        if(Yii:: $app->user->can('administrator')){
            
            $data = Yii::$app->request->post();
            if(isset($data)){
                $a = Yii::$app->session->get('agency_id');
                $user_id =(int)$data['user_id'];
                $q = 'INSERT IGNORE INTO auth_assignment SET item_name = "Manager",
                user_id=' .(int) $user_id . '';
            Yii::$app->db->createCommand($q)->execute();
            $q = 'INSERT INTO manager SET id=' . (int) $user_id . ', agency_id = ' . $a .
            '';
            Yii::$app->db->createCommand($q)->execute();
            $model = Profile::findOne(['user_id' => (int)$user_id]);
           
            $model->is_consultant=(int)1;
            $model->save();
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Role has been added'));
            //return $this->render('index');
           
            //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $this->redirect(['/manager/manager/update/', 'id' => $model->user_id]);
            }
            
        }else{
            throw new  ForbiddenHttpException('Access denied');
        };
    }

    public function actionDropRole($id)
    {   
        if(Yii:: $app->user->can('administrator')){
            if(Yii::$app->request->post()){ 
                $user_id =(int)$id;
                $q = 'DELETE FROM `auth_assignment` WHERE
                `auth_assignment`.`item_name` = "Manager"
                    AND `auth_assignment`.`user_id` = ' .(int) $user_id . '';
                Yii::$app->db->createCommand($q)->execute();
                $q = 'DELETE FROM `manager` WHERE
                `manager`.`id` = ' .(int) $user_id . '';
                Yii::$app->db->createCommand($q)->execute();
                $model = Profile::findOne(['user_id' => (int)$user_id]);
                $model->is_consultant=(int)0;
                $model->agency_id=(int)0;
                $model->save();
                //\Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'Role has been deleted'));
                //return $this->render('index');
               
                return $this->redirect('/manager/manager/index');
            }
        }else{
            throw new  ForbiddenHttpException('Access denied');
        };


    }

    

       

}
