<?php

namespace app\modules\manager\controllers;
use app\models\Profile;

class ReferalViewController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        $model = Profile::findOne(['user_id'=>$id]);
        return $this->render('index',[
            'model' => $model,
        ]);
    }

}
