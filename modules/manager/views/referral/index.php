<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Profiles');
$a = Yii::$app->session->get('agency_id');
?>
<div class="profile-index">

   
    
    <a id="photo-js" href="<?php echo Url::to(['profile/create?id=' . $a .''], true);?>" class="btn btn-success">Create profile</a>
    <?php if (Yii::$app->user->can('administrator')): ?>
        <a id="photo-js" href="<?php echo Url::to(['/agency/profile-list'], true);?>" class="btn btn-success">Назначить консультанта/принять девушку</a>
    <?php endif; ?>
     
    <?php if (!Yii::$app->user->can('administrator')&&Yii::$app->user->can('Manager')): ?>
        <a id="photo-js" href="<?php echo Url::to(['/agency/profile-list'], true);?>" class="btn btn-success">Принять девушку</a>
    <?php endif; ?>
    



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'format'=>'row',

            'user_id',
            'first_name',
            'manager_id',
            'public_email:email',
            'gravatar_email:email',
            'gravatar_id',
            //'photo',
            //'agency_id',
            //'location',
            //'website',
            //'bio:ntext',
            //'timezone',
            //'photo',
            //'credit',
            //'referrer_id',
            //'first_name',
            //'last_name',
            //'birthday',
            //'zodiac',
            //'sex',
            //'language',
            //'about_me:ntext',
            //'about_ideal:ntext',
            //'age_from',
            //'age_to',
            //'height',
            //'weight',
            //'body_type',
            //'hair_type',
            //'hair_color',
            //'eyes_color',
            //'nationality',
            //'country_id',
            //'region_id',
            //'city_id',
            //'occupation',
            //'religion',
            //'children_count',
            //'marital_status',
            //'drinking',
            //'smoking',
            //'agency_id',
            //'manager_id',
            //'last_active',
            //'facebook_link',
            //'status',
            //'avatar_id',
            //'lang_id',
            //'themes:ntext',
            //'lang2_id',
            //'lang3_id',
            //'kind_id',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>




</div>
