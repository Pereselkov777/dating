<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\modules\blog\models\Article;
use app\modules\blog\models\ArticleSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * CabinetController implements the CRUD actions for Article model.
 */
class DefaultController extends Controller
{
    public $layout = '//cabinet';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'images-get' => [
                'class' => '\vova07\imperavi\actions\GetImagesAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']], // These options are by default.
            ],
            'image-upload' => [
                'class' => '\vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetFilesAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false, // For any kind of files uploading.
            ],
            'file-delete' => [
                'class' => '\vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to(['/'], true).'uploads/blog/', // Directory URL address, where files are stored.
                'path' => '@webroot/uploads/blog', // Or absolute path to directory where files are stored.
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $this->layout = $this->module->params['admin_layout'];

        $searchModel = new ArticleSearch();
        $searchModel->author_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->author_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException(Yii::t('app', 'Доступ запрещен. Это не ваша статья'));
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();
        $model->is_enabled = 1;
        $model->is_commentable = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->author_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException(Yii::t('app', 'Доступ запрещен. Вы не имеете прав редактировать чужие статьи'));
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->author_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException(Yii::t('app', 'Доступ запрещен. Вы не имеете прав удалять чужие статьи'));
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
