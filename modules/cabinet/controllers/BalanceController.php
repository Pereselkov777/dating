<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\models\BalanceForm;
use app\models\Bill;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class BalanceController extends \yii\web\Controller
{
    public $layout = '//cabinet';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
        ];
    }
    public function actionIndex()
    {
        $model = new BalanceForm();
        if ($model->load(Yii::$app->request->post())) {
            $bill = new Bill();
            $bill->user_id = Yii::$app->user->id;
            $bill->amount = $model->amount;
            $bill->status = 0;
            $bill->order_id = 0;
            $bill->payment_id = $model->method_id;
            $bill->create_time = $bill->update_time = date('Y-m-d H:i:s');
            if (!$bill->save()) {
                var_dump($bill->getErrors()); exit;
            }
            echo \yarcode\payeer\RedirectForm::widget([
                'merchant' => Yii::$app->get('payeer'),
                'invoiceId' => $bill->id,
                'amount' => $bill->amount,
                'description' => 'Pay credits on Asia Bride',
                'currency' => \yarcode\payeer\Merchant::CURRENCY_USD // By default Merchant component currency
            ]);
            exit;
        }
        return $this->render('index',[
            'model' => $model,
        ]);
    }

}
