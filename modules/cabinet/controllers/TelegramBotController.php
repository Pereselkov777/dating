<?php

namespace app\modules\cabinet\controllers;

use app\modules\telegram\helpers\TelegramHelper;
use app\modules\telegram\helpers\TelegramValidator;
use app\modules\telegram\models\TelegramPage;
use app\modules\telegram\models\TelegramForm;
use app\modules\telegram\models\TelegramFormField;
use app\modules\telegram\models\TelegramPageField;
use app\modules\telegram\models\TelegramPageMenu;
use dosamigos\editable\EditableAction;
use Yii;
use app\modules\telegram\models\TelegramBot;
use app\modules\telegram\models\TelegramBotSearch;
use app\modules\telegram\models\TelegramFormSearch;
use app\modules\telegram\models\TelegramPageFieldSearch;
use app\modules\telegram\models\TelegramPageSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
/**
 * TelegramBotController implements the CRUD actions for TelegramBot model.
 */
class TelegramBotController extends Controller
{
    public $layout = '//cabinet-fluid';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->module->adminRoles,
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            // ...
            'editable' => [
                'class' => EditableAction::className(),
                'modelClass' => TelegramBot::className(),
                'forceCreate' => false
            ],
            'editable-page' => [
                'class' => EditableAction::className(),
                'modelClass' => TelegramPage::className(),
                'forceCreate' => false
            ],
            'editable-forms' => [
                'class' => EditableAction::className(),
                'modelClass' => TelegramForm::className(),
                'forceCreate' => false
            ],
            'editable-search' => [
                'class' => EditableAction::className(),
                'modelClass' => TelegramFormSearch::className(),
                'forceCreate' => false
            ],
            'editable-field' => [
                'class' => EditableAction::className(),
                'modelClass' => TelegramFormField::className(),
                'forceCreate' => false
            ],
            'editable-menu' => [
                'class' => EditableAction::className(),
                'modelClass' => TelegramPageMenu::className(),
                'forceCreate' => false
            ]
            // ...
        ];
    }
        /**
     * Lists all TelegramBot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TelegramBotSearch();
        $searchModel->owner_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TelegramBot model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TelegramBot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TelegramBot();
        $model->owner_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TelegramBot model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdatePage($id)
    {
        $id = (int)$id;
        $model = TelegramPage::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            if (!in_array($model->type,[ TelegramPage::TYPE_VIEW]) ){
                $model->form_id = 0;
            }
            if ( $model->save() ){
                return $this->redirect(['page', 'id' => $model->bot_id]);
            }
        }
        $model->menu_ids = TelegramPageMenu::getPageMenu($id);
   //     var_dump($model->menu_ids); exit;
        return $this->render('update-page', [
            'model' => $model,
        ]);
    }

    public function actionUpdateFieldSearch($id)
    {
        $id = (int)$id;
        $model = TelegramFormSearch::findOne($id);
        if (!empty($_POST) ) {
            $model->search_field_id = $_POST['TelegramFormSearch']['search_field_id'];
            $model->search_operator = $_POST['TelegramFormSearch']['search_operator'];
            if ( $model->save()){
                return $this->redirect(['form-search', 'id' => $model->form_id]);
            }
        }
        return $this->render('update-field-search', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing TelegramBot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeletePage($bot_id,$id)
    {
        if (($model = TelegramPage::findOne($id)) !== null){
            Yii::$app->db->createCommand("delete from {{%telegram_page_menu}}
             where page_id=".$model->id." or menu_id=".$model->id)->execute();

            $model->delete();
        };

        return $this->redirect(['page','id'=>$bot_id]);
    }

    public function actionDeleteForm($bot_id,$id)
    {
        if (($model = TelegramForm::findOne($id)) !== null){
            $t1 = '{{%telegram_form_field}}'; $t2 = '{{%telegram_form_search}}';
            Yii::$app->db->createCommand("delete $t1,$t2 from $t1 join $t2 on $t2.form_id = $t1.form_id 
             where $t1.form_id=".$model->id)->execute();
            $model->delete();
        };

        return $this->redirect(['forms','id'=>$bot_id]);
    }

    public function actionDeleteField($form_id,$id)
    {
        if (($model = TelegramFormField::findOne($id)) !== null){
            $table = 'table_'.$model->form_id;
            $field = $model->link_field;
            $show = Yii::$app->db->createCommand("SHOW COLUMNS FROM {$table} LIKE '{$field}'")->execute();
            if (!empty($show)){
                $res = Yii::$app->db->createCommand("ALTER TABLE {$table} DROP {$field}")->execute();
            }
            $q = "delete from ".TelegramFormSearch::tableName().' where form_id='.$model->form_id.
                ' and search_field_id = '.$model->id;
            Yii::$app->db->createCommand($q)->execute();
            $model->delete();
            $table = "{{%telegram_form_field}}";
            $this->setCorrectNumder($form_id,$id,$table,'id','form_id');
        };

        return $this->redirect(['fields','id'=>$form_id]);
    }

    public function actionDeleteFieldSearch($form_id,$id)
    {
        if (($model = TelegramFormSearch::findOne($id)) !== null){
            $model->delete();
            $table = "{{%telegram_form_search}}";
            $this->setCorrectNumder($form_id,$id,$table,'id','form_id');
        };
        return $this->redirect(['form-search','id'=>$form_id]);
    }

    public function actionDeleteMenu($page_id,$menu_id)
    {
        $q = 'delete from {{%telegram_page_menu}} where page_id ='.(int)$page_id.' and menu_id ='.(int)$menu_id;
        Yii::$app->db->createCommand($q)->execute();
        $table = "{{%telegram_page_menu}}";
        $this->setCorrectNumder($page_id,$menu_id,$table,'menu_id','page_id');

        return $this->redirect(['menu','id'=>$page_id]);
    }

    public function actionStart($id)
    {
        $model = $this->findModel($id);

        if (!$model->status) {
            $model->status = 1;
            $model->save();
            try {

                $bot = \app\modules\telegram\models\TelegramBot::findOne(['id' => (int)$id]);
                if ($bot === null) {
                    throw new NotFoundHttpException('The telegram bot does not exist.');
                }
                $url = Yii::$app->urlManager->createAbsoluteUrl(['/api/telegram', 'id' => $model->id]);
                $telegramBot = new TelegramHelper([
                    'apiKey' => $bot->api_token,
                    'botName' => $bot->bot_name,
                    'webhook' => $url,
                    'botId' => $bot->id,
                ]);

                $result = $telegramBot->setWebhook($url);
                if ($result) {
                    Yii::$app->session->setFlash('success', "Бот успешно активирован.");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('danger', "Произошла ошибка.");
                }
            } catch (Exception $e) {
                file_put_contents('telegram.log', print_r($e->getMessage(), true), FILE_APPEND | LOCK_EX);
            }
        }

    }

    public function actionStop($id)
    {
        $model = $this->findModel($id);

        if ($model->status) {
            $model->status = 0;
            $model->save();
            try {

                $bot = \app\modules\telegram\models\TelegramBot::findOne(['id' => (int)$id]);
                if ($bot === null) {
                    throw new NotFoundHttpException('The telegram bot does not exist.');
                }
                $url = Yii::$app->urlManager->createAbsoluteUrl(['/api/telegram', 'id' => $model->id]);
                $telegramBot = new TelegramHelper([
                    'apiKey' => $bot->api_token,
                    'botName' => $bot->bot_name,
                    'webhook' => $url,
                    'botId' => $bot->id,
                ]);

                $result = $telegramBot->unsetWebhook();
                if ($result) {
                    Yii::$app->session->setFlash('success', "Бот успешно деактивирован.");
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('danger', "Произошла ошибка.");
                }
            } catch (Exception $e) {
                file_put_contents('telegram.log', print_r($e->getMessage(), true), FILE_APPEND | LOCK_EX);
            }
        } else {
            echo 'dddd'; exit;
        }

    }


    public function actionPage($id)
    {
        $id = (int)$id;
        $model_bot = $this->findModel($id);
        $searchModel = new TelegramPageSearch();
        $searchModel->owner_id = Yii::$app->user->id;
        $searchModel->bot_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('page', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_bot' => $model_bot,
        ]);
    }

    public function actionForms($id){
        $id = (int)$id;
        $model_bot = $this->findModel($id);
        $query = TelegramForm::find();
        $query->andFilterWhere(['bot_id' => $id,]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['name' => SORT_ASC]]
        ]);

        return $this->render('forms', [
            'dataProvider' => $dataProvider,
            'model_bot' => $model_bot,
        ]);

    }

    public function actionFormField($id)
    {
        $id = (int)$id;
        if (($model_page = TelegramPage::findOne($id)) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $query = TelegramPageField::find();
        $query->andFilterWhere([
            'page_id' => $id,
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['number' => SORT_ASC]]
        ]);

        return $this->render('form', [
            //      'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_page' => $model_page,
        ]);
    }

    public function actionFields($id)
    {
        $id = (int)$id;
        if (($model_form = TelegramForm::findOne($id)) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $query = TelegramFormField::find();
        $query->andFilterWhere([
            'form_id' => $id,
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['number' => SORT_ASC]]
        ]);

        return $this->render('fields', [
            //      'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model_form' => $model_form,
        ]);
    }

    public function actionFormSearch($id)
    {
        $id = (int)$id;
        if (($model_form = TelegramForm::findOne($id)) === null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $query = TelegramFormSearch::find();
        $query->andFilterWhere([
            'form_id' => $id,
        ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['number' => SORT_ASC]]
        ]);

        return $this->render('form-search', [
            'dataProvider' => $dataProvider,
            'model_form' => $model_form,
        ]);
    }

    public function actionAjaxField()
    {
        $response = ['status'=>'fail'];
        if (isset($_POST['field_id'])){
            $field_id = $_POST['field_id'];
            $list = TelegramFormField::getOperatorField($field_id);
            $response = $list;
        }
        echo json_encode($response);
        exit;
    }

    public function actionAjaxType()
    {
        $response = ['status'=>'fail'];
        if (isset($_POST['bot_id']) && isset($_POST['type_page'])){
            $list = TelegramPage::getLinkTable($_POST['bot_id'],$_POST['type_page']);
            $response = $list;
        }
        echo json_encode($response);
        exit;
    }


    public function actionCreateField($id)
    {
        $id = (int)$id;
        $model = new TelegramFormField();
        $model->form_id = $id;
        if ($model->load(Yii::$app->request->post()) ) {
            $q = 'select id,`name`,link_field,`type` from {{%telegram_form_field}} where form_id = '.$id.' order by link_field';
            $nums = Yii::$app->db->createCommand($q)->queryAll();
            $empty_field = $exist_field = $type_field = [];
            if (empty($nums)){
                $num = 0;
            } else {
                $num = count($nums);
                foreach ($nums as $row){
                    if (empty($row['link_field'])) {
                        $empty_field[$row['id']] = TelegramFormField::$mysql_types[$row['type']];
                    } else {
                        $exist_field[$row['link_field']] = TelegramFormField::$mysql_types[$row['type']];
                    }
                }
            }
            $model->number = $num+1;
            $table = 'table_'.$id;
            $sql = "CREATE TABLE IF NOT EXISTS {$table}( ".
                "id INT NOT NULL AUTO_INCREMENT, ".
                "user_id INTEGER NOT NULL, ".
                "PRIMARY KEY ( id )); ";
            $res = Yii::$app->db->createCommand($sql)->execute();
            $exist_field_table = [];
            $colum = Yii::$app->db->createCommand('DESCRIBE '.$table)->queryAll();
            foreach ($colum as $val){
                if (($val['Field'] == 'id') || ($val['Field'] == 'user_id')) continue;
                $exist_field_table[$val['Field']] = $val['Type'];
            }
            $all_exist = $exist_field_table + $exist_field;
            $i = 0;
            if (empty($all_exist)){
                foreach ($empty_field as $id_t=>$t){
                    $i++;
                    $field_name = 'field_'.$i;
                    TelegramFormField::addFieldTable($table,$field_name,$id_t,$t);
                    $exist_field[$field_name] = $t;
                    $exist_field_table[$field_name] = $t;
                }
            } else {
                foreach ($empty_field as $id_t=>$t){
                    $i++;
                    $field_name = 'field_'.$i;
           //         echo $field_name."\n";
                    while (key_exists($field_name,$all_exist)){
                        $i++;
                        $field_name = 'field_'.$i;
                    }
                    TelegramFormField::addFieldTable($table,$field_name,$id_t,$t);
                    $exist_field[$field_name] = $t;
                    $exist_field_table[$field_name] = $t;
                }
            }
            foreach ($exist_field_table as $field=>$t){
                if (key_exists($field,$exist_field)) continue;
                $res = Yii::$app->db->createCommand("ALTER TABLE {$table} DROP {$field}")->execute();
                unset($exist_field_table[$field]);
            }
            foreach ($exist_field as  $field=>$type){
                if (key_exists($field,$exist_field_table)){
                    if ($type != $exist_field_table[$field]){
                        //$type_m = TelegramPageField::$mysql_types[$type];
                        Yii::$app->db->createCommand("ALTER TABLE {$table} modify {$field} {$type} ")->execute();
                    }
                } else {
                 //   $type_m = TelegramPageField::$mysql_types[$type];
                    Yii::$app->db->createCommand("ALTER TABLE {$table} ADD {$field} {$type} NOT NULL")->execute();
                }
            }
            $i++;
            $field_name = 'field_'.$i;
            while (key_exists($field_name,$all_exist)){
                $i++;
                $field_name = 'field_'.$i;
            }
            $model->link_field = $field_name;
            if ( $model->save()){
                $type = TelegramFormField::$mysql_types[$model->type];
                $res = Yii::$app->db->createCommand("ALTER TABLE {$table} ADD {$field_name} {$type} NOT NULL")->execute();
                return $this->redirect(['fields', 'id' => $id]);
            }
        }
        return $this->render('create-field', [
            'model' => $model,
        ]);
    }


    public function actionCreateFieldSearch($id)
    {
        $id = (int)$id;
        $model = new TelegramFormSearch();
        $model->form_id = $id;
        $model_form = TelegramForm::findOne($id);
/*        $q = "select count(*) from {{%telegram_form_search}} where form_id=".$id;
        $kol = Yii::$app->db->createCommand($q)->queryScalar();
        $is_one = $kol < 2 ? 1 : 0;*/
/*        $search_page_id = 0;
        if (!$is_one){
            $search_page_id = $res['s'];
        }*/
        if ($model->load(Yii::$app->request->post()) ) {
            /*if (!$is_one){
                $model->search_page_id = $search_page_id;
            }*/
            $q = 'select count(id) from {{%telegram_form_search}} where form_id = '.$id;
            $num = Yii::$app->db->createCommand($q)->queryScalar();
            $model->number = $num+1;
            //$model->validate();     var_dump($model->attributes); var_dump($model->errors); exit;
            if ( $model->save()){
                return $this->redirect(['form-search', 'id' => $id]);
            }
        }
        return $this->render('create-field-search', [
            'model' => $model,
            'model_form' => $model_form,
        ]);
    }

 /*   public function actionCreateMenu($id, $bot_id)
    {
        $id = (int)$id;

        if (isset($_POST['menu'])){
            $q = 'DELETE FROM {{%telegram_page_menu}} WHERE page_id = ' . $id;
            Yii::$app->db->createCommand($q)->execute();
            $insert_arr = [];
            if (count($_POST['menu'])){
                $i = 0;
                foreach ($_POST['menu'] as $menu_id) {
                    $i++;
                    $insert_arr[] = [$id, $menu_id, $i];
                }
                Yii::$app->db->createCommand()->batchInsert('{{%telegram_page_menu}}', ['page_id', 'menu_id', 'number'],
                    $insert_arr)->execute();
                return $this->redirect(['menu', 'id' => $id]);
            }
        }
        $menu = TelegramPageMenu::getListPage($id);
        return $this->render('create-menu', [
            'menu' => $menu,
            'bot_id' => $bot_id,
        ]);
    }
*/
    public function actionCreatePage($id)
    {
        $model = new TelegramPage();
        $model->owner_id = Yii::$app->user->id;
        $model->bot_id = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['page', 'id' => $id]);
        }
        return $this->render('create-page', [
            'model' => $model,
        ]);
    }

    public function actionCreateForm($id)
    {
        $model = new TelegramForm();
        $model->bot_id = $id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['forms', 'id' => $id]);
        }
        return $this->render('create-form', [
            'model' => $model,
        ]);
    }

    public function actionUp($id,$page_id,$type = 'menu')
    {
        switch ($type){
            case 'menu' :
                $table = '{{%telegram_page_menu}}';
                $field = 'menu_id';
                $field1 = 'page_id';
                $redirect = 'menu';
                break;
            case 'form' :
                $table = '{{%telegram_form_field}}';
                $field = 'id';
                $field1 = 'form_id';
                $redirect = 'fields';
                break;
            case 'form_search' :
                $table = '{{%telegram_form_search}}';
                $field = 'id';
                $field1 = 'form_id';
                $redirect = 'form-search';
                break;
            default :
                $table = '{{%telegram_page_menu}}';
                $field = 'menu_id';
                $field1 = 'page_id';
                $redirect = 'menu';
        }
        $id = (int) $id; $page_id = (int) $page_id;
        $q = "select `number`, $field1 from {$table} where $field = ".$id." and $field1= ".$page_id;
        $res = Yii::$app->db->createCommand($q)->queryOne();
        $nom = (int) $res['number'];
        $page_id = (int) $res[$field1];
        $q = "select $field,`number`, (select count($field) from {$table} where $field1 = $page_id and `number` < ".$nom.
             ") as kol  from $table where $field1 = $page_id and `number`< $nom order by $field1,`number` desc limit 1";
        $pred = Yii::$app->db->createCommand($q)->queryOne();
//    var_dump($page_id); var_dump($pred); var_dump($nom);
        if (isset($pred['number']) && $pred['number'] == $pred['kol'] && $pred['number'] == $nom-1){
            $this->setNumder($nom,$id,$pred[$field],$table,$field);
        } else {
            if ($nom != 1) {
                $new_nom = $this->setCorrectNumder($page_id,$id,$table,$field,$field1);
//  echo 'new_nom='.$new_nom;
                $q = "select $field, `number`  from $table  where $field1 = $page_id and `number` < ".$new_nom.
                    " order by $field1, `number` desc limit 1";
                $new_pred = Yii::$app->db->createCommand($q)->queryOne();
                if (isset($new_pred['id'])){
                    $this->setNumder($new_nom,$id,$new_pred['id'],$table,$field);
                }
            }
        }
        return $this->redirect([$redirect, 'id' => $page_id]);
    }

    public function actionDown($id,$page_id,$type = 'menu')
    {
        switch ($type){
            case 'menu' :
                $table = '{{%telegram_page_menu}}';
                $field = 'menu_id';
                $field1 = 'page_id';
                $redirect = 'menu';
                break;
            case 'form' :
                $table = '{{%telegram_form_field}}';
                $field = 'id';
                $field1 = 'form_id';
                $redirect = 'fields';
                break;
            case 'form_search' :
                $table = '{{%telegram_form_search}}';
                $field = 'id';
                $field1 = 'form_id';
                $redirect = 'form-search';
                break;
            default :
                $table = '{{%telegram_page_menu}}';
                $field = 'menu_id';
                $field1 = 'page_id';
                $redirect = 'menu';
        }
        $id = (int) $id;
        $q = "select `number`, $field1 from $table where $field = ".$id." and $field1=".$page_id;
        $res = Yii::$app->db->createCommand($q)->queryOne();
        $nom = (int) $res['number'];
        $page_id = (int) $res[$field1];
        $q = "select $field ,`number`, (select count($field) from $table  where $field1 =$page_id and `number` <= ".
            $nom." ) as kol  from $table  where $field1 = $page_id and `number` > ".$nom.
            " order by $field1, `number` asc limit 1";
        $pred = Yii::$app->db->createCommand($q)->queryOne();
        if (isset($pred['number'])){
            if ($pred['number'] == ($pred['kol']+1) && $pred['number'] == $nom+1){
                $this->setNumder(($nom+1),$pred[$field],$id,$table,$field);
            } else {
                $new_nom = $this->setCorrectNumder($page_id,$id,$table,$field,$field1);
                $q = "select $field,`number`  from $table  where $field1= $page_id and `number` > ".
                $new_nom." order by $field1, `number` asc limit 1";
                $new_pred = Yii::$app->db->createCommand($q)->queryOne();
                if (isset($new_pred[$field])) {
                    $this->setNumder(($new_nom+1),$new_pred[$field],$id,$table,$field);
                }
            }
        }
        //   exit;
        return $this->redirect([$redirect, 'id' => $page_id]);
    }

    function setNumder($nom,$id,$id_other,$table,$field)
    {
        $id = (int)$id; $id_other = (int)$id_other;
        $q = "update $table set `number`= $nom  where $field=".$id_other;
        $res = Yii::$app->db->createCommand($q)->execute();
        $q = "update $table set `number`= ".($nom-1)."  where $field=".$id;
        $res = Yii::$app->db->createCommand($q)->execute();
        return true;
    }

    function setCorrectNumder($page_id, $id, $table, $field,$field1)
    {
        $page_id = (int)$page_id; $id = (int)$id;
        $q = "select $field,`number` from $table where $field1 = $page_id order by $field1, `number`";
        $res = Yii::$app->db->createCommand($q)->queryAll();
        if (count($res)){
            $i = 1;
            foreach ($res as $row){
                $q = "update $table set `number`= ".$i." where $field=".(int)$row['id'];
                $res = Yii::$app->db->createCommand($q)->execute();
                $i++;
            }
        }

        $q = "select `number` from $table where $field = ".$id;
        $nom = Yii::$app->db->createCommand($q)->queryScalar();
        return $nom;
    }


    /**
     * Finds the TelegramBot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TelegramBot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TelegramBot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
