<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Article */

$this->title = Yii::t('app', 'Top Up Balance');
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];


?>
<section class="section bg-default">
    <div class="container">
        <div class="row row-50 justify-content-center justify-content-xl-between">
            <div class="col-md-10 col-lg-6 col-xl-5">
                <p><b>Now your balance:</b> <?= Yii::$app->user->identity->profile->credit ?></p>
                <h3><?= Html::encode($this->title) ?></h3>
                <!-- RD Mailform-->
                <?php  $form = ActiveForm::begin([
                    'id' => 'balance-form',
                    'method' => 'post',
                ]) ?>
                <?= $form->errorSummary($model) ?>
                <?= $form->field($model, 'amount')->textInput(); ?>
                <?= $form->field($model, 'method_id')->dropdownList([1 => 'Payeer']); ?>

                <p>
                    <?= Html::submitButton('Pay', ['class' => 'btn btn-primary']) ?>
                </p>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>