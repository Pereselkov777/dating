<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\blog\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Articles');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];


?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Article'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'category_id',
                'filter' => \app\modules\blog\models\Category::getAllCategoryList(),
                'value' => function($data) {
                    return !empty($data->category) ? $data->category->title : '';
                }
            ],
          //  'image_id',
//            'is_commentable',
            //'is_enabled',
            'title',
            'slug',
            'annotation:ntext',
            //'text:ntext',
            //'keywords',
            //'description',
            'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function($name, $model, $key){
                        return Html::a('<i class="fas fa-eye" aria-hidden="true"></i>', ['view', 'id' => $model->id]);
                    },
                    'update' => function($name, $model, $key){
                        return Html::a('<i class="fas fa-pencil" aria-hidden="true"></i>', ['update', 'id' => $model->id]);
                    },
                    'delete' => function($name, $model, $key){
                        return Html::a('<i class="fas fa-trash" aria-hidden="true"></i>', ['delete', 'id' => $model->id]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
