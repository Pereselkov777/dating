<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Private photos');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];

$this->registerCssFile(Yii::$app->request->baseUrl . '/cropper/cropper.min.css', ['depends' => [\app\assets\AppAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/cropper/cropper.min.js', ['depends' => [\app\assets\AppAsset::className()]]);

$this->registerCssFile(Yii::$app->request->baseUrl . '/css/photo_upload.css', ['depends' => [\app\assets\AppAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/upload_photo.js?t=' . time(), ['depends' => [\app\assets\AppAsset::className()]]);

?>
<div class="photo-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row row-12 row-x-12 d-md-flex flex-md-equal w-100" data-lightgallery="group" id="public_photo_list">
        <div class="col-xs-12 col-md-4 bg-light text-center overflow-hidden js-upload-item" id="new_public_photo">
            <div class="bg-dark box-shadow mx-auto" style="width: 100%; height: 400px; border-radius: 21px;">
                <div class="p-3">
                    <h2 class="display-5 text-light">Add new private photo</h2>
                </div>
            </div>
            <div class="my-3">
                <p class="lead">
                    <button class="btn btn-primary js-show-upload" data-id="0" style="min-width: 240px;">Add</button>
                </p>
            </div>
        </div>
        <?php
        $photos = $dataProvider->getModels();

        foreach($photos as $photo ) { ?>
        <div class="col-xs-12 col-md-4 bg-light text-center overflow-hidden js-upload-item">
            <a class="thumbnail-light bg-dark box-shadow mx-auto" href="<?= $photo->full ?>" data-lightgallery="item" style="width: 100%; height: 400px; border-radius: 21px;">
                <img class="thumbnail-light-image" src="<?= $photo->prew  ?>" alt="" width="300" height="400">
            </a>
            <div class="my-3">
                <p class="lead">
                    <button class="btn btn-primary js-show-upload" data-id="<?= $photo->id ?>" style="min-width: 240px;">Change</button>
                    <button class="btn btn-danger js-photo-delete" data-id="<?= $photo->id ?>">Delete</button>
                </p>
            </div>
        </div>
        <?php } ?>
    </div>
    <div>
        <?php
        echo \yii\bootstrap4\LinkPager::widget([
            'pagination' => $dataProvider->getPagination(),
        ]);
        ?>
    </div>

    <div class="modal" tabindex="-1" id="modal_photo_upload">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change photo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="crop_image" src="">
                </div>
                <div class="modal-footer" style="display: block">
                    <div>
                        <input type="hidden" id="upload_user_id" value="<?= Yii::$app->user->id ?>">
                        <input type="hidden" id="upload_photo_id" value="0">
                        <input type="hidden" id="is_private_photo" value="1">
                        <input type="file"  id="btn_upload" accept="image/*" /> or
                        <input type="text" id="url_photo_upload" value="" placeholder="URL" />
                    </div>
                    <div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btn_crop">Crop && Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php /*
    <p>
       <button class="btn btn-success btn-fla"> <a href="http://asiandating/cabinet/upload/hello">Загрузка</a></button>
    </p>
 <?php */ ?>
    <?php /*
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'title',
                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function ($data) {
                        return dosamigos\gallery\Gallery::widget(['items' => [
                            [
                                'url' => '/uploads/photos/full/' . $data->img,
                                'src' => '/uploads/photos/thumbs/' . $data->img,
                                'options' => array('title' => $data->img)
                            ],
                        ]]);
                    }
                ],
                [
                    'attribute' => 'status',
                    'filter' => \app\models\Photo::getStatusList(),
                    'value' => 'statusName',
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function($name, $model, $key){
                            return Html::a('<i class="fas fa-eye" aria-hidden="true"></i>', ['view', 'id' => $model->id]);
                        },
                        'update' => function($name, $model, $key){
                            return Html::a('<i class="fas fa-pencil" aria-hidden="true"></i>', ['update', 'id' => $model->id]);
                        },
                        'delete' => function($name, $model, $key){
                            return Html::a('<i class="fas fa-trash" aria-hidden="true"></i>', ['delete', 'id' => $model->id]);
                        }
                    ],
                ],
            ],
        ]); ?>
    </div>
    <?php */ ?>
</div>
