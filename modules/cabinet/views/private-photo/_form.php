<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Photo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-form box box-primary">
    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <div class="box-body table-responsive">
        <?php echo $form->errorSummary($model) ?>

        <?php if ($model->img) { ?>
        <div>
            <?= dosamigos\gallery\Gallery::widget(['items' => [
                [
                    'url' => '/uploads/photos/full/' . $model->img,
                    'src' => '/uploads/photos/thumbs/' . $model->img,
                    'options' => array('title' => $model->img)
                ],
            ]]); ?>
        </div>
        <?php } ?>
        <?= $form->field($model, 'file')->fileInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
