<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Photos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-admin">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'Create Photo'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </p>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'user_id',
                'title',
                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function ($data) {
                        return dosamigos\gallery\Gallery::widget(['items' => [
                            [
                                'url' => '/uploads/photos/full/' . $data->img,
                                'src' => '/uploads/photos/thumbs/' . $data->img,
                                'options' => array('title' => $data->img)
                            ],
                        ]]);
                    }
                ],
                [
                    'attribute' => 'status',
                    'filter' => \app\models\Photo::getStatusList(),
                    'value' => 'statusName',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
