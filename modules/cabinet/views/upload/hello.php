<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use app\models\Photo;
use  yii\bootstrap\BootstrapAsset;
/* @var $this yii\web\View */
/* @var $model app\models\Photo */


$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jquery.min.js');
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/jquery.min.js');
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/bootstrap.min.js');
$this->registerJsFile(Yii::$app->request->BaseUrl . '/js/croppie.js');
$this->registerCssFile(Yii::$app->request->BaseUrl . '/css/croppie.css', [
    'depends' => [BootstrapAsset::className()],
    'media' => 'print',
], 'css-print-theme');
/*$this->registerJs('
$(document).ready(function(){

	var $image_crop = $("#image_demo").croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:"square"
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $("#upload_image").on("change", function(){
	  alert("Upload");
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie("bind", {
        url: event.target.result
      }).then(function(){
        console.log("jQuery bind complete");
      });
    }
    reader.readAsDataURL(this.files[0]);
    $("#uploadimageModal").modal("show");
  });

  $(".crop_image").click(function(event){
    $image_crop.croppie("result", {
      type: "canvas",
      size: "viewport"
    }).then(function(response){
      $.ajax({
        url: " ' .Url::to(['sample']). ' "
		,
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
          $("#uploadimageModal").modal("hide");
          $("#uploaded_image").html(data);
        }
      });
    })
  });

});

');*/
?>
<html> 
<button onclick="myFunction()">Bu</button> 
    <head>  
        <title>Make Price Range Slider using JQuery with PHP Ajax</title>  
  
  
    </head>  
    <body>  
        <div class="container">
          <br />
      <h3 align="center">Image Crop & Upload using JQuery with PHP Ajax</h3>
      <br />
      <br />
   <div class="panel panel-default">
      <div class="panel-heading">Select Profile Image</div>
      <div class="panel-body" align="center">
       <input type="file" name="upload_image" id="upload_image" accept="image/*" />
       <br />
       <div id="uploaded_image"></div>
      </div>
     </div>
    </div>
    </body>  
</html>

<div id="uploadimageModal" class="modal" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload & Crop Image</h4>
        </div>
        <div class="modal-body">
          <div class="row">
       <div class="col-md-8 text-center">
        <div id="image_demo" style="width:350px; margin-top:30px"></div>
       </div>
       <div class="col-md-4" style="padding-top:30px;">
        <br />
        <br />
        <br/>
        <button class="btn btn-success crop_image">Crop & Upload Image</button>
     </div>
    </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
     </div>
    </div>
</div>
<script type="text/javascript">  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:200,
      height:200,
      type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
		alert('AJAX');
      $.ajax({
        url: '<?php echo Url::to(['sample']); ?>',
        type: "POST",
        data:{"image": response},
        success:function(data)
        {
			console.log('success');
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(data);
        }
		
      })
	  event.preventDefault();
    })
  });

});  
</script>




