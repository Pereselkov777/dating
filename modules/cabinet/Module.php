<?php

namespace app\modules\cabinet;

/**
 * billing module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\cabinet\controllers';
    public $adminRoles = ['administrator', 'admin', 'superadmin'];
    public $layout_frontend = '//column2';
    public $layout_admin = '//admin';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->layout = '//cabinet';
        // custom initialization code goes here
    }
}
