<?php

namespace app\modules\admin\controllers;

use app\models\Profile;
use Yii;
use app\models\ProfileSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ClientController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProfileSearch();
        $searchModel->sex = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAdd()
    {
        if (isset($_POST['user_id'], $_POST['credits'])) {
            $model = Profile::findOne($_POST['user_id']);
            if ($model === null) {
                echo 'user not found'; exit;
            }
        }
        $model->credit = $model->credit + (float)$_POST['credits'];
        $model->save(false);
        return $this->goBack();
    }

}
