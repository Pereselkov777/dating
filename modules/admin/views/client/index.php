<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Clients (Man)');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dating-order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
               'attribute' => 'user_id',
               'value' => function ($data) {
                    return Html::a($data->user->username, ['/user/profile/show', 'id' => $data->user_id], ['target' =>'_blank']);
               },
                'format' => 'raw',
            ],
            [
                'attribute' => 'credit',
                'value' => function ($data) {
                    return $data->credit . '&nbsp;' .
                        Html::a('Add Credits', '#', ['onclick' =>'showModalAddCredits('.$data->user_id.')']);
                },
                'format' => 'raw',
            ],
            'agency_id',
            'manager_id',
        ],
    ]); ?>

    <div class="modal" tabindex="-1" id="modal_add_credits">
        <div class="modal-dialog">
            <div class="modal-content">
                <?= Html::beginForm(['add']) ?>
                <div class="modal-header">
                    <h5 class="modal-title">Add credits</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"></span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="user_id" name="user_id">
                    <input type="text" id="credits" name="credits">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
                <?=Html::endForm() ?>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function showModalAddCredits(user_id) {
            $('#user_id').val(user_id);
            $('#credits').val(10);
            $('#modal_add_credits').modal('show');
        }
    </script>
</div>
