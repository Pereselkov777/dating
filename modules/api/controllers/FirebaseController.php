<?php

namespace app\modules\api\controllers;

use app\models\PushToken;
use yii\web\Response;

class FirebaseController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    public function actionIndex()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $token = \Yii::$app->request->post('token', '');
        if ($token == '') {
            return [
                'result' => 'fail',
                'message' => 'empty token'
            ];
        }
        $model = PushToken::findOne($token);
        if ($model === null) {
            $model = new PushToken();
            $model->token_id = $token;
            $model->user_id = \Yii::$app->user->id;
            if (!$model->save()) {
                return [
                    'result' => 'fail',
                    'message' => print_r($model->getErrors(), true)
                ];
            }
        }
        return [
            'result' => 'success',
        ];
    }

}
