<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\manager\models\Manager;
use app\models\Profile;
use app\models\Agency;
use yii\helpers\Url;
//use yii\bootstrap\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\manager\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Managers');
$a = Yii::$app->session->get('agency_id');
$agency = Agency::findOne(['id'=>$a]);
$girl_count = $agency->getGirls($a); 
//(echo ($girl_count); exit();

///$d = $agency->getGirls($a);

//var_damp($agency->getGirls($agency->id));exit();
/*$manager = Manager::findOne(Yii::$app->user->id);
var_dump($manager);
$models =  $manager->getManagerProfiles();
var_dump($models['id']); exit();*/


?>
<div class="manager-index">

    <h1><?= Html::encode($this->title) ?></h1>


   

</div>
  <!-- Content Wrapper. Contains page content -->

  <div style="margin-left:0 !important;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $girl_count ?></h3>

              <p>Девушек в агенстве</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="<?php echo Url::to(['/agency/profile/index/?id=' . $a .''], true);?>" class="small-box-footer">К списку девушек <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $my_girl_count ?><sup style="font-size: 20px">человек</sup></h3>

              <p>Мои рефералы</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="<?php echo Url::to(['/manager/referral/index/?id=' . $a .''], true);?>" class="small-box-footer">К списку реферралов<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $total_user_count ?></h3>

              <p>Всего юзеров на сайте</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="<?php echo Url::to(['/agency/profile-list/index/'], true);?>" class="small-box-footer">Принять юзера в агенство<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>?</h3>

              <p>Мои проекты</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
       
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
  
  
