<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
?>
<h1>detail/index</h1>

<div class="card-header">
    <h3 class="card-title">Total amount:<?php echo "{$total_girl}" ?></h3>
    <h3 class="card-title">Period: <?php echo " from {$timeThen} before {$timeNow}" ?></h3>
</div>

<?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'girl_id',
            'create_time',
            'amount',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} '],
        ],
    ]); ?>

