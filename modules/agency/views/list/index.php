<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;
/* @var $this yii\web\View */

?>
<h1>list/index</h1>

<?= GridView::widget([
        'dataProvider' => $provider,
        'filterModel' => $searchModel,
        'columns' => [
           /* [   
                'attribute' => 'user_id',
                'contentOptions' => ['nowrap' => 'nowrap'],
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'clientOptions' => [
                        'source' => [],
                        'autoFill' => true,
                        //'minLength' => 2
                    ],
                    'options' => ['class' => 'form-control']
                ]),
            ],*/
            ['class' => 'yii\grid\SerialColumn'],
            'user_id',
            'photo',
            'title',
            //'agency_id',
            'update_time',

            ['class' => 'yii\grid\ActionColumn', 'template'=>'{view}'],
        ],
    ]); ?>