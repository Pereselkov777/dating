<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Photo */

$this->title = Yii::t('app', 'Create Photo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Photos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];


?>
<div class="photo-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>

