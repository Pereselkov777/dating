<?php
/* @var $this yii\web\View */
//use app\assets\AppAsset;
//AppAsset::register($this);
use yii\helpers\Url;
use app\assets\AgencyAsset;
use yii\widgets\ListView;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use app\modules\blog\models\Category;
use app\models\ProfileTag;
use yii\helpers\ArrayHelper;
use app\models\Profile;
//use Yii;
use yii\base\Widget;
//AgencyAsset::register($this);
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];
//$models =  Profile::find()->all();
//$user_list=ArrayHelper::map($models,'user_id','first_name');
//$sex_list=ArrayHelper::map($models,'sex','first_name');
$models =  Profile::find()->asArray()->all();
$user_list=ArrayHelper::map($models,'user_id','first_name');
//var_dump($user_list);
//exit();

?>

<div id="alert" class="alert alert-success" role="alert">
  <h4 class="alert-heading">Well done!</h4>
  <p>Aww yeah, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content.</p>
  <hr>
  <p class="mb-0">Whenever you need to, be sure to use margin utilities to keep things nice and tidy.</p>
</div>
<script type="text/javascript">
$(document).ready(function(){
    console.log($("#alert"));
    $("#alert").hide();
  $(".click").click(function (e) {
    e.preventDefault();
    console.log('SUC');
   
    user_id = $(this).data('id');
   
    console.log($(this).data('id'), "data-id");
    $.ajax({
        type: "POST",
        url: "/manager/add-role/index",
        data: {
                user_id: user_id,
        },
        success: function (response) {
            
        console.log(response);
        $("#alert").show();
        //console.log($("#card_push")
        //var txt="test" ;
            
        },
        error: function (exception) {
            console.log(exception.responseText);
            console.log('SUC');
            
        }
    });
    
  }); 
  $(".click2").click(function (e) {
    e.preventDefault();
    //console.log('SUC');
   
    user_id = $(this).data('id');
   
    console.log($(this).data('id'), "data-id");
    $.ajax({
        type: "POST",
        url: "/manager/girl/get-girl",
        data: {
                user_id: user_id,
        },
        success: function (response) {
           $("#alert").show();
            
        console.log(response);
        //console.log($("#card_push")
        //var txt="test" ;
            
        },
        error: function (exception) {
            console.log(exception.responseText);
            console.log('SUC');
            
        }
    });
    
  }); 
});  

</script>
<style type="text/css">
  .series-tbl
  {
    float: left;
  }
</style>
<div class="container">
                <?= Html::beginForm(['/agency/profile-list/index/'], 'get', ['id' => 'profile_search', 'class' => 'search-form row row-narrow-20 align-items-center justify-content-center row-20']) ?>
                    
                    
                    <div class="col-xl-7 block-l">
                        <div class="form-wrap">
                            <label class="form-label form-label-outside" for="country">User</label>
                        </div>
                        <div class="group-20 align-items-end big-group">
                            <div class="form-wrap">
                                <select class="form-input" id="country" name="ProfileListSearch[user_id]" data-constraints="@Required" data-placeholder="Choose your user">
                                    <option label="placeholder"></option>
                                    <?php
                                    foreach ($user_list as $user_id =>$first_name) { ?>
                                    <option value="<?= $user_id ?>"<?= $user_id == $searchModel->user_id ? ' selected' : '' ?>><?= $first_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            
                            <div class="form-wrap form-button">
                                <button class="button button-icon button-icon-left button-primary" type="submit"><span class="icon mdi mdi-magnify"></span>Find your partner</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                       
                      
                        <div class="col-sm-6">
                           
                        </div>
                    </div>
                   
                        
                <?= Html::endForm() ?>
            </div>
    <div class="divider-3"></div>
        <div class="container mt-30 mt-md-45 mt-xxl-75 text-center">
            <h2 class="wow fadeIn">Only True People</h2>
            <p class="text-gray-700 wow fadeIn" data-wow-delay=".025s"><span style="max-width: 850px;">Every user registered on GO is verified via photo and mobile phone so you don’t have to worry how real or fake anyone is.</span></p>
            <?php echo ListView::widget([
                'dataProvider' => $listDataProvider,
                'itemView' => '_profile',
                'layout' => "{items}\n",
                'itemOptions' => ['class' => 'col-sm-6 col-lg-4'],
                'options' => ['class' => 'row row-30 row-xl-50 mt-md-45 mt-xxl-70'],
                'pager' => [
                    'firstPageLabel' => 'first',
                    'lastPageLabel' => 'last',
                    'nextPageLabel' => 'next',
                    'prevPageLabel' => 'previous',
                    'maxButtonCount' => 3,
                ],
            ]); ?>
             <div>
                <?php
                    echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $listDataProvider->getPagination(),
                    ]);
                ?>
            </div>
            <div class="row row-30 row-xl-50 mt-md-45 mt-xxl-70">
                <div class="col-12 wow fadeIn text-center" data-wow-delay=".6s"><a class="button button-sm button-gray-outline wow fadeIn" href="#" data-wow-delay=".07s">View more</a></div>
            </div>
        </div>
    </div>

<?php

/*if(Yii::$app->user->can('administrator')){
    foreach ($data as $key => $value) {
        echo "
        <div class='card-group series-tbl'>
             <div class='card text-white bg-info mb-3 mr-2' style='max-width: 18rem;'>
             <div class='card-body'>
                 <h5 class='card-title'>" .$value['user_id'] ."</h5>
                 <h5 class='card-title'>" .$value['first_name'] ."</h5>
                 <p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                 <button class='btn btn-success'>
                 <a style='color:white;' data-id=" .$value['user_id'] ." class='click'
                  >Add Role</a></button>
                  <button class='btn btn-success'>
                  <a style='color:white;' data-id=" .$value['user_id'] ." class='click2'
                >Add to agency</a></button>
             </div>
             </div>
         </div>
      ";
        
     }
}

if(Yii::$app->user->can('Manager')&& !Yii::$app->user->can('administrator')){
    foreach ($data as $key => $value) {
        echo "
        <div class='card-group series-tbl'>
             <div class='card text-white bg-info mb-3 mr-2' style='max-width: 18rem;'>
             <div class='card-body'>
                 <h5 class='card-title'>" .$value['user_id'] ."</h5>
                 <h5 class='card-title'>" .$value['first_name'] ."</h5>
                 <p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                 <button class='btn btn-success'>
                 <a style='color:white;' data-id=" .$value['user_id'] ." class='click2'
                  >Add to agency</a></button>
             </div>
             </div>
         </div>
      ";
        
     }
}*/







?>


