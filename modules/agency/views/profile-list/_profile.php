<?php
use yii\widgets\ListView;
use app\models\Profile;


/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$name = $model->first_name . ' ' . $model->last_name;
if (empty(trim($name))) {
    $name = $model->user->username;
}
$profile= Profile::findOne(['user_id'=>Yii::$app->user->id]);
?>


<!-- Tour 3-->

<article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(<?= $model->photo ? $model->getAvatarUrl() : '/img/no-user.jpg' ?>);">
    <div class="tour-3-inner">
        <div class="tour-3-main">
            <h3 class="tour-3-title"><?= \yii\helpers\Html::a($name, ['/user/profile/show', 'id' => $model->user_id]) ?></h3>
            <ul class="tour-3-meta">
                <li><span class="icon mdi material-design-clock100"></span><span><?= $model->last_active ?></span></li>
            </ul>
            <?php if ($model->manager_id > 0): ?>
                <ul class="tour-3-meta">
                <li><span class="icon mdi material-design-clock100"></span><span>Assigned consultant:<?= $model->manager_id ?></span></li>
            </ul>
            <?php endif; ?>
            <?php if ($model->is_consultant): ?>
                <ul class="tour-3-meta">
                <li><span class="icon mdi material-design-clock100"></span><span>Consultant</span></li>
            </ul>
            <?php endif; ?>
            
        </div>
       
    </div>
    
</article>
<?php if (!$model->is_consultant && $model->manager_id===0 && Yii::$app->user->can('administrator')): ?>
<button data-id='<?= $model->user_id ?>' class="btn btn-success mt-2 click">Add Role</button>
<?php endif; ?>
<?php if ((int)$model->manager_id === 0 &&  !$model->is_consultant): ?>
    <button data-id='<?= $model->user_id ?>' class="btn btn-success mt-2 click2">Add to Agency</button>
<?php endif; ?>


