<?php

/*use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Profile;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;*/
use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
//use yii\widgets\ActiveForm;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
//use app\components\cropper\Cropper;
use app\models\Profile;
use app\modules\manager\models\Manager;

//use Yii;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
$profile = Profile::findOne(Yii::$app->user->id);
//var_dump($profile->getKindList());
$a = Yii::$app->session->get('agency_id');
//var_dump($a);
$kind_list = $profile->getKindList();
/*$items = [
    '1' => (int)$a,
    
];*/
$params = [
    'prompt' => 'Choose your value...',
    'value' =>$model->kind_id,
];

$models =  Manager::find()->all();
$manager_list = ArrayHelper::map($models,'id','name');
//$manager_list = array_merge( [ '0' => 'Не задан'], $manager);
//var_dump($manager_list); exit();
//echo($model->manager_id);
$consultant_list = [
    '0'=>0,
    '1'=>1,
];
$test_list = [
    '0'=>0,
    '1'=>1,
];

//print_r($a);
//var_dump($manager_list); exit();

/*Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];
/*Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
    'sourcePath' => null,
    'css' => [],
    'js' => [],
];*/

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

});
</script>

<div class="profile-form">

    <div class="card">

        <div class="card-body">
            <?php $form = ActiveForm::begin([
                    'id' => 'profile-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                ]); ?>
            <?= $form->errorSummary($model) ?>
            <div class="form-group field-profile-bio">
                <label class="col-lg-3 control-label">Referral link</label>
                <div class="col-lg-9">
                    <?= Html::textInput('ref_link', Url::to(['/user/register', 'referrer' => $model->user_id], true),
                            ['readonly' => 'readonly', 'class' => 'form-control']) ?>
                </div>
                <div class="col-sm-offset-3 col-lg-9">
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                    <br>
                </div>
            </div>

            <?= $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::className(),[
                    'language' => 'en-Gb',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'defaultDate' => '1990-01-01',
                        'changeMonth'=> true,
                        'changeYear'=> true,
                        'yearRange' => "-90:-18",
                    ],
                    'options' => ['class' => 'form-control',]
                ]) ?>

            <?php
                if ($model->sex) { ?>

            <div class="form-group field-profile-bio">
                <label class="col-lg-3 control-label">Sex</label>
                <div class="col-lg-9">
                    <?= Html::textInput('sex_readonly', $model->getSexTypeName(),
                                ['readonly' => 'readonly', 'class' => 'form-control']) ?>
                </div>
                <div class="col-sm-offset-3 col-lg-9">
                    <div class="help-block"></div>
                </div>
            </div>
            <?php } else {
                   echo $form->field($model, 'sex')->dropDownList(\app\models\Profile::getSexList()); 
                }
                ?>

            <?= $form->field($model, 'first_name') ?>

            <?= $form->field($model, 'last_name') ?>

            <?= $form->field($model, 'marital_status')->dropDownList(\app\models\Profile::getMaritalStatusList()) ?>

            <?= $form->field($model, 'children_count') ?>

            <?= $form->field($model, 'age_from') ?>

            <?= $form->field($model, 'age_to') ?>

            <?= $form->field($model, 'height') ?>

            <?= $form->field($model, 'weight') ?>

            <?= $form->field($model, 'body_type')->dropDownList(\app\models\Profile::getBodyTypeList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'hair_type')->dropDownList(\app\models\Profile::getHairTypeList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'hair_color')->dropDownList(\app\models\Profile::getHairColorList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'eyes_color')->dropDownList(\app\models\Profile::getEyesColorList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'drinking')->dropDownList(\app\models\Profile::getDrinkingList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'smoking')->dropDownList(\app\models\Profile::getSmokingList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'about_me')->textarea() ?>

            <?= $form->field($model, 'about_ideal')->textarea() ?>

            <?= $form->field($model, 'themes')->textarea() ?>

            <?= $form->field($model, 'lang_id')->dropDownList(\app\models\Profile::getLangList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'lang2_id')->dropDownList(\app\models\Profile::getLangList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'lang3_id')->dropDownList(\app\models\Profile::getLangList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

            <?= $form->field($model, 'country_id')->widget(Select2::classname(), [
                    'data' => \app\components\GeoHelper::getCountries(),
                    'options' => ['placeholder' => 'Choose country ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

            <?= $form->field($model, 'region_id')->widget(Select2::classname(), [
                    'data' => $model->country_id ? \app\components\GeoHelper::getRegions($model->country_id) : [],
                    'options' => ['placeholder' => 'Choose region ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

            <?= $form->field($model, 'city_id')->widget(Select2::classname(), [
                    'data' => $model->region_id ? \app\components\GeoHelper::getCities($model->region_id) : [],
                    'options' => ['placeholder' => 'Choose city ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

            <?= $form->field($model, 'facebook_link') ?>

            <?= $form->field($model, 'public_email') ?>

            <?= $form->field($model, 'kind_id')->textInput()->dropDownList($kind_list, $params); ?>

            <?= $form->field($model, 'agency_id')->textInput(['readonly' => true,
                 'value' =>$a]) ?>
            <?php
                    if (!Yii::$app->user->can('administrator')) { ?>
            <div class="form-group field-profile-bio">
                <label class="col-lg-3 control-label">Manager</label>
                <div class="col-lg-9">
                    <?= Html::textInput('manager_id_readonly', $model->manager_id,
                                    ['readonly' => 'readonly', 'class' => 'form-control']) ?>
                </div>
                <div class="col-sm-offset-3 col-lg-9">
                    <div class="help-block"></div>
                </div>
            </div>
            <?php } else {
                   
                    echo $form->field($model, 'manager_id')->dropDownList($manager_list, ['value'=>$model->manager_id]);
                }
                ?>

            <?= $form->field($model, 'is_consultant')->textInput(['readonly' => true,
                 'value' =>$model->is_consultant]) ?>
            <?= $form->field($model, 'is_test')->dropDownList($test_list, ['prompt' => Yii::t('app','Not chosen')]) ?>





            <?php if ($model->photo) { ?>
            <?= $form->field($model, 'status')->dropDownList([0 => 'Not Active', 1 => 'Active']) ?>
            <?php } else { ?>
            <div class="alert alert-danger" role="alert">

                Your profile is inactive.
                <?= Html::a('Add a photo', ['/cabinet/photo/index'], ['target' => '_blank']) ?> and make it your avatar,
                after which you can activate your profile.

            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                    <br>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <?php
$script = <<< JS
    jQuery(document).ready(function () {
        $('#profile-country_id').change(function() {
            console.log('changed');
            $.ajax({
                      url: '/geo/get-regions',  //урл запроса
                      data: {country_id: + $(this).val()},
                      dataType: 'json',
                      success: function(data){ 
                          $('#profile-region_id option:gt(0)').remove();
                          //data.sort();
                          //console.log(data);
                          Object.keys(data).forEach(function(k){
                              $('#profile-region_id').append($("<option></option>")
                                 .attr("value", k).text(data[k]));
                          });
                    }}
             );
        });

        $('#profile-region_id').change(function() {
            console.log('changed');
            $.ajax({
                      url: '/geo/get-cities',  //урл запроса
                      data: {region_id: + $(this).val()},
                      dataType: 'json',
                      success: function(data){ 
                          $('#profile-city_id option:gt(0)').remove();
                          Object.keys(data).forEach(function(k){
                              $('#profile-city_id').append($("<option></option>")
                                 .attr("value", k).text(data[k]));
                          });
                    }}
             );
        });
    });

JS;
$this->registerJs($script);
?>