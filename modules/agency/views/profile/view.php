<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {

$("#photo-js").click(function(){
    //window.location.href =  'asiandating/agency/photo/index/?id=' + <?php echo "{$model->user_id}" ?>;  
})
});

</script>
<div class="profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>

         <?= Html::a(Yii::t('app', 'Delete from agency'), ['/manager/girl/drop-girl', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
         <a id="photo-js" href="<?php echo Url::to(['photo/index/?id=' . $model->user_id .''], true);?>" class="btn btn-success">Assign photo</a>
    </p>
    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'name',
            'public_email:email',
            'gravatar_email:email',
            'gravatar_id',
            'location',
            'website',
            'bio:ntext',
            'photo',
            'credit',
            'referrer_id',
            'first_name',
            'last_name',
            'birthday',
            'zodiac',
            'sex',
            'language',
            'about_me:ntext',
            'about_ideal:ntext',
            'age_from',
            'age_to',
            'height',
            'weight',
            'body_type',
            'hair_type',
            'hair_color',
            'eyes_color',
            'nationality',
            'country_id',
            'region_id',
            'city_id',
            'occupation',
            'religion',
            'children_count',
            'marital_status',
            'drinking',
            'smoking',
            'agency_id',
            'manager_id',
            'last_active',
            'facebook_link',
            'status',
            'avatar_id',
            'lang_id',
            'themes:ntext',
            'lang2_id',
            'lang3_id',
            'kind_id',
        ],
    ]) ?>

</div>
