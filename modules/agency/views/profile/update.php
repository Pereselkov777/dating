<?php




use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = Yii::t('app', 'Update Profile: {name}', [
    'name' => $model->name,
]);




$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profiles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

/*Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];*/
//AgencyAsset::register($this);
?>
<div class="profile-update">
<a id="photo-js" href="<?php echo Url::to(['photo/index/?id=' . $model->user_id .''], true);?>" class="btn btn-success">Assign photo</a>
   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
