<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\jui\AutoComplete;
/* @var $this yii\web\View */

?>

<div class="card-header">
    <h3 class="card-title">Agency:<?php echo "{$model->name}" ?></h3>
    <h3 class="card-title">Total amount:<?php echo "{$total_amount}" ?></h3>
    <h3 class="card-title">Period: <?php echo " from {$timeThen} before {$timeNow}" ?></h3>
</div>

<?= GridView::widget([
        'dataProvider' => $provider,
        'filterModel' => $searchModel,
        'columns' => [
           /* [   
                'attribute' => 'user_id',
                'contentOptions' => ['nowrap' => 'nowrap'],
                'filter' => AutoComplete::widget([
                    'model' => $searchModel,
                    'clientOptions' => [
                        'source' => [],
                        'autoFill' => true,
                        //'minLength' => 2
                    ],
                    'options' => ['class' => 'form-control']
                ]),
            ],*/
            ['class' => 'yii\grid\SerialColumn'],
            'girl_id',
            'sum',
            'first_name',
            //'agency_id',
            'last_name',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} '],
        ],
    ]); ?>