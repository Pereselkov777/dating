<?php

namespace app\modules\agency\controllers;

use app\models\Profile;
use Yii;
use yii\data\ActiveDataProvider;
use app\models\ProfileListSearch;

class ProfileListController extends \yii\web\Controller
{
    //public $layot = "//cabinet";

    /*public function actionIndex()
    {
        $this->layout = '//cabinet';
        if(Yii::$app->user->can('administrator')||Yii::$app->user->can('Manager')){
            $q = 'SELECT * FROM profile WHERE is_consultant=0 AND agency_id=0 AND manager_id=0 ORDER BY  user_id';
            //$q = 'SELECT * FROM profile ORDER BY user_id';
           // var_dump($q); exit();
            $data = Yii::$app->db->createCommand($q)->queryAll();
        }
       
      

        return $this->render('index',[
            'data' => $data,
        ]);
    }*/

    public function actionIndex()
    {
        $this->layout = '//cabinet';
        if(Yii::$app->user->can('administrator')||Yii::$app->user->can('Manager')){
            /*$dataProvider = new ActiveDataProvider([
                'query' => Profile::find()->where(['is_consultant' => 0,
                'agency_id'=>0, 'manager_id'=>0])->orderBy('user_id DESC'),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);*/
            //var_dump($_GET);exit();
            $searchModel = new ProfileListSearch();
            /*$searchModel->is_consultant = 0;
            $searchModel->agency_id = 0;
            $searchModel->manager_id = 0;*/
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

           /* var_dump($dataProvider);
            exit();*/
    
        }
       
        return $this->render('index',
         ['listDataProvider' => $dataProvider,
         'searchModel' => $searchModel,
         ]);
    }

}
