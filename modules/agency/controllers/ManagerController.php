<?php

namespace app\modules\agency\controllers;

class ManagerController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $q = 'SELECT COUNT(user_id) FROM profile WHERE profile.manager_id =' .(int) Yii::$app->user->id . '  ';
        $my_girl_count = Yii::$app->db->createCommand($q)->queryScalar();
        $q = 'SELECT COUNT(user_id) FROM profile';
        $total_user_count = Yii::$app->db->createCommand($q)->queryScalar();

        return $this->render('index', [
            'my_girl_count'=>$my_girl_count,
            'total_user_count'=> $total_user_count,

            
        ]);
    }

}
