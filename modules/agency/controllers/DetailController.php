<?php

namespace app\modules\agency\controllers;

use Yii;
use app\models\ClientOrder;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\data\SqlDataProvider;

class DetailController extends \yii\web\Controller
{
    public function actionIndex($id, $agency_id)
    {   
        if(Yii::$app->user->can('Manager') && !Yii::$app->user->can('administrator')){
            if($agency_id != Yii::$app->user->identity->profile->agency_id){
                throw new  ForbiddenHttpException('You can to see your agency only');
            }
        }
        $girl_id =(int)$id;
        if($girl_id !==Yii::$app->user->id && !Yii::$app->user->can('Manager') ){
            throw new  ForbiddenHttpException('You can to see your profile only');
        }
        $timeNow = date('Y-m-d 19:00:00');
        //var_dump($timeNow);
        $timeThen = date('Y-m-01 00:00:00');
        //Yii::$app->db->createCommand()->update('agency', ['description' => $data], 'id=1')->execute();
        $sql='SELECT * FROM 
        client_order WHERE agency_id = ' . (int)$agency_id . ' AND girl_id = ' . (int)$girl_id .  '
        AND
          create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '"';
        $sqlCount = Yii::$app->db->createCommand('
        SELECT COUNT(*) FROM 
        client_order WHERE agency_id = :agency_id AND girl_id = :girl_id
        AND
          create_time BETWEEN :timeThen AND :timeNow
        ', [':agency_id' => $agency_id, ':timeNow'=>(string)$timeNow,
         ':timeThen'=>(string)$timeThen, ':girl_id'=>(int)$girl_id])->queryScalar();
        //$query = ClientOrder::find()->where(['agency_id' => (int)$agency_id, 'girl_id'=>(int)$girl_id,]);
        //$girl_data = Yii::$app->db->createCommand($q)->queryOne();

        $q='SELECT SUM(amount_agency) as total_agency, SUM(amount) as total_girl FROM
         client_order WHERE agency_id = ' . $agency_id . ' AND girl_id = ' . $girl_id . ' AND
         create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '"';
        //var_dump($q);
        //exit();
        $total_data = Yii::$app->db->createCommand($q)->queryOne();
        //$q='SELECT first_name FROM profile WHERE user_id = ' . $girl_id . '';
        //$first_name = Yii::$app->db->createCommand($q)->queryOne();
        /*var_dump($sql);
        var_dump($sqlCount);
        exit();*/
        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':agency_id' =>$agency_id,
            ':timeNow'=>(string)$timeNow,
            ':timeThen'=>(string)$timeThen,
            ':search_girl_id'=>(int)$girl_id],
            //'key'=>'user_id',
            //'params' => [':status' => 1],
            'totalCount' => $sqlCount,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        //var_dump($dataProvider);
        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $provider,
            'total_girl' =>$total_data['total_girl'],
            'timeNow'=>$timeNow,
            'timeThen'=>$timeThen,
        ]);
        
    }

}
