<?php

namespace app\modules\agency\controllers;

use app\models\PhotoSearch;
use yii\data\ActiveDataProvider;
use app\models\Profile;
use app\models\ProfileSearch;
use app\models\Photo;
use yii\data\SqlDataProvider;


use Yii;

class ListController extends \yii\web\Controller
{   
    
    public $girl_id;
        
    public function actionIndex($id)
    {   
        $searchModel = new PhotoSearch();
        $user = Profile::findOne(Yii:: $app->user->id);
        $sql='SELECT * FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=2 WHERE photo.user_id=profile.user_id';
        $sqlCount =  Yii::$app->db->createCommand('
        SELECT COUNT(*) FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=2 WHERE photo.user_id=profile.user_id
        ', [':agency_id' => $id])->queryScalar();
        /*var_dump($sql);
        var_dump($sqlCount);
        //var_dump($provider);
        exit();*/
        //$sqlCount = 'SELECT COUNT(*) FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=0 WHERE photo.user_id=profile.user_id';
        //$count = Yii::$app->db->createCommand($sqlCount)->queryScalar();
        
        /*if($_GET["PhotoSearch"]['title'] !== ''){
            var_dump($_GET["PhotoSearch"]['title']);
            var_dump($_GET["PhotoSearch"]['user_id']);
        }*/
       
        if(isset($_GET["PhotoSearch"]) && isset($_GET["PhotoSearch"]['user_id'])){
            //1.)user_id = string title = string(o)
            //var_damp();
            //exit();
            $girl_id = $_GET["PhotoSearch"]['user_id'];
            //var_dump($girl_id);
            $sql='SELECT * FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=2 WHERE photo.user_id=profile.user_id AND photo.user_id =' . $girl_id . ' ';
            $sqlCount =  Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=2 WHERE photo.user_id=profile.user_id
            AND photo.user_id =:girl_id', [':agency_id' => 1,':girl_id'=>$girl_id ])->queryScalar();
            //var_dump($sql);
            //var_dump($sqlCount);
            //exit();
            $provider = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':agency_id' => 1, ':girl_id'=>$girl_id],
                'key'=>'user_id',
                //'params' => [':status' => 1],
                'totalCount' => $sqlCount,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            //exit();
        }

        if(isset($_GET["PhotoSearch"]) && isset($_GET["PhotoSearch"]['title']) && ($_GET["PhotoSearch"]['title'] !== '')){
            //2.)user_id = string(o) title = string
            //echo('TITLE');
            //var_damp();
            //exit();
            $title = $_GET["PhotoSearch"]['title'];
            //var_dump($girl_id);
            $sql='SELECT * FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=2 WHERE photo.user_id=profile.user_id AND photo.title ="' . (string)$title .'" ';
            $sqlCount =  Yii::$app->db->createCommand('
            SELECT COUNT(*) FROM photo INNER JOIN profile ON profile.agency_id=:agency_id AND profile.sex=2 WHERE photo.user_id=profile.user_id
            AND photo.title = :title', [':agency_id' => 1,':title'=>(string)$title ])->queryScalar();
            //var_dump($sql);
            //var_dump($sqlCount);
            //exit();
            $provider = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':agency_id' => 1, ':title'=>$title],
                'key'=>'user_id',
                //'params' => [':status' => 1],
                'totalCount' => $sqlCount,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            //exit();
        }
        

        
        //var_dump($sql);
        //var_dump($sqlCount);
        //exit();
        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':agency_id' => 1],
            'key'=>'user_id',
            //'params' => [':status' => 1],
            'totalCount' => $sqlCount,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        
        return $this->render('index', [
            'provider' => $provider,
            'searchModel'=>$searchModel,
        ]);
    }

    public function actionView($id)
    {

        //echo($id);
        
        return $this->redirect(['profile/view', 'id' => $id]);
       
    }
    public function actionUpdate($id)
    {
        
        return $this->redirect(['profile/update', 'id' => $id]);
       
    }
    public function actionDelete($id)
    {
        
        return $this->redirect(['profile/delete', 'id' => $id]);
       
    }

}
