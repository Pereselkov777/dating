<?php

namespace app\modules\agency\controllers;

use app\models\PhotoSearch;
use yii\data\ActiveDataProvider;
use app\models\Agency;
use app\models\ProfileSearch;
use app\models\Photo;
use yii\data\SqlDataProvider;
use app\models\ClientOrderSearch;


use Yii;

class AgencyProfitController extends \yii\web\Controller
{   
    
    public $girl_id;

    public $layout = '//agency';
        
    public function actionIndex($id)
    {   
        
        
        if(Yii::$app->user->identity->profile->agency_id
            && Yii::$app->user->identity->profile->agency_id !== (int)$id && !Yii::$app->user->can('administrator')){
            throw new  ForbiddenHttpException('It \'s not your agency');
        }
        if(Yii::$app->user->isGuest){
            return $this->redirect('login');
        }
        Yii :: $app-> session->set('agency_id', $id);
        $model = Agency ::findOne((int)$id);
        $searchModel = new ClientOrderSearch;
        $agency_id = (int)$id;
        $timeNow = date('Y-m-d 19:00:00');
        //var_dump($timeNow);
        $timeThen = date('Y-m-01 00:00:00');
        //var_dump($timeThen);
        //var_dump($id);
        $sql= 'SELECT client_order.girl_id as girl_id, SUM(amount) as sum, profile.first_name as first_name,
        profile.last_name as last_name, profile.status as status FROM  client_order INNER  JOIN profile ON
        profile.user_id=client_order.girl_id WHERE client_order.agency_id =' . $agency_id . ' AND create_time BETWEEN
        "' . $timeThen . '" AND "' . $timeNow . '" GROUP BY girl_id';
        $sqlCount =  Yii::$app->db->createCommand('
        SELECT COUNT(girl_id)FROM  client_order INNER  JOIN profile ON
        profile.user_id=client_order.girl_id WHERE client_order.agency_id =:agency_id AND create_time BETWEEN
        :timeThen AND :timeNow
        ', [':agency_id' => $agency_id, ':timeNow'=>(string)$timeNow, ':timeThen'=>(string)$timeThen])->queryScalar();

        if(isset($_GET["ClientOrderSearch"]) && isset($_GET["ClientOrderSearch"]['girl_id'])){
           // echo('SEARCH');
            //1.)user_id = string title = string(o)
            //var_damp();
            //exit();
            $search_girl_id = $_GET["ClientOrderSearch"]['girl_id'];
            //var_dump($girl_id);
            $sql='SELECT client_order.girl_id as girl_id, SUM(amount) as sum, profile.first_name as first_name,
            profile.last_name as last_name, profile.status as status FROM  client_order INNER  JOIN profile ON
            profile.user_id=client_order.girl_id WHERE client_order.agency_id =' . (int)$agency_id . ' AND create_time BETWEEN
            "' . $timeThen . '" AND "' . $timeNow . '" AND client_order.girl_id=' .(int)$search_girl_id . ' GROUP BY girl_id';
            $sqlCount =  Yii::$app->db->createCommand('
            SELECT COUNT(girl_id)FROM  client_order INNER  JOIN profile ON
            profile.user_id=client_order.girl_id WHERE client_order.agency_id =:agency_id AND create_time BETWEEN
            :timeThen AND :timeNow AND girl_id = :search_girl_id
           ', [':agency_id' => (int)$id,
                ':search_girl_id'=>$search_girl_id,
                ':timeNow'=>(string)$timeNow,
                ':timeThen'=>(string)$timeThen,

            ])->queryScalar();
            
            $provider = new SqlDataProvider([
                'sql' => $sql,
                'params' => [':agency_id' =>$agency_id,
                ':timeNow'=>(string)$timeNow,
                ':timeThen'=>(string)$timeThen,
                ':search_girl_id'=>(int)$search_girl_id],
                'key'=>'user_id',
                //'params' => [':status' => 1],
                'totalCount' => $sqlCount,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
           /* var_dump($sql);
            var_dump($sqlCount);
            exit();*/
        }
        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => [':agency_id' =>$agency_id, ':timeNow'=>(string)$timeNow, ':timeThen'=>(string)$timeThen],
            'key'=>'girl_id',
            //'params' => [':status' => 1],
            'totalCount' => $sqlCount,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        /*var_dump($sql);
        var_dump($sqlCount);
        //var_dump($provider);
        exit();*/
        $q='SELECT SUM(amount_agency) as total_amount FROM client_order WHERE create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '"';
        $total_amount =  Yii::$app -> db -> createCommand($q)->queryOne();
        /*if($names){
           // echo 'Загрузка прошла успешно <br><pre>';
                        
            $tmp = [];
            foreach ($names as $k => $v) {
            if (array_key_exists($v['user_id'], $tmp)) {
            unset($names[$k]);
            } else {
            $tmp[$v['user_id']] = true;
            }
            }
           /*echo'<pre>';
            var_dump($total_amount["total_amount"]);
            var_dump($q);
            exit();
           echo '</pre>';
           
           
            
        };*/
        
        return $this->render('index', [
            'provider' => $provider,
            'searchModel'=>$searchModel,
            'model'=> $model,
            'total_amount'=>$total_amount['total_amount'],
            'timeThen' =>$timeThen,
            'timeNow'=>$timeNow,
        ]);
    }

    public function actionView($id)
    {

        $a =(Yii::$app->session->get('agency_id'));
        //var_dump($a);
        //$agency_id = 
        //echo($id);
        //echo($agency_id);
        //exit();
        
        return $this->redirect(['detail/index', 'id' => $id, 'agency_id'=>$a]);
       
    }
    public function actionUpdate($id)
    {
        
        return $this->redirect(['detail/update', 'id' => $id]);
       
    }
    public function actionDelete($id)
    {
        
        return $this->redirect(['profile/delete', 'id' => $id]);
       
    }

}

