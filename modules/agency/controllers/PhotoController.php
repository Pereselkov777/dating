<?php

namespace app\modules\agency\controllers;

use Yii;
use app\models\Photo;
use app\models\PhotoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use app\models\Profile;

/**
 * PhotoController implements the CRUD actions for Photo model.
 */
class PhotoController extends Controller
{
    public $layout = '//agency_cabinet';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create, index, update, delete, view'],
                'rules' => [
                    [
                        'actions' => ['create, index, update, delete, view'],
                        'allow' => true,
                        'roles'=>['Manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Photo models.
     * @return mixed
     */
    public function actionIndex($id)
    {   

       
            $searchModel = new PhotoSearch();
            $searchModel->user_id = (int)$id;
            $profile = Profile::findOne(['user_id' =>$searchModel->user_id]);
           // var_dump($profile->manager_id);exit();
            if(Yii::$app->user->can('administrator')
            ||(int)$profile->manager_id ===(int)Yii::$app->user->id){
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            //var_dump(Yii::$app->user->id);
            //var_dump($searchModel->user_id);
            //var_dump( $dataProvider);
            //exit();
    
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user_id' => $id,
            ]);

         }else{
            throw new  ForbiddenHttpException('Access denied');
         }
        
    }

    /**
     * Displays a single Photo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Photo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   

        
        $model = new Photo();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Photo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   
        
        $model = $this->findModel($id);
        $profile = Profile::findOne(['user_id' =>$model->user_id]);
        // var_dump($profile->manager_id);exit();
        if(Yii::$app->user->can('administrator')
            ||(int)$profile->manager_id ===(int)Yii::$app->user->id){
       

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }

            return $this->render('update', [
                'model' => $model,
            ]);
        }else{
            throw new  ForbiddenHttpException('Access denied');

        }
          
    }

    /**
     * Deletes an existing Photo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Photo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Photo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Photo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
