<?php


namespace app\modules\agency\controllers;

use app\models\Agency;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;



class ProfitController extends \yii\web\Controller

{   

    public $layout = '//agency';

     /**
     * Displays a single Agency model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'detail', 
                    ],
                'rules' => [
                    [
                        'actions' => ['index', 'detail',],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex($id)

        {   if(Yii::$app->user->identity->profile->agency_id
            && Yii::$app->user->identity->profile->agency_id !== (int)$id){
                throw new  ForbiddenHttpException('It \'s not your agency');
            }
            $model = Agency ::findOne((int)$id);
            $timeNow = date('Y-m-d 19:00:00');
            //var_dump($timeNow);
            $timeThen = date('Y-m-01 00:00:00');
            //var_dump($timeThen);
            //var_dump($id);
            $q= 'SELECT girl_id, SUM(amount) as sum FROM client_order WHERE agency_id = ' .(int)$id . ' AND create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '" GROUP BY girl_id';
            //var_dump($q);
            //exit();
            $tables = Yii::$app -> db -> createCommand($q)->queryAll();
            $q = 'SELECT profile.user_id, profile.first_name FROM profile INNER JOIN client_order ON client_order.girl_id=profile.user_id';
            $names =  Yii::$app -> db -> createCommand($q)->queryAll();
            $q='SELECT SUM(amount_agency) as total_amount FROM client_order WHERE create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '"';
            $total_amount =  Yii::$app -> db -> createCommand($q)->queryOne();
            if($names){
               // echo 'Загрузка прошла успешно <br><pre>';
                            
                $tmp = [];
                foreach ($names as $k => $v) {
                if (array_key_exists($v['user_id'], $tmp)) {
                unset($names[$k]);
                } else {
                $tmp[$v['user_id']] = true;
                }
                }
               /*echo'<pre>';
                var_dump($total_amount["total_amount"]);
                var_dump($q);
                exit();
               echo '</pre>';*/
               
               
                
            };

            //var_dump($table);
            //exit();
            /*foreach ($table as $key => $value) {
            // var_dump($value['girl_id']);
                $profile = Profile ::findOne($value['girl_id']);
                $table['name']=$profile->user->username;
                //var_dump($profile->user->username);

            };*/
            return $this->render('index', [
                'model' => $model,
                'tables'=>$tables,
                'names' =>$names,
                'total_amount' => (float)$total_amount['total_amount'],
            ]);
        }


    public function actionDetail()

    {
        $data = Yii::$app->request->post();
        
        if(isset($data)) {
            $girl_id =(int)$data['girl_id'];
            /*if($girl_id !==Yii::$app->user->id && !Yii::$app->user->can('Manager') ){
                throw new  ForbiddenHttpException('You can to see your profile only');
            }*/
            $agency_id =(int)$data['agency_id'];
            $timeNow = date('Y-m-d 19:00:00');
            //var_dump($timeNow);
            $timeThen = date('Y-m-01 00:00:00');
            //Yii::$app->db->createCommand()->update('agency', ['description' => $data], 'id=1')->execute();
            $q='SELECT create_time, amount, amount_agency FROM client_order WHERE agency_id = ' . (int)$agency_id . ' AND girl_id = ' . (int)$girl_id . '';
            $girl_data = Yii::$app->db->createCommand($q)->queryOne();
            $q='SELECT SUM(amount_agency) as total_agency, SUM(amount) as total_girl FROM client_order WHERE agency_id = ' . $agency_id . ' AND girl_id = ' . $girl_id . ' AND create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '"';
            //var_dump($q);
            //exit();
            $total_data = Yii::$app->db->createCommand($q)->queryOne();
            $q='SELECT first_name FROM profile WHERE user_id = ' . $girl_id . '';
            $first_name = Yii::$app->db->createCommand($q)->queryOne();
            /*echo 'Загрузка прошла успешно <br><pre>';
                        
            var_dump($first_name); 
            var_dump($q); 
            echo '</pre>';
            exit();*/
         $txt = "Ajax Worked!";
        }else {
         $txt = "Ajax failed";
        };
        //return \yii\helpers\Json::encode($txt);
        //var_damp($girl_data);
        /*if($girl_data){
            echo 'Загрузка прошла успешно <br><pre>';
                        
            var_dump($girl_data); 
            
            echo '</pre>';
        }*/

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'create_time' =>$girl_data['create_time'],
            'amount' => $girl_data['amount'],
            'amount_agency' =>$girl_data['amount_agency'],
            'total_agency' => $total_data['total_agency'],
            'total_girl' => $total_data['total_girl'],
            'first_name' => $first_name['first_name'],
            
        ];
    }
        




}
