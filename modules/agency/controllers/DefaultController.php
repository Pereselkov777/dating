<?php

namespace app\modules\agency\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;

/**
 * Default controller for the `agency` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public $layout = '//cabinet';


    public function behaviors()
    {
       
       // var_dump($role);
       //exit();
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles'=>['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {   if(Yii::$app->user->isGuest){
        //Yii::$app->session->setFlash('danger', 'Access denied');
        return $this->redirect('login');
    }
        return $this->render('index');
    }
}
