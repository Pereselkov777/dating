<?php

namespace app\modules\agency\controllers;

use Yii;
use app\models\Profile;
use app\models\ProfileSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use dektrium\user\filters\AccessRule;
use dektrium\user\Finder;
use dektrium\user\models\User;
use dektrium\user\models\UserSearch;
use dektrium\user\helpers\Password;
use dektrium\user\Module;
use dektrium\user\traits\EventTrait;
use yii\base\ExitException;
use yii\base\Model;
use yii\base\Module as Module2;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\data\ActiveDataProvider;

/**
 * ProfileController implements the CRUD actions for Profile model.
 */
class ProfileController extends Controller

{   
   
    use EventTrait;

    /**
     * Event is triggered before creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CREATE = 'beforeCreate';

    /**
     * Event is triggered after creating new user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * Event is triggered before updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';

    /**
     * Event is triggered after updating existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';

    /**
     * Event is triggered before impersonating as another user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_IMPERSONATE = 'beforeImpersonate';

    /**
     * Event is triggered after impersonating as another user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_IMPERSONATE = 'afterImpersonate';

    /**
     * Event is triggered before updating existing user's profile.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_PROFILE_UPDATE = 'beforeProfileUpdate';

    /**
     * Event is triggered after updating existing user's profile.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_PROFILE_UPDATE = 'afterProfileUpdate';

    /**
     * Event is triggered before confirming existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_CONFIRM = 'beforeConfirm';

    /**
     * Event is triggered after confirming existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_CONFIRM = 'afterConfirm';

    /**
     * Event is triggered before deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';

    /**
     * Event is triggered after deleting existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_DELETE = 'afterDelete';

    /**
     * Event is triggered before blocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_BLOCK = 'beforeBlock';

    /**
     * Event is triggered after blocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_BLOCK = 'afterBlock';

    /**
     * Event is triggered before unblocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_BEFORE_UNBLOCK = 'beforeUnblock';

    /**
     * Event is triggered after unblocking existing user.
     * Triggered with \dektrium\user\events\UserEvent.
     */
    const EVENT_AFTER_UNBLOCK = 'afterUnblock';

    /**
     * Name of the session key in which the original user id is saved
     * when using the impersonate user function.
     * Used inside actionSwitch().
     */
    const ORIGINAL_USER_SESSION_KEY = 'original_user';

    /** @var Finder */
    protected $finder;

    /**
     * @param string  $id
     * @param Module2 $module
     * @param Finder  $finder
     * @param array   $config
     */
    public function __construct($id, $module, Finder $finder, $config = [])
    {
        $this->finder = $finder;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
       
       // var_dump($role);
       //exit();
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles'=>['Manager'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Profile models.
     * @return mixed
     */
    public function actionIndex($id)

    {   $session = Yii::$app->session;
        $session->open();
        if ($session->isActive){
            $session->set('agency_id', $id);
        }
        
        $searchModel = new ProfileSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$user = Profile::findOne(Yii:: $app->user->id);
        //$query = Profile::find()->where(['agency_id' => $user->agency_id]);
        //$q='SELECT photo, user_id FROM profile WHERE agency_id = ' . $user->agency_id . ' AND LENGTH (photo)';
        //$data = Yii::$app->db->createCommand($q)->queryAll();
        //$sql='SELECT * FROM photo INNER JOIN profile ON profile.agency_id=' .(int)$user->agency_id .  ' AND profile.sex=0 WHERE photo.user_id=profile.user_id';
        
        //var_dump($_GET["ProfileSearch"]['user_id']);
       
        if(isset($_GET["ProfileSearch"])
         && isset($_GET["ProfileSearch"]['user_id'])
         ){
            //1.)user_id = string title = string(o)

           if(array_key_exists('manager_id', $_GET["ProfileSearch"])){
            $manager_id=$_GET["ProfileSearch"]['manager_id'];
            $query = Profile::find()->where(['agency_id' => (int)$id, 'sex'=>2, 'manager_id'=>(int)$manager_id]);
            //var_dump($query);
            //exit();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'user_id' => SORT_DESC,
                    ]
                ],
            ]);
            //var_dump($dataProvider);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            
           }
            $girl_id = $_GET["ProfileSearch"]['user_id'];
           
            //var_dump($girl_id);
            $query = Profile::find()->where(['agency_id' => (int)$id, 'sex'=>2, 'user_id'=>(int)$girl_id]);
            //var_dump($query);
            //exit();
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'user_id' => SORT_DESC,
                    ]
                ],
            ]);
            //var_dump($dataProvider);
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
            //exit();
        }
        //var_dump($_GET);
        //exit();
      
        //echo('3');
        //exit();
        
        $query = Profile::find()->where(['agency_id' => (int)$id, 'sex'=>2 ])->andWhere(['not', ['manager_id' =>0]]);
        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
       // exit();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'user_id' => SORT_DESC,
                ]
            ],
        ]);
        //var_dump($dataProvider);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profile model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Profile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   

        $id = Yii::$app->session->get('agency_id');
        $this->layout = '//cabinet';
        /*$model = new Profile();
         

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);*/
        if(Yii::$app->user->can('administrator')||Yii::$app->user->can('Manager') ){
            //echo('is admin, 1');
            /** @var User $user */
            //echo('HERE');exit();
            $user = \Yii::createObject([
            'class'    => User::className(),
            'scenario' => 'create',
            ]);

            $event = $this->getUserEvent($user);

            $this->performAjaxValidation($user);
            $this->trigger(self::EVENT_BEFORE_CREATE, $event);
            if ($user->load(\Yii::$app->request->post()) && $user->create()) {
               
                $this->trigger(self::EVENT_AFTER_CREATE, $event);
                $model = Profile::findOne($user->id);
                if($model){
                    //$model->sex = 2;
                    //$model->agency_id=$id;
                    //echo('HERE1');
                    //exit();
                    $model->manager_id= Yii::$app->user->id;
                    if($model->save(false)){//а почему не хочет model(save)
                        //echo('HERE2');exit();
                        //var_dump($model->manager_id);exit();
                        \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));

                        return $this->redirect(['profile/update', 'id' => $user->id]);
                    }
                    
                }
                /*if($user->user_id){
                    $model = new Profile();
                    $model->user_id = $user->id;
                    $model->save(false);
                    return $this->redirect(['profile/update', 'id' => $user->id]);
                }*/
                
                
                
            }
    
            return $this->render('create', [
                'user' => $user,
            ]);
        }else {
                if(Yii::$app->user->identity->profile->agency_id
                    && Yii::$app->user->identity->profile->agency_id === (int)$id ){
                //echo('is manager 2');
                    /** @var User $user */
                $user = \Yii::createObject([
                    'class'    => User::className(),
                    'scenario' => 'create',
                ]);
                $event = $this->getUserEvent($user);
        
                $this->performAjaxValidation($user);
                $this->trigger(self::EVENT_BEFORE_CREATE, $event);
                if ($user->load(\Yii::$app->request->post()) && $user->create()) {
                    \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));
                    $this->trigger(self::EVENT_AFTER_CREATE, $event); 
                    /*if($user->user_id){
                        $model = new Profile();
                        $model->user_id = $user->id;
                        $model->save(false);
                        return $this->redirect(['profile/update', 'id' => $user->id]);
                    }*/
                    
                    return $this->redirect(['profile/update', 'id' => $user->id]);
                    
                }
        
                return $this->render('create', [
                    'user' => $user,
                ]);
    
                }else{
                //echo('manager has no agent_id or agent_id != agency_id,3');
                throw new  ForbiddenHttpException('Access denied');
                //exit();
                }
           
        }
       
    }

    /**
     * Updates an existing Profile model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {   

        //$this->layout = '//cabinet';
        $model = $this->findModel($id);
        //echo($model->manager_id);
        //exit();
        if(Yii::$app->user->can('administrator')
            ||(int)$model->manager_id === (int)Yii::$app->user->id){
                
                /*Url::remember('', 'actions-redirect');
            $user = User::findOne($id);
            $user->scenario = 'update';
            $event = $this->getUserEvent($user);*/

            $this->performAjaxValidation($model);

            // $this->trigger(self::EVENT_BEFORE_UPDATE, $event);
            if ($model->load(\Yii::$app->request->post()) && $model->save()) {
                //var_dump($model->country_id); exit();
                if(!$model->first_name){
                    //var_dump($model->first_name);exit();
                    \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'Please choose your first_name'));
                    return $this->redirect(['profile/update', 'id' => $model->user_id]);
                    //return false;
                }
                if($model->sex == 0){
                    //var_dump($model->first_name);exit();
                    \Yii::$app->getSession()->setFlash('danger', \Yii::t('user', 'Please choose your sex'));
                    return $this->redirect(['profile/update', 'id' => $model->user_id]);
                    //return false;
                }
                \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
                // $this->trigger(self::EVENT_AFTER_UPDATE, $event);
                //$model->save();
                return $this->refresh();
            }
    
            /*var_dump($id);
            exit();*/
            /* if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->user_id]);
            }*/
            
            //$model->load(Yii::$app->request->post());
            //$model->save();
            //return $this->refresh();
            return $this->render('update', [
                'model' => $model,
            ]);

        }else{
            throw new  ForbiddenHttpException('Access denied');
        }
       
    }

    /**
     * Deletes an existing Profile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)

    {   
        $agency_id = Yii::$app->session->get('agency_id');
     
        $this->findModel($id)->delete();
        
        return $this->redirect(['profile/index', 'id'=>$agency_id]);
    }

    /**
     * Finds the Profile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    protected function performAjaxValidation($model)
    {
        if (\Yii::$app->request->isAjax && !\Yii::$app->request->isPjax) {
            if ($model->load(\Yii::$app->request->post())) {
                var_dump(\Yii::$app->request->post()); exit();
                \Yii::$app->response->format = Response::FORMAT_JSON;
                \Yii::$app->response->data = json_encode(ActiveForm::validate($model));
                //var_dump(\Yii::$app->response->data);
                \Yii::$app->end();
            }
        }
    }
}