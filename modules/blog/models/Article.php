<?php

namespace app\modules\blog\models;

use dektrium\user\models\User;
use dosamigos\taggable\Taggable;
use Yii;
use yii\bootstrap\Html;
use yii\behaviors\SluggableBehavior;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "{{%blog_articles}}".
 *
 * @property int $id ID
 * @property int $author_id Автор
 * @property int $category_id Категория
 * @property int $image Изображение
 * @property int $is_commentable Разрешено комментировать
 * @property int $is_enabled Включено
 * @property string $title Заголовок
 * @property string $slug Алиас
 * @property string $annotation Аннотация
 * @property string $text Текст
 * @property string $keywords Метатеги. Ключевые слова
 * @property string $description Метатеги. Описание
 * @property string $created_at Создано
 * @property string $updated_at Обновлено

 * @property User $author Автор
 * @property Category $category Категория
 * @property Tag[] $tags Теги
 */
class Article extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%blog_article}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'category_id', 'is_commentable', 'is_enabled'], 'integer'],
            [['is_commentable', 'title', 'text'], 'required'],
            [['annotation', 'text'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'slug', 'keywords', 'description', 'image'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['tagNames'], 'safe'],
            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'jpg, jpeg, gif, png'],
            [['file'], 'file', 'maxSize'=>'10000000'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', Yii::t('app','ID')),
            'author_id' => Yii::t('app', Yii::t('app','Author')),
            'category_id' => Yii::t('app', Yii::t('app','Category')),
            'image_id' => Yii::t('app', Yii::t('app','Image')),
            'is_commentable' => Yii::t('app', Yii::t('app','Is Commentable')),
            'is_enabled' => Yii::t('app', Yii::t('app','Is Enabled')),
            'title' => Yii::t('app', Yii::t('app','Title')),
            'slug' => Yii::t('app', Yii::t('app','Alias')),
            'annotation' => Yii::t('app', Yii::t('app','Annotation')),
            'text' => Yii::t('app', Yii::t('app','Text')),
            'keywords' => Yii::t('app', Yii::t('app','Mata tags.Keywords')),
            'description' => Yii::t('app', Yii::t('app','Mata tags. Description')),
            'created_at' => Yii::t('app', Yii::t('app','Created at')),
            'updated_at' => Yii::t('app', Yii::t('app','Updated at')),
            'file' => Yii::t('app', Yii::t('app','File`s image')),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
            ],
            'tagNames' => [
                'class' => Taggable::className(),
                'attribute' => 'tagNames',
                'name' => 'title',
                'frequency' => 'weight'
            ]
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                //var_dump($insert);exit();
                // Да это новая запись (insert)
                $this->created_at = date('Y-m-d H:i:s');
                $this->author_id = Yii::$app->user->id;
            }
            $this->updated_at = date('Y-m-d H:i:s');
            $dirties = $this->getDirtyAttributes();
            if (empty($this->annotation) || (!isset($dirties['annotation']) && isset($dirties['text']))) {
                $this->annotation = StringHelper::truncateWords(strip_tags($this->text), 100);
            }
            return true;
        }
        return false;
    }

    public function getFullImage() {
        return $this->image ? Yii::getAlias('@web') . '/images/articles/full/' . $this->image : '';
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     *  @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('{{%blog_article_via_tag}}', ['article_id' => 'id']);
    }

    public static function getArchived($limit = 12)
    {
        $q = "SELECT created_at, MONTH(created_at) as month, YEAR (created_at) as year
				FROM ".self::tableName()." as p
				WHERE is_enabled = 1
				GROUP BY year, month				
				ORDER BY year desc, month desc
				LIMIT :limit";

        $res =  Yii::$app->db->createCommand($q)
            ->bindValues(["limit"=>$limit])->queryAll();

        return ($res == null?[]:$res);
    }

    public static function getLast($limit = 12)
    {
        $models = self::find()->where('is_enabled = 1')->orderBy(['created_at' => SORT_DESC])->all();

        return $models;
    }


}
