<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use budyaga\users\components\AuthChoice;
use wbraganca\fancytree\FancytreeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */

?>

<div class="card">
    <div class="card-header">Рубрики блога</div>
    <div class="card-body">
        <div class="card-text">
            <div id="rubrics"><?= $rubrics ?></div>
        </div>
    </div>
</div>
