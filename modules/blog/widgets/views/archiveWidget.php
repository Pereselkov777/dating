<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use budyaga\users\components\AuthChoice;
use wbraganca\fancytree\FancytreeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */

?>

<div class="card">
    <div class="card-header">
        <div class="portlet-title">Архив статей</div>
    </div>
    <div class="card-body">
        <div class="card-text">
            <ul class="nav nav-pills flex-column">
                <?php
                foreach ($models as $m)
                {
                    echo '<li class="nav-item">'.Html::a(Yii::$app->formatter->asDate(strtotime($m['created_at']), 'LLLL YYYY'),["archive","year"=>$m["year"],"month"=>$m["month"]], ['class' => 'nav-link']).'</li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>
