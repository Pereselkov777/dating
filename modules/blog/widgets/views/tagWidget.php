<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use budyaga\users\components\AuthChoice;
use wbraganca\fancytree\FancytreeWidget;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $tags array */

?>

<div class="card">
    <div class="card-header">
        <div class="portlet-title">Облако тегов</div>
    </div>
    <div class="card-body">
        <div id="tags_cloud" class="card-text">
           <?php
           foreach($tags as $slug=>$tag)
           {
               $link=Html::a($tag['title'], ['tag', 'slug'=>$slug], ['class' => 'text-light']);
               echo Html::tag('span', $link, [
                       'class'=>'badge bg-primary p-2 mb-1',
                       'style'=>"font-size:{$tag['weight']}pt",
                   ])."\n";
           }

           ?>
        </div>
    </div>
</div>
