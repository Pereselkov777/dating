<?php

namespace app\modules\blog\controllers;

use app\models\User;
use app\modules\blog\models\Article;
use app\modules\blog\models\Category;
use app\modules\blog\models\Tag;
use Yii;
use app\modules\blog\models\ArticleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `blog` module
 */
class DefaultController extends Controller
{
    public $layout = '//column2';

    public function beforeAction($action)
    {
        $this->layout = $this->module->layout_frontend;
        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => Yii::t('app', 'Articles'),
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionCategory($slug)
    {
        $categoryModel = Category::findOne(['slug' => $slug]);
        if ($categoryModel === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $searchModel = new ArticleSearch();
        $params = Yii::$app->request->queryParams;
        $params['ArticleSearch']['category_id'] = $categoryModel->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $categoryModel->title,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionUser($user_id)
    {
        $userModel = User::findOne(['id' => $user_id]);
        if ($userModel === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $searchModel = new ArticleSearch();
        $params = Yii::$app->request->queryParams;
        $params['ArticleSearch']['author_id'] = $userModel->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $userModel->username . '`s articles '
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionTag($slug)
    {
        $tagModel = Tag::findOne(['slug' => $slug]);
        if ($tagModel === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $searchModel = new ArticleSearch();
        $params = Yii::$app->request->queryParams;
        $params['ArticleSearch']['tag_id'] = $tagModel->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Статьи с тегом ' . $tagModel->title,
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionArchive($year, $month)
    {
        $searchModel = new ArticleSearch();
        $params = Yii::$app->request->queryParams;
        $params['ArticleSearch']['start_time'] = date('Y-m-d H:i:s', strtotime($year.'-'.$month.'-01 00:00:00'));
        $params['ArticleSearch']['end_time'] = date('Y-m-t 23:59:59', strtotime($year.'-'.$month.'-01'));
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Архив за ' . Yii::$app->formatter->asDate($year.'-'.$month.'-01', 'YYYY, LLLL'),
        ]);
    }

    /**
     * Displays a single Article model.
     * @param string $slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($slug)
    {
        return $this->render('view', [
            'model' => $this->findModel($slug),
        ]);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Article::findOne(['slug' => $slug])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

}
