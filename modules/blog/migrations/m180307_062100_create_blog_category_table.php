<?php

use yii\db\Migration;

/**
 * Handles the creation of table `blog_category`.
 */
class m180307_062100_create_blog_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%blog_category}}', [
            'id' => $this->primaryKey()->unsigned()->comment('ID'),
            'parent_id' => $this->integer()->unsigned()->unique()->comment('Родитель'),
            'slug' => $this->string(128)->unique()->notNull()->comment('Алиас'),
            'title' => $this->string(128)->notNull()->comment('Заголовок'),
            'created_at' => $this->dateTime()->comment('Создано'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%blog_category}}');
    }
}
