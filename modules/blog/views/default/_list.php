<?php
use \yii\bootstrap\Html;
/* @var $model app\modules\blog\models\Article */
?>
<h2><?= Html::a($model->title, ['view', 'slug' => $model->slug]) ?></h2>

<p>
    <span class="glyphicon glyphicon-calendar"></span> <?= Yii::$app->formatter->asDatetime(strtotime($model->created_at), 'dd MMMM yyyy, hh:mm') ?>
    , написал <span class="glyphicon glyphicon-user"></span> <a href="<?= \yii\helpers\Url::to(['/user/profile/show', 'id' => $model->author->id ]) ?>"><?= $model->author->username ?></a>
</p>

<p><?= $model->annotation ?></p>

<p><?= Html::a('Read more', ['view', 'slug' => $model->slug], ['class' => 'btn btn-info pull-right']) ?>
    </p>

<?php if (!empty($model->tags)) {
    foreach ($model->tags as $tag) { ?>
    <?= Html::a('<span class="p-2 mb-1 badge bg-info text-light">'.$tag->title.'</span>', ['tag', 'slug' =>$tag->slug ]) ?>
<?php }
    } ?>

<?php if (!empty($model->category)) { ?>
        <?= Html::a('<span class="p-2 mb-1 badge bg-success text-light">'.$model->category->title.'</span>', ['category', 'slug' =>$model->category->slug ]) ?>
<?php } ?>

<hr/>


