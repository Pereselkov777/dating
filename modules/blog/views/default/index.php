<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\blog\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- формируем блок хедера -->
<?php $this->beginBlock('breadcrumbs-custom'); ?>
<h2 class="breadcrumbs-custom-title">Blog</h2>
<p class="big">
    This is the section of blogs and personal diaries
</p>
<?php $this->endBlock();?>

<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Article'), ['/cabinet/default/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'col-md-12 item','tag'=>'div'],
        //'summary'=>Yii::t('app','List of account codes where increase on receipt or revenues'),
        'itemView'=>'_list',
        'options' => ['class' => 'row list-view'],
        'layout'=>"{items}<div class='row'></div>{pager}",
    ]); ?>

    <div class="google_adsens">
      <!--  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:block; text-align:center;"
             data-ad-format="fluid"
             data-ad-layout="in-article"
             data-ad-client="ca-pub-4133262202239972"
             data-ad-slot="7974909240"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>-->
    </div>

</div>