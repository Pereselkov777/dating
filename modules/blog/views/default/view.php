<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];

?>
<div class="article-view">

    <h2><?= Html::a($model->title, ['view', 'slug' => $model->slug]) ?></h2>

    <p>
        <span class="glyphicon glyphicon-calendar"></span> <?= Yii::$app->formatter->asDatetime(strtotime($model->created_at), 'dd MMMM yyyy, hh:mm') ?>
        , написал <span class="glyphicon glyphicon-user"></span> <a href="<?= \yii\helpers\Url::to(['/user/profile/show', 'id' => $model->author->id ]) ?>"><?= $model->author->username ?></a>
    </p>

    <?php if (!empty($model->image)) { ?>
        <img src="<?= $model->getFullImage() ?>" style="width: 100%">
    <?php } ?>

    <p><?= $model->text ?></p>

    <?php if (!empty($model->tags)) {
        foreach ($model->tags as $tag) { ?>
            <?= Html::a('<span class="badge bg-info text-light p-2 mb-1">'.$tag->title.'</span>', ['tag', 'slug' =>$tag->slug ]) ?>
        <?php }
    } ?>

    <?php if (!empty($model->category)) { ?>
        <?= Html::a('<span class="badge bg-success text-light p-2 mb-1">'.$model->category->title.'</span>', ['category', 'slug' =>$model->category->slug ]) ?>
    <?php } ?>

    <div>Поделиться записью с друзьями: </div>
    <script type="text/javascript">(function() {
            if (window.pluso)if (typeof window.pluso.start == "function") return;
            if (window.ifpluso==undefined) { window.ifpluso = 1;
                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                var h=d[g]('body')[0];
                h.appendChild(s);
            }})();</script>
    <div class="pluso" data-background="#ebebeb" data-options="medium,square,line,horizontal,counter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
    <?php /*
    <div class="google_adsens">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:block; text-align:center;"
             data-ad-format="fluid"
             data-ad-layout="in-article"
             data-ad-client="ca-pub-4133262202239972"
             data-ad-slot="7974909240"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <?Php */ ?>

    <?php
    // https://github.com/yii2mod/yii2-comments
    echo \yii2mod\comments\widgets\Comment::widget([
        'model' => $model,
        'maxLevel' => 5,
        'dataProviderConfig' => [
            'pagination' => [
                'pageSize' => 10
            ],
        ],
        'listViewConfig' => [
            'emptyText' => Yii::t('app', 'No comments found.'),
        ],
    ]);
    /*
    <div class="block_comments">
        <h3>Комментарии</h3>
        <div><?php
            echo \dosamigos\disqus\CommentsCount::widget([
                'shortname' => Yii::$app->params['disqus_name'],
                'identifier' => \yii\helpers\Url::to(['view', 'slug' => $model->slug])
            ]); ?> </div>
        <div><?php
            echo \dosamigos\disqus\Comments::widget([
                // see http://help.disqus.com/customer/portal/articles/472098-javascript-configuration-variables
                'shortname' => Yii::$app->params['disqus_name'],
                'identifier' => \yii\helpers\Url::to(['view', 'slug' => $model->slug])
            ]);?>
        </div>
    </div>
    <?php */ ?>
</div>
