<?php

use yii\db\Migration;

/**
 * Class m210127_162008_profile_add_status
 */
class m210127_162008_profile_add_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'facebook_link', $this->string(100)->notNull()->defaultValue(''));
        $this->addColumn('{{%profile}}', 'status', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->createIndex(
            'idx-status',
            '{{%profile}}',
            'status'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'status');
        $this->dropColumn('{{%profile}}', 'facebook_link');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210127_162008_profile_add_status cannot be reverted.\n";

        return false;
    }
    */
}
