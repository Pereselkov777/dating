<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%agency}}`.
 */
class m210304_175601_create_agency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%agency}}', [
            'id' => Schema::TYPE_PK,
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%agency}}');
    }
}
