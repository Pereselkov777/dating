<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%profile}}`.
 */
class m210303_173505_add_lang_2_3_id_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'lang2_id', $this->integer()->null());
        $this->addColumn('{{%profile}}', 'lang3_id', $this->integer()->null());
        $this->createIndex(
            'idx-lang2',
            '{{%profile}}',
            'lang2_id'
        );
        $this->createIndex(
            'idx-lang3',
            '{{%profile}}',
            'lang3_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'lang2_id');
        $this->dropColumn('{{%profile}}', 'lang3_id');
    }
}
