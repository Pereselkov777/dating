<?php

use yii\db\Migration;

/**
 * Class m210120_070435_chat_add_unread_messages
 */
class m210120_070435_chat_add_unread_messages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%chat_message}}', 'is_read', $this->tinyInteger()->unsigned()->notNull()->defaultValue(0));
        $this->addColumn('{{%chat_list}}', 'first_count_unread', $this->smallInteger()->unsigned()->notNull()->defaultValue(0));
        $this->addColumn('{{%chat_list}}', 'second_count_unread', $this->smallInteger()->unsigned()->notNull()->defaultValue(0));
        $this->createIndex(
            'idx-is_read',
            '{{%chat_message}}',
            'is_read'
        );

        $this->createIndex(
            'idx-first_count_unread',
            '{{%chat_list}}',
            'first_count_unread'
        );

        $this->createIndex(
            'idx-second_count_unread',
            '{{%chat_list}}',
            'second_count_unread'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%chat_message}}', 'is_read');
        $this->dropColumn('{{%chat_list}}', 'first_count_unread');
        $this->dropColumn('{{%chat_list}}', 'second_count_unread');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210120_070435_chat_add_unread_messages cannot be reverted.\n";

        return false;
    }
    */
}
