<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%profile_tag}}`.
 */
class m210320_071220_add_slug_column_to_profile_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_tag}}', 'slug',  $this->string(255)->notNull()->comment('Слаг'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_tag}}', 'slug');
    }
}
