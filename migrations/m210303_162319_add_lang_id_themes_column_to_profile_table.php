<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%profile}}`.
 */
class m210303_162319_add_lang_id_themes_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'lang_id', $this->integer()->null());
        $this->addColumn('{{%profile}}', 'themes', $this->text()->null());
        $this->createIndex(
            'idx-lang',
            '{{%profile}}',
            'lang_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'lang_id');
        $this->dropColumn('{{%profile}}', 'themes');
    }
}
