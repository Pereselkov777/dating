<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%profile_tag}}`.
 */
class m210319_153436_create_profile_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%profile_tag}}', [
            'id' => $this->primaryKey(),
            'title' =>  $this->string(255)->notNull()->unique()->comment('Название'),
            'created_at'=>$this->dateTime()->notNull()->comment('Создано'),
        ]);
        $this->createIndex(
            'idx-profile_tag-title',
            'profile_tag',
            'title'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_tag}}');
    }
}
