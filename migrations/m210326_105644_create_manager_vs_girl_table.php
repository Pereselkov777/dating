<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%manager_vs_girl}}`.
 */
class m210326_105644_create_manager_vs_girl_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%manager_vs_girl}}', [
            'manager_id' => $this->integer()->unsigned()->notNull(),
            'girl_id' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->addPrimaryKey('manager_vs_girl_pk', 'manager_vs_girl', ['manager_id', 'girl_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%manager_vs_girl}}');
    }
}
