<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%profile}}`.
 */
class m210109_115858_profile_add_anket_fields extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'first_name', $this->string(255)->notNull()->defaultValue(''));
        $this->addColumn('{{%profile}}', 'last_name', $this->string(255)->notNull()->defaultValue(''));
        $this->addColumn('{{%profile}}', 'birthday', $this->date()->notNull()->defaultValue('1990-01-01'));
        $this->addColumn('{{%profile}}', 'zodiac', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'sex', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'language', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'about_me', $this->text());
        $this->addColumn('{{%profile}}', 'about_ideal', $this->text());
        $this->addColumn('{{%profile}}', 'age_from', $this->tinyInteger(2)->notNull()->unsigned()->defaultValue('18'));
        $this->addColumn('{{%profile}}', 'age_to', $this->tinyInteger(2)->notNull()->unsigned()->defaultValue('80'));
        $this->addColumn('{{%profile}}', 'height', $this->smallInteger(3)->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'weight', $this->smallInteger(3)->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'body_type', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'hair_type', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'hair_color', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'eyes_color', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'nationality', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'country_id', $this->integer()->null()->unsigned());
        $this->addColumn('{{%profile}}', 'region_id', $this->integer()->null()->unsigned());
        $this->addColumn('{{%profile}}', 'city_id', $this->integer()->null()->unsigned());
        $this->addColumn('{{%profile}}', 'occupation', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'religion', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'children_count', $this->tinyInteger()->null()->unsigned());
        $this->addColumn('{{%profile}}', 'marital_status', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'drinking', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
        $this->addColumn('{{%profile}}', 'smoking', $this->tinyInteger()->notNull()->unsigned()->defaultValue('0'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'first_name');
        $this->dropColumn('{{%profile}}', 'last_name');
        $this->dropColumn('{{%profile}}', 'birthday');
        $this->dropColumn('{{%profile}}', 'zodiac');
        $this->dropColumn('{{%profile}}', 'sex');
        $this->dropColumn('{{%profile}}', 'language');
        $this->dropColumn('{{%profile}}', 'about_me');
        $this->dropColumn('{{%profile}}', 'about_ideal');
        $this->dropColumn('{{%profile}}', 'age_from');
        $this->dropColumn('{{%profile}}', 'age_to');
        $this->dropColumn('{{%profile}}', 'height');
        $this->dropColumn('{{%profile}}', 'weight');
        $this->dropColumn('{{%profile}}', 'body_type');
        $this->dropColumn('{{%profile}}', 'hair_type');
        $this->dropColumn('{{%profile}}', 'hair_color');
        $this->dropColumn('{{%profile}}', 'eyes_color');
        $this->dropColumn('{{%profile}}', 'nationality');
        $this->dropColumn('{{%profile}}', 'country_id');
        $this->dropColumn('{{%profile}}', 'region_id');
        $this->dropColumn('{{%profile}}', 'city_id');
        $this->dropColumn('{{%profile}}', 'occupation');
        $this->dropColumn('{{%profile}}', 'religion');
        $this->dropColumn('{{%profile}}', 'children_count');
        $this->dropColumn('{{%profile}}', 'marital_status');
        $this->dropColumn('{{%profile}}', 'drinking');
        $this->dropColumn('{{%profile}}', 'smoking');
    }
}
