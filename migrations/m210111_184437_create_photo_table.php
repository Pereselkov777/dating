<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%photo}}`.
 */
class m210111_184437_create_photo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%photo}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull()->comment('Пользователь'),
            'title' => $this->string(255)->notNull()->comment('Название'),
            'img' => $this->string(255)->notNull()->comment('Изображение'),
            'status' => $this->tinyInteger()->unsigned()->notNull()->comment('Статус'),
            'create_time' => $this->dateTime()->notNull()->comment('Создано'),
            'update_time' => $this->dateTime()->notNull()->comment('Обновлено'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%photo}}');
    }
}
