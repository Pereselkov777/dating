<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%profile_via_tag}}`.
 */
class m210319_172203_create_profile_via_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       
        $this->createTable('{{%profile_via_tag}}', [
            'profile_id' => $this->integer()->unsigned()->notNull(),
            'tag_id' => $this->integer()->unsigned()->notNull(),
        ]);
        $this->addPrimaryKey('profile_via_tag_pk', 'profile_via_tag', ['profile_id', 'tag_id']);

        $this->createIndex(
            'idx-profile_via_tag-profile_id',
            'profile_via_tag',
            'profile_id'
        );
        $this->createIndex(
            'idx-profile_via_tag-tag_id',
            'profile_via_tag',
            'tag_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%profile_via_tag}}');
    }
}



