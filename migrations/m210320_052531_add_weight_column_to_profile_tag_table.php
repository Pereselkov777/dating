<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%profile_tag}}`.
 */
class m210320_052531_add_weight_column_to_profile_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile_tag}}', 'weight', $this->integer()->null()->comment('Частота'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile_tag}}', 'weight');
    }
}
