<?php

use yii\db\Migration;

/**
 * Class m210120_065813_profile_add_last_active
 */
class m210120_065813_profile_add_last_active extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'last_active', $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'));
        $this->createIndex(
            'idx-last_active',
            '{{%profile}}',
            'last_active'
        );
        $this->createIndex(
            'idx-sex',
            '{{%profile}}',
            'sex'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'last_active');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210120_065813_profile_add_last_active cannot be reverted.\n";

        return false;
    }
    */
}
