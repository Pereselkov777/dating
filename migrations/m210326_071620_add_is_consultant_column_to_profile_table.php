<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%profile}}`.
 */
class m210326_071620_add_is_consultant_column_to_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'is_consultant', $this->tinyInteger()->notNull()->defaultValue(0)); 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
