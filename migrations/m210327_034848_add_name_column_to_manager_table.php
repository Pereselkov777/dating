<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%manager}}`.
 */
class m210327_034848_add_name_column_to_manager_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%manager}}', 'name', $this->string(12)->notNull());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
