<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chat_list}}`.
 */
class m210113_142239_create_chat_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chat_list}}', [
            'id' => $this->primaryKey(),
            'first_id' => $this->integer()->unsigned()->notNull(),
            'second_id' => $this->integer()->unsigned()->notNull(),
            'sender_id' => $this->integer()->unsigned()->notNull(),
            'message' => $this->string(255)->notNull(),
            'is_photo' => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_video' => $this->tinyInteger()->notNull()->defaultValue(0),
            'updated_at' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
        ]);

        $this->createIndex(
            'idx-first_second',
            '{{%chat_list}}',
            ['first_id', 'second_id'],
            true
        );

        $this->createIndex(
            'idx-updated_at',
            '{{%chat_list}}',
            'updated_at'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-first_second',
            '{{%chat_list}}'
        );

        $this->dropIndex(
            'idx-updated_at',
            '{{%chat_list}}'
        );

        $this->dropTable('{{%chat_list}}');
    }
}
