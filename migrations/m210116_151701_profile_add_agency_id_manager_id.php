<?php

use yii\db\Migration;

/**
 * Class m210116_151701_profile_add_agency_id_manager_id
 */
class m210116_151701_profile_add_agency_id_manager_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'agency_id', $this->integer()->unsigned()->notNull()->defaultValue(0));
        $this->addColumn('{{%profile}}', 'manager_id', $this->integer()->unsigned()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'agency_id');
        $this->dropColumn('{{%profile}}', 'manager_id');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210116_151701_profile_add_agency_id_manager_id cannot be reverted.\n";

        return false;
    }
    */
}
