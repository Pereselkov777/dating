<?php

use yii\db\Migration;

/**
 * Class m210307_172155_add_kind_profile
 */
class m210307_172155_add_kind_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profile}}', 'kind_id', $this->tinyInteger()->notNull()->defaultValue('1'));
        $this->createIndex(
            'idx-kind_id',
            '{{%profile}}',
            'kind_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profile}}', 'kind_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210307_172155_add_kind_profile cannot be reverted.\n";

        return false;
    }
    */
}
