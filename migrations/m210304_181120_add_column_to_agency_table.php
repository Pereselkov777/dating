<?php

use yii\db\Migration;


/**
 * Handles adding columns to table `{{%agency}}`.
 */
class m210304_181120_add_column_to_agency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%agency}}', 'name', $this->string(12)->notNull()->unique());
        $this->addColumn('{{%agency}}', 'percent', $this->decimal(10,2)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%agency}}', 'name');
        $this->dropColumn('{{%agency}}', '');
    }
}
