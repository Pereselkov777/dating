<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chat_message}}`.
 */
class m210113_142309_create_chat_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chat_message}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->unsigned()->notNull(),
            'from_id' => $this->integer()->unsigned()->notNull(),
            'to_id' => $this->integer()->unsigned()->notNull(),
            'message' => $this->text()->notNull(),
            'photo' => $this->string(255)->null(),
            'video' => $this->string(255)->null(),
            'created_at' => $this->dateTime()->notNull(),
        ]);

        $this->createIndex(
            'idx-parent_id',
            '{{%chat_message}}',
            'parent_id'
        );

        $this->createIndex(
            'idx-from_to',
            '{{%chat_message}}',
            ['from_id', 'to_id']
        );

        $this->createIndex(
            'idx-created_at',
            '{{%chat_message}}',
            'created_at'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-parent_id',
            '{{%chat_message}}'
        );

        $this->dropIndex(
            'idx-from_to',
            '{{%chat_message}}'
        );

        $this->dropIndex(
            'idx-created_at',
            '{{%chat_message}}'
        );

        $this->dropTable('{{%chat_message}}');
    }
}
