<?php
namespace app\widgets;

use Yii;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * Yii::$app->session->setFlash('error', 'This is the message');
 * Yii::$app->session->setFlash('success', 'This is the message');
 * Yii::$app->session->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * Yii::$app->session->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @author Alexander Makarov <sam@rmcreative.ru>
 */
class Menu extends \yii\bootstrap4\Widget
{
    public $items = [];


    /**
     * {@inheritdoc}
     */
    public function run()
    {
   //     $session = Yii::$app->session;
   //     $flashes = $session->getAllFlashes();
       // $appendClass = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        echo '<ul class="rd-navbar-nav">'. "\n";

        //print_r($this->items); exit;
        foreach ($this->items as $item) {
            $link = isset($item['url']) ? Yii::$app->urlManager->createUrl($item['url']) : '#';
            if (in_array($link, ['/', '/ru', '/de'])) {
                $active = Yii::$app->request->url == $link ? true : false;
            } else {
                $active = (stripos(Yii::$app->request->url, $link) === 0 ? true : false);
            }
            if (!isset($item['visible']) || (isset($item['visible']) && $item['visible'])) {
                echo '<li class="rd-nav-item' . ($active ? ' active' : '') . '">' . "\n";
                echo '<a class="rd-nav-link" href="' . $link . '">' . $item['label'] . '</a>' . "\n";
                if (isset($item['items'])) {
                    echo '<ul class="rd-menu rd-navbar-dropdown">' . "\n";
                    foreach ($item['items'] as $subitem) {
                        if (!isset($subitem['visible']) || (isset($subitem['visible']) && $subitem['visible'])) {
                            $link = isset($subitem['url']) ? Yii::$app->urlManager->createUrl($subitem['url']) : '#';
                            $active = (stripos(Yii::$app->request->url, $link) === 0 ? true : false);
                            echo '<li class="rd-dropdown-item' . ($active ? ' active' : '') . '">' . "\n";
                            $linkOptions = '';
                            if (isset($subitem['linkOptions'])) {
                                foreach ($subitem['linkOptions'] as $name=>$option) {
                                    $linkOptions .= ' ' . $name .'="'.$option.'"';
                                }
                            }
                            echo '<a class="rd-dropdown-link" '.$linkOptions.' href="' . $link . '">' . $subitem['label'] . '</a>' . "\n";
                        }
                    }

                    echo '</ul>';
                }
                echo '</li>' . "\n";
            }
        }
        echo '</ul>'."\n";
    }
}
