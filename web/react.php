<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Hello World</title>
<body>
<div id="like_button_container"></div>


<!-- Загрузим React. -->
<!-- Примечание: при деплое на продакшен замените «development.js» на «production.min.js». -->
<script src="https://unpkg.com/react@17/umd/react.development.js" crossorigin></script>
<script src="https://unpkg.com/react-dom@17/umd/react-dom.development.js" crossorigin></script>

<!-- Загрузим наш React-компонент. -->
<script type="module" src="js/like_button.js"></script>
<script type="module" src="js/App.js"></script>

</body>
</html>