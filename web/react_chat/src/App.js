import React, { Component } from "react";
import Subscribe from "./client";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      access: "",
    };
    Subscribe((err) =>
      this.setState({
        access: Subscribe.access,
      })
    );
  }

  render() {
    return <div>{this.state.access}</div>;
  }
}

export default App;
