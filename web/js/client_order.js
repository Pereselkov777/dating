$(document).ready(function () {
  $('.js-btn-buy-photo').click(function () {
      var elem = $(this);
      var private_photo_id = elem.data('id');
      $.ajax({
          url: '/client-order/buy-photo?private_photo_id=' + private_photo_id, // point to server-side PHP script
          cache: false,
          contentType: false,
          processData: false,
          dataType: 'JSON',
          type: 'get',
          success: function(data){
              console.log('data', data);
              if (data.result == 'success') {
                elem.closest('.private-photo-buy-panel').hide();
                var photo_item = elem.closest('.album-photo-item');
                  photo_item.removeClass('private-photo-no-access')
                    .find('.thumbnail-light').attr('href', data.full)
                    .click()
                    .find('img').attr('src', data.prev);
              } else {
                  alert(data.message);
              }
          }
      });
  });
});