window.Client =
  window.Client ||
  (function (config) {
    "use strict";
    var //windowObject = clientWindowObject,
      user_id = 0,
      user_name = "",
      parent_id = 0,
      access_token = "",
      avatar = "";

    /*if (typeof c !== null) {
      console.log("no null");
      console.log($("#chat_messages_container"), "CH");
      //$("#chat_messages_container").display = "block";
      /* this = window.Client;
      console.log(this, "draw");
    }*/

    return {
      socket: null,
      interlocutor: {},
      page: 1,

      safe: function (str) {
        return str
          .replace(/&/g, "&amp;")
          .replace(/</g, "&lt;")
          .replace(/>/g, "&gt;");
      },

      msg_system: function (message) {
        var m =
          '<div class="message-item"><div class="message-system">' +
          this.safe(message) +
          '</div><div class="clearfix"></div>';
        this.messages.prepend(m).scrollTop(this.messages[0].scrollHeight);
      },

      define_chat: function (socket) {
        var client = this;
        $("#send, #send_mess").click(function () {
          var message = $("#chat").val();
          if (message == "") {
            message = $("#chat_text_message").val();
          }
          if (message == "") {
            //document.getElementById("send_mess").disabled = true;
            // alert("Сообщение пустое!");
          }
          var to_id;
          if ("user_id" in client.interlocutor) {
            to_id = client.interlocutor["user_id"];
          } else {
            to_id = $("#to_id").val();
          }
          console.log("to_id", to_id);
          if ($("#chat_text_message").val() != "") {
            console.log($("#chat_text_message").val(), "НЕ пустое");
            socket.emit("chat_message", {
              access_token: client.access_token,
              user_id: client.user_id,
              to_id: to_id,
              message: message,
            });
            console.log("Я произошло");
            if ($("#chat_messages_container").hasClass("only")) {
              console.log("INVISIBLE");
            }
            $("#chat_text_message").val("");
          } else {
            console.log(document.getElementById("send_mess"), "this");
            alert("Empty message!");
          }

          setTimeout(function () {
            $("#chat-messages").scrollTop($("#chat-messages")[0].scrollHeight);
          }, 1000);
          return false;
        });

        $("#list").click(function () {
          var to_id = $("#to_id").val();
          socket.emit("load_chat_list", {
            access_token: client.access_token,
            user_id: client.user_id,
            to_id: to_id,
          });
          return false;
        });

        $("#messages").click(function () {
          console.log("parent_id", window.Client.parent_id);
          if (client.parent_id) {
            socket.emit("load_chat_messages", {
              access_token: client.access_token,
              parent_id: client.parent_id,
              user_id: client.user_id,
            });
          }
          return false;
        });

        $("#current").click(function () {
          console.log("DATA", window.Client);
          var to_id = $("#to_id").val();
          client.socket.emit("get_chat_by_to_id", {
            access_token: client.access_token,
            user_id: client.user_id,
            to_id: to_id,
          });
          return false;
        });

        var timeout;

        /*$("#chat-messages")
          .scroll(function () {
            var me = this;

            if (timeout) clearTimeout(timeout);
            timeout = setTimeout(function () {
              $(me)
                .find(".bg-light")
                .each(function () {
                  var scrollTop = $(me).scrollTop(),
                    posTop = $(this).position().top,
                    message = this;

                  setTimeout(function () {
                    if (
                      posTop >= scrollTop &&
                      posTop <= scrollTop + $(me).height()
                    ) {
                      $(message).removeClass("bg-light");
                    }
                  }, 1500);
                });
            }, 100);
          })
          .scrollTop(150);*/

        $("#chat-messages").on("scroll", function () {
          if (client.parent_id) {
            var height = $(this).scrollTop();

            if (height <= 1) {
              client.page = client.page + 1;
              console.log("Event Scroll!");
              client.socket.emit("load_chat_messages", {
                access_token: client.access_token,
                parent_id: client.parent_id,
                user_id: client.user_id,
                page: client.page,
              });
            }
          }
        });
        $("#chat_text_message").keyup(function (event) {
          if (event.keyCode === 13 && !event.shiftKey) {
            console.log("Click");
            $("#send_mess").click();
            event.preventDefault();
          }
        });
      },
      goto_chat_list(page) {
        var client = this;
        client.socket.emit("load_chat_list", {
          access_token: client.access_token,
          user_id: client.user_id,
          page: page,
        });
      },
      draw_pagination(page, max_page, fn_name) {
        var html = '<ul class="pagination justify-content-center">';
        var prev_page = page - 1;
        html =
          html +
          '<li class="page-item' +
          (prev_page > 0 ? "" : " disabled") +
          '">\n' +
          ' <a class="page-link" href="#" aria-label="Previous"  onclick="' +
          (prev_page > 0 ? fn_name + "(" + prev_page + "); " : "") +
          'return false;">\n' +
          '      <span aria-hidden="true">&laquo;</span>\n' +
          '      <span class="sr-only">Previous</span>\n' +
          "</a>\n" +
          "</li>\n";
        for (var i = 1; i <= Number(max_page); i++) {
          html =
            html + '<li class="page-item' + (page == i ? " active" : "") + '">';
          html =
            html +
            '<a class="page-link" href="#" onclick="' +
            fn_name +
            "(" +
            i +
            '); return false;">' +
            i +
            "</a>";
          html = html + "</li>";
        }
        var next_page = page + 1;
        html =
          html +
          '<li class="page-item' +
          (next_page < max_page ? "" : " disabled") +
          '">\n' +
          ' <a class="page-link" href="#" aria-label="Next" onclick="' +
          (next_page < max_page ? fn_name + "(" + next_page + "); " : "") +
          ' return false;">\n' +
          '      <span aria-hidden="true">&raquo;</span>\n' +
          '      <span class="sr-only">Next</span>\n' +
          "</a>\n" +
          "</li>\n";
        html = html + "</ul>";
        return html;
      },
      /*soundClick() {
        console.log("HERE SOUND");
        var audio = new Audio();
        audio.src = "web/js/hutch_hd_sfx_2014_0243.mp3";

        return function () {
          console.log("Sound");
          audio.play();
        };
      },*/
      /*soundClick() {
        var audioElement = document.createElement("audio");
        audioElement.setAttribute("src", "web/js/hutch_hd_sfx_2014_0243");
        audioElement.load();
        audioElement.addEventListener(
          "load",
          function () {
            audioElement.play();
          },
          true
        );
      },*/
      playSound(url) {
        console.log("SOUND!");
        const audio = new Audio(url);
        audio.play();
      },
      draw_chat_list_item(message) {
        console.log(message, "Draw");
        console.log("TEST");
        var client = this;
        var fio = message["first_name"] + " " + message["last_name"];

        if (message["photo"] != "") {
          console.log(message["photo"], "AV");
        } else {
          console.log("NULL");
        }

        var avatar = message["photo"];
        if (message["last_active"]) {
          var last_online =
            message["last_active"].replace(" ", "T") + server_tz;
          if (last_online == "a few seconds ago") {
            last_online =
              '<i class="fa fa-circle chat-online" aria-hidden="true"></i> Online';
          }
        }

        var prefix = "";
        var prefix_rec = "";
        if (client.user_id == message["first_id"]) {
          prefix = "first_";
          prefix_rec = "second_";
        } else {
          prefix = "second_";
          prefix_rec = "first_";
        }
        console.log(message, "MESSAGE");
        var count_unread = Number(message[prefix + "count_unread"]);
        console.log(prefix, "PREF");
        console.log(count_unread, "un");
        console.log(
          Number(message[prefix + "count_unread"]),
          "M in draw_chat_list_item"
        );

        console.log(window.Client.draw_chat_list_item, "WW");
        var parent_id =
          "parent_id" in message ? message["parent_id"] : message["id"];
        var user_id =
          "parent_id" in message
            ? message[prefix_rec + "id"]
            : message["user_id"];
        if (avatar != "") {
          var html =
            '<a href="#" data-parent="' +
            parent_id +
            '" ' +
            ' data-user="' +
            user_id +
            '" ' +
            'onclick="window.Client.goto_chat_history(this)" class="list-group-item list-group-item-action border-0">\n' +
            '<div class="badge bg-success float-right js-count-new"' +
            (count_unread ? "" : 'style="display:none"') +
            ">" +
            count_unread +
            "</div>\n" +
            '     <div class="d-flex align-items-start">\n' +
            '          <img src="' +
            avatar +
            '" class="rounded-circle mr-1" alt="' +
            fio +
            '" width="30" height="40">\n' +
            '          <div class="flex-grow-1 ml-3">\n' +
            "            " +
            fio +
            "\n" +
            '             <div class="small js-last-active">' +
            last_online +
            "</div>\n" +
            "          </div>\n" +
            "     </div>\n" +
            " </a>\n";
        } else {
          var html =
            '<a href="#" data-parent="' +
            message["id"] +
            '" ' +
            ' data-user="' +
            message["user_id"] +
            '" ' +
            'onclick="window.Client.goto_chat_history(this)" class="list-group-item list-group-item-action border-0">\n' +
            '<div  class="badge bg-success float-right js-count-new" ' +
            (count_unread ? "" : 'style="display:none"') +
            ">" +
            count_unread +
            "</div>\n" +
            '     <div class="d-flex align-items-start">\n' +
            '          <img src="https://offvkontakte.ru/wp-content/uploads/avatarka-pustaya-vk_0.jpg" class="rounded-circle mr-1" alt="' +
            fio +
            '" width="30" height="40">\n' +
            '          <div class="flex-grow-1 ml-3">\n' +
            "            " +
            fio +
            "\n" +
            '             <div class="small js-last-active">' +
            last_online +
            "</div>\n" +
            "          </div>\n" +
            "     </div>\n" +
            " </a>\n";
        }
        if (count_unread > 0) {
          console.log(html, "HERE HTML");
          var elements = document.getElementById("chat-message");

          console.log($("#chat_list"), "HERE");
          //console.log($("#chat_list .list-group-item"), "WHAT");//нужен badge bg-success float-right js-count-new чтобы у него удалить js-count-new как его получить АААААА
          console.log(document.querySelectorAll(".badge"), "B");
        }

        return html;
      },
      draw_chat_list: function (messages) {
        //console.log("draw_chat_list");
        //$("#chat_list").addClass("only_chat");
        //console.log($("#chat_messages_container"), "MESS");
        //console.log(window.Client.draw_chat_list,'d');
        $("#chat_messages_container").addClass("only");
        $("#chat_list_pagination").addClass("only");
        var client = this;
        var html = "";
        for (var key_m in messages) {
          var message = messages[key_m];
          html = html + client.draw_chat_list_item(message);
        }
        $("#chat_list").empty().html(html);
      },
      draw_chat_message: function (message) {
        console.log("HERE");
        var client = this;
        var sender_id = message["from_id"];
        var fio =
          sender_id == client.user_id
            ? client.user_name
            : client.interlocutor["name"];
        var avatar =
          sender_id == client.user_id
            ? client.avatar
            : client.interlocutor["avatar"];
        var side_class = sender_id == client.user_id ? "right" : "left";
        var created_at = message["created_at"].replace(" ", "T") + server_tz;
        created_at = moment(created_at).format("DD.MM.YYYY HH:mm");
        if (avatar != "") {
          var html =
            '<div class="chat-message-' +
            side_class +
            ' pb-4">\n' +
            "<div>\n" +
            '     <img src="' +
            avatar +
            '" class="rounded-circle mr-1" alt="+fio+" width="30" height="40">\n' +
            '     <div class="text-muted small text-nowrap mt-2">' +
            created_at +
            "</div>\n" +
            "      </div>\n" +
            '     <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">\n' +
            '     <div class="font-weight-bold mb-1">' +
            fio +
            "</div>\n" +
            "     " +
            message["message"] +
            "    </div>\n" +
            "</div>";
        } else {
          var html =
            '<div class="chat-message-' +
            side_class +
            ' pb-4">\n' +
            "<div>\n" +
            '     <img src="https://offvkontakte.ru/wp-content/uploads/avatarka-pustaya-vk_0.jpg" class="rounded-circle mr-1" alt="+fio+" width="30" height="40">\n' +
            '     <div class="text-muted small text-nowrap mt-2">' +
            created_at +
            "</div>\n" +
            "      </div>\n" +
            '     <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">\n' +
            '     <div class="font-weight-bold mb-1">' +
            fio +
            "</div>\n" +
            "     " +
            message["message"] +
            "    </div>\n" +
            "</div>";
        }

        $("#chat-messages").append(html);
      },

      goto_chat_history: function (elem) {
        console.log("ENter");
        // If the inner width of the window is greater then 768px
        console.log("goto");
        $("#chat_messages_container").removeClass("only");
        $("#chat_list").addClass("only");

        /*function checkMediaQuery() {
          console.log("START");

          if (window.innerWidth <= 768) {
            console.log("NOW");
            $("#chat_list").hide();
            $("#chat_list_pagination").hide();
            // Then log this message to the conso)le
          }

          if (window.innerWidth > 768) {
            console.log("больше");
            if (($("#chat_list").display = "none")) {
              console.log("HERE");
            }
            $("#chat_list").show();
            // Then log this message to the conso)le
          }
        }*/

        // Add a listener for when the window resizes
        //window.addEventListener("resize", checkMediaQuery);
        var el = $(elem);
        var client = this;
        client.parent_id = el.data("parent");
        console.log($("#chat_list"), "CHATLIST");
        $("#chat_list .list-group-item").removeClass("active");
        el.addClass("active");
        console.log("parent_id", el.data("parent"));
        if (client.parent_id) {
          client.interlocutor["user_id"] = el.data("user");
          client.interlocutor["name"] = el.find("img").attr("alt");
          client.interlocutor["avatar"] = el.find("img").attr("src");
          var last_online = el.find(".js-last-active").text();
          $("#interlocutor_last_active").text(last_online);
          window.history.pushState(
            "chat",
            "Chat - " + client.interlocutor["name"],
            "/chat?user_id=" + client.interlocutor["user_id"]
          );
          $("#chat_messages_container").show();
          $("#interlocutor_avatar")
            .attr("src", client.interlocutor["avatar"])
            .attr("alt", client.interlocutor["name"]);
          $("#interlocutor_name")
            .text(client.interlocutor["name"])
            .attr("href", "/user/" + client.interlocutor["user_id"]);
          $("#chat-messages").empty();
          client.page = 1;
          client.socket.emit("load_chat_messages", {
            access_token: client.access_token,
            parent_id: client.parent_id,
            user_id: client.user_id,
            to_id: client.interlocutor["user_id"],
            page: client.page,
          });
        }
      },

      persSelect: function (pers_id, location_id) {
        this.socket.emit("loadLocation", {
          pers_id: pers_id,
          location_id: location_id,
        });
        console.log(
          "send load location... pers_id=" +
            pers_id +
            " location_id=" +
            location_id
        );
      },

      gotoKoord: function (pers_id, x, y, j, i) {
        this.socket.emit("gotoKoord", {
          pers_id: pers_id,
          x: x,
          y: y,
          j: j,
          i: i,
        });
        console.log("send goto koord pers_id" + pers_id + "x=" + x + "y=" + y);
      },

      logout: function () {
        this.socket.disconnect();
        console.log("соединение разорвано");
      },

      init: function (
        self_user_id,
        self_user_name,
        self_parent_id,
        self_access_token,
        self_avatar
      ) {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var c = url.searchParams.get("user_id");
        this.user_id = self_user_id;
        this.user_name = self_user_name;
        this.parent_id = self_parent_id;
        this.access_token = self_access_token;
        this.avatar = self_avatar;
        console.log("init=" + self_user_id);
        // $("#chat-info").removeClass("info");
        $("#btn-info").click(function () {
          console.log("remove");
          $("#chat_list").removeClass("only");
          $("#chat_messages_container").addClass("only");
          $("#chat-info").addClass("info");
          return false;
        });
        var client = this;
        //            this.socket = new WebSocket('ws://mif.mysite:8124');
        //this.socket = io.connect(chat_host);
        //            this.socket = io.connect(game_host, {wsEngine: 'ws', forceNew:true, 'multiplex':false });
        this.socket = io.connect(chat_host, {
          transports: ["websocket"],
          upgrade: false,
        });
        var socket = this.socket;
        var is_chat =
          window.location.pathname == "/chat" ||
          window.location.pathname == "/chat/index";

        socket.on("connecting", function () {
          console.log("connecting!");
        });

        socket.on("connect", function () {
          console.log("send connect_chat");
          socket.emit("connect_chat", {
            name: client.user_name,
            access_token: client.access_token,
            user_id: client.user_id,
          });
          //            socket.emit('new-player', {'access_token': access_token });
        });

        socket.on("disconnect", function (data) {
          console.log("SOCKET DISCONNECTED!");
        });

        socket.on("connect_chat", function (data) {
          console.log("connect_chat", data);
        });

        socket.on("error_message", function (data) {
          console.log("error_message", data);
          alert(data.message);
        });

        socket.on("chat_message", function (data) {
          console.log("chat_message", data);
          var message = data;
          console.log(data, "DATA");

          //client.soundClick(); //ну и почему не играет
          if (client.user_id == data.to_id) {
            client.playSound(
              "https://audiokaif.ru/wp-content/uploads/2019/04/1-%D0%97%D0%B2%D1%83%D0%BA-%D1%83%D0%B2%D0%B5%D0%B4%D0%BE%D0%BC%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BD%D0%B0-iphone.mp3"
            );
          }
          if (
            client.parent_id &&
            client.parent_id == Number(message["parent_id"])
          ) {
            console.log("р", client.parent_id);
            console.log(client.user_id, "USER");
            console.log(data.to_id, "TO");
            client.draw_chat_message(message);

            //client.soundClick();
          } else {
            var el_list = $(
              '#chat_list a[data-parent="' + Number(message["parent_id"]) + '"]'
            );
            if (!client.parent_id && client.user_id == message["from_id"]) {
              client.parent_id = Number(message["parent_id"]);
              console.log("F", client.parent_id);
              client.draw_chat_message(message);
              if (!el_list.length) {
                client.goto_chat_list(1);
              }
            } else {
              if (el_list.length) {
                var count_new_el = el_list.find(".js-count-new");
                console.log("count_new_el", count_new_el);
                count_new_el.text(Number(count_new_el.text()) + 1);
                if (!count_new_el.is(":visible")) {
                  count_new_el.show();
                }
              } else {
                var html = client.draw_chat_list_item(message);
                $("#chat_list").prepend(html);
              }
            }
          }
          if ("credit" in message) {
            $("#chat_credit").text(message["credit"]);
          }
        });

        socket.on("load_chat_list", function (data) {
          console.log("load_chat_list", data);
          client.draw_chat_list(data["rows"]);
          if (c === null) {
            console.log(c === null, "CASE 1"); // в url нет user_id; показать контакты скрвть сообщения
            $("#chat_list").removeClass("only");
          }
          if (c !== null) {
            $("#chat_list").addClass("only"); // url есть user_id показать сообщения скрыть контакты
            $("#chat_messages_container").removeClass("only");
            console.log(c, "CASE 2");
          }

          /* if (typeof c !== null) {
            function contains(arr) {
              for (var i = 0; i < arr.length; i++) {
                console.log(arr[i].user_id, "Arr");
                if (arr[i].user_id === c) {
                  return true; //вернет true если в списке уже есть id из url
                }
              }
              return false; //false если нету
            }
            if (contains(data["rows"]) === false) {
              console.log("this case");
              $("#chat_list").addClass("only");
              $("#chat_messages_container").removeClass("only");
            }
          }*/
          $("#chat_list_pagination")
            .empty()
            .html(
              client.draw_pagination(
                data["page"],
                data["max_page"],
                "window.Client.goto_chat_list"
              )
            );
        });

        socket.on("load_chat_messages", function (data) {
          console.log("load_chat_messages", data);
          console.log(data, "DAta in load_chat_messages");
          console.log("in", client.interlocutor);
          if ("user_id" in client.interlocutor) {
            var html = "";
            for (var key_m in data["rows"]) {
              var message = data["rows"][key_m];
              //console.log(message, "MESS");
              var sender_id = message["from_id"];
              var created_at =
                message["created_at"].replace(" ", "T") + server_tz;
              created_at = moment(created_at).format("DD.MM.YYYY HH:mm");
              var fio =
                sender_id == client.user_id
                  ? client.user_name
                  : client.interlocutor["name"];
              var avatar =
                sender_id == client.user_id
                  ? client.avatar
                  : client.interlocutor["avatar"];
              var side_class = sender_id == client.user_id ? "right" : "left";
              $("#chat-info").removeClass("info");
              var info =
                '<img src="' +
                client.interlocutor["avatar"] +
                '" class="rounded-circle mr-1 mb2" alt="+fio+" width="30" height="40">\n' +
                "<div>" +
                client.interlocutor["name"] +
                "</div>";
              $("#av").html(info);
              if (message.is_read == 1) {
                console.log(message.is_read, "ISs");
                html =
                  '<div class="chat-message-' +
                  side_class +
                  ' pb-4">\n' +
                  "<div>\n" +
                  '     <img src="' +
                  avatar +
                  '" class="rounded-circle mr-1" alt="+fio+" width="30" height="40">\n' +
                  '     <div class="text-muted small text-nowrap mt-2">' +
                  created_at +
                  "</div>\n" +
                  "      </div>\n" +
                  '     <div class="flex-shrink-1 rounded py-2 px-3 mr-3">\n' +
                  '     <div class="font-weight-bold mb-1">' +
                  fio +
                  "</div>\n" +
                  "     " +
                  message["message"] +
                  "    </div>\n" +
                  "</div>" +
                  html;
              } else {
                console.log(message.is_read, "ISs");
                html =
                  '<div class="chat-message-' +
                  side_class +
                  ' pb-4">\n' +
                  "<div>\n" +
                  '     <img src="' +
                  avatar +
                  '" class="rounded-circle mr-1" alt="+fio+" width="30" height="40">\n' +
                  '     <div class="text-muted small text-nowrap mt-2">' +
                  created_at +
                  "</div>\n" +
                  "      </div>\n" +
                  '     <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">\n' +
                  '     <div class="font-weight-bold mb-1">' +
                  fio +
                  "</div>\n" +
                  "     " +
                  message["message"] +
                  "    </div>\n" +
                  "</div>" +
                  html;
              }
            }
            //console.log(html, "HTML");
            $("#chat-messages").prepend(html);
            if (data["page"] == 1) {
              $("#chat-messages").scrollTop(
                $("#chat-messages")[0].scrollHeight
              );
              $(
                '#chat_list .list-group-item[data-parent="' +
                  client.parent_id +
                  '"] .js-count-new'
              )
                .text(0)
                .hide();
            }
          }
        });

        socket.on("get_chat_by_to_id", function (data) {
          console.log("get_chat_by_to_id", data);
          var online = data.last_active;
          console.log(online, "in get_chat_by_to_id");

          client.parent_id = data["parent_id"];
          client.interlocutor["user_id"] = data["to_id"];
          client.interlocutor["name"] = data["name"];
          client.interlocutor["avatar"] = data["avatar"];
          if (client.parent_id) {
            $("#chat_messages_container").show();
            $("#interlocutor_avatar")
              .attr("src", client.interlocutor["avatar"])
              .attr("alt", client.interlocutor["name"]);
            $("#interlocutor_name")
              .text(client.interlocutor["name"])
              .attr("href", "/user/" + client.interlocutor["user_id"]);
            var last_online = data["last_active"].replace(" ", "T") + server_tz;
            last_online = moment(last_online).fromNow();
            $("#interlocutor_last_active").text(last_online);
            $("#chat-messages").empty();
            client.page = 1;
            client.socket.emit("load_chat_messages", {
              access_token: client.access_token,
              parent_id: client.parent_id,
              user_id: client.user_id,
              to_id: client.interlocutor["user_id"],
              page: client.page,
            });
            setTimeout(function () {
              var el = $(
                '#chat_list .list-group-item[data-parent="' +
                  client.parent_id +
                  '"]'
              );
              console.log("ddddd", el.hasClass("active"));
              if (el.length && !el.hasClass("active")) {
                $("#chat_list .list-group-item").removeClass("active");
                el.addClass("active");
              }
            }, 1000);
          } else {
            $("#chat_messages_container").show();
            $("#interlocutor_avatar")
              .attr("src", client.interlocutor["avatar"])
              .attr("alt", client.interlocutor["name"]);
            $("#interlocutor_name")
              .text(client.interlocutor["name"])
              .attr("href", "/user/" + client.interlocutor["user_id"]);
            client.page = 1;
            $("#chat-messages").empty();
          }
        });

        if (is_chat) {
          this.define_chat(socket);
          var urlParams = new URLSearchParams(window.location.search);
          var interlocutorId = urlParams.get("user_id");
          if (interlocutorId) {
            console.log("get_chat_by_to_id send user_id = ", interlocutorId);
            console.log("socket", socket);
            socket.emit("get_chat_by_to_id", {
              access_token: client.access_token,
              user_id: client.user_id,
              to_id: interlocutorId,
            });
          }
          console.log("load_chat_list sending ");
          client.goto_chat_list(1);
          /* socket.emit("load_chat_list", {
            access_token: client.access_token,
            user_id: client.user_id,
          });*/
        }
      },
    };
  })(window);

window.document.addEventListener("DOMContentLoaded", function (event) {
  "use strict";
  var client = window.Client;
  console.log(window.Client);
  client.init(user_id, user_name, 0, access_token, avatar);
});
