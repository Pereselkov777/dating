<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- формируем блок хедера -->
<?php $this->beginBlock('breadcrumbs-custom'); ?>
<h2 class="breadcrumbs-custom-title">About</h2>
<p class="big">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Duis aute irure dolor in reprehenderit.  </p>
<?php $this->endBlock();?>

        <div class="row row-50 justify-content-center align-items-center">
            <div class="col-md-10 col-lg-7 d-flex flex-column justify-content-center align-items-start">
                <h2 class="text-black block-1">About Us</h2>
                <div class="heading-3 ls-ng-1 offset-xl-t-40 text-gray-600"><span class="d-xl-block">We are here to build emotions, connect people</span><span class="d-xl-block">and create happy stories.</span></div>
                <div class="block-2 mt-20 mt-md-30 mt-lg-40">
                    <p class="text-gray-800 text-sm">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div><a class="button button-lg button-icon button-icon-left button-primary offset-xl-t-45 wow fadeIn" href="#" data-wow-delay=".05s"><span class="icon mdi mdi-magnify"></span>Find your partner</a>
            </div>
            <div class="col-md-10 col-lg-5 text-lg-right text-center"><img src="/themes/agency/images/about-1-457x479.png" alt="" width="457" height="479"/>
            </div>
        </div>
