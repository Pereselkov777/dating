<?php
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$name = $model->first_name . ' ' . $model->last_name;
if (empty(trim($name))) {
    $name = $model->user->username;
}
?>


<!-- Tour 3-->
<article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(<?= $model->photo ? $model->getAvatarUrl() : '/img/no-user.jpg' ?>);">
    <div class="tour-3-inner">
        <div class="tour-3-main">
            <h3 class="tour-3-title"><?= \yii\helpers\Html::a($name, ['/user/profile/show', 'id' => $model->user_id]) ?></h3>
            <ul class="tour-3-meta">
                <li><span class="icon mdi material-design-clock100"></span><span><?= $model->last_active ?></span></li>
            </ul>
        </div>
    </div>
</article>
