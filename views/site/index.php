<?php
use yii\widgets\ListView;
use yii\bootstrap4\Html;
use kartik\select2\Select2;
use app\modules\blog\models\Category;
use app\models\ProfileTag;
use yii\helpers\ArrayHelper;
//use Yii;
use yii\base\Widget;
//use \kartik\widgets\Select2;

$data = [
    "red" => "red",
    "green" => "green",
    "blue" => "blue",
    "orange" => "orange",
    "white" => "white",
    "black" => "black",
    "purple" => "purple",
    "cyan" => "cyan",
    "teal" => "teal"
];


$models = ProfileTag::find()->all();
//$models = ProfileTag::findAll(false);
$tags = ArrayHelper::map($models,'id','title');


//var_dump($tags);exit();

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->name;
Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];

?>

<?php if (!Yii::$app->user->isGuest) {
    $my_sex = Yii::$app->user->identity->profile->sex;
    
    ?>
    <div class="jumbotron-2-block">
        <div class="jumbotron-2-inner">
            <div class="container">
                <?= Html::beginForm(['/'], 'get', ['id' => 'profile_search', 'class' => 'search-form row row-narrow-20 align-items-center justify-content-center row-20']) ?>
                    <div class="col-xl-7 block-m">
                        <div class="group-20">
                            <div class="form-wrap">
                                <label class="form-label form-label-outside" for="gender">I am</label>
                                <select class="form-input" id="gender" name="sex" data-constraints="@Required" data-placeholder="<?= $my_sex == 1 ? 'Man' : 'Woman' ?>">
                                    <option label="placeholder"></option>
                                    <option value="1" <?= $my_sex == 1 ? ' selected' : '' ?>>Man</option>
                                    <option value="2" <?= $my_sex == 2 ? ' selected' : '' ?>>Woman</option>
                                </select>
                            </div>
                            <div class="form-wrap">
                                <label class="form-label form-label-outside" for="looking">Looking for</label>
                                <select class="form-input" id="looking" name="ProfileSearch[sex]" data-constraints="@Required" data-placeholder="<?= $searchModel->sex == 1 ? 'Man' : 'Woman' ?>">
                                    <option label="placeholder"></option>
                                    <option value="1" <?= $searchModel->sex == 1 ? ' selected' : '' ?>>Man</option>
                                    <option value="2" <?= $searchModel->sex == 2 ? ' selected' : '' ?>>Woman</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 block-s">
                        <div class="form-wrap">
                            <label class="form-label form-label-outside" for="start-age">Age</label>
                        </div>
                        <div class="small-group">
                            <div class="form-wrap">
                                <select class="form-input" id="start-age" name="ProfileSearch[min_age]" data-constraints="@Required" data-placeholder="<?= $searchModel->min_age ? $searchModel->min_age : '18' ?>">
                                    <option label="placeholder"></option>
                                    <?php for ($age = 18; $age <= 90; $age++) { ?>
                                    <option value="<?= $age ?>"<?= $searchModel->min_age == $age ? ' selected' : '' ?>><?= $age ?></option>
                                    <?php } ?>
                                </select>
                            </div><span class="divider-1"></span>
                            <div class="form-wrap">
                                <select class="form-input" id="end-age" name="ProfileSearch[max_age]" data-constraints="@Required" data-placeholder="<?= $searchModel->max_age ? $searchModel->max_age : '45' ?>">
                                    <option label="placeholder"></option>
                                    <?php for ($age = 18; $age <= 90; $age++) { ?>
                                        <option value="<?= $age ?>"<?= $searchModel->max_age == $age ? ' selected' : '' ?>><?= $age ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 block-l">
                        <div class="form-wrap">
                            <label class="form-label form-label-outside" for="country">Country</label>
                        </div>
                        <div class="group-20 align-items-end big-group">
                            <div class="form-wrap">
                                <select class="form-input" id="country" name="ProfileSearch[country_id]" data-constraints="@Required" data-placeholder="<?= $searchModel->country_id ? \app\components\GeoHelper::getCountryName($searchModel->country_id) : 'Choose your country' ?>">
                                    <option label="placeholder"></option>
                                    <?php $countryList = \app\components\GeoHelper::getCountries();
                                    foreach ($countryList as $country_id =>$country_name) { ?>
                                    <option value="<?= $country_id ?>"<?= $country_id == $searchModel->country_id ? ' selected' : '' ?>><?= $country_name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-wrap form-button">
                                <button class="button button-icon button-icon-left button-primary" type="submit"><span class="icon mdi mdi-magnify"></span>Find your partner</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                        <div class="form-group field-color_2 required">
                                    <label class="control-label">Tags</label>
                                    <span class="select2-selection select2-selection--single">
                                        <?php echo Select2::widget([
                                            'name' => 'ProfileSearch[tag_id]',
                                            'bsVersion' => '4.x' ,  
                                            'theme' => Select2 :: THEME_BOOTSTRAP ,  
                                            'value' => '',
                                            'data' =>$tags,
                                            'options' => ['multiple' => true, 'placeholder' => 'Select tags ...',
                                                'class' =>'select2-selection__rendered', 
                                        ],
                                        ]);
                                        ?>
                                    </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group field-color_2 required">
                                    <label class="control-label">Language</label>
                                    <span class="select2-selection select2-selection--single">
                                        <?php echo Select2::widget([
                                            'name' => 'ProfileSearch[lang_id]',
                                            'bsVersion' => '4.x' ,  
                                            'theme' => Select2 :: THEME_BOOTSTRAP ,  
                                            'value' => '',
                                            'data' =>\app\models\Profile::getLangList(),
                                            'options' => ['multiple' => true, 'placeholder' => 'Select languages ...',
                                                'class' =>'select2-selection__rendered', 
                                        ],
                                        ]);
                                        ?>
                                    </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                           
                        </div>
                    </div>
                   
                        
                <?= Html::endForm() ?>
            </div>
        </div>
        <div class="divider-3"></div>
        <div class="container mt-30 mt-md-45 mt-xxl-75 text-center">
            <h2 class="wow fadeIn">Only True People</h2>
            <p class="text-gray-700 wow fadeIn" data-wow-delay=".025s"><span style="max-width: 850px;">Every user registered on GO is verified via photo and mobile phone so you don’t have to worry how real or fake anyone is.</span></p>
            <?php echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_profile',
                'layout' => "{items}\n{pager}",
                'itemOptions' => ['class' => 'col-sm-6 col-lg-4'],
                'options' => ['class' => 'row row-30 row-xl-50 mt-md-45 mt-xxl-70'],
            ]); ?>
            <div class="row row-30 row-xl-50 mt-md-45 mt-xxl-70">
                <?php /*
                <div class="col-sm-6 col-lg-4">
                    <!-- Tour 3-->
                    <article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(/themes/agency/images/visitor-1-370x417.jpg);">
                        <div class="tour-3-inner">
                            <div class="tour-3-main">
                                <h3 class="tour-3-title"><a href="person-page.html">John Wilson</a></h3>
                                <ul class="tour-3-meta">
                                    <li><span class="icon mdi material-design-clock100"></span><span>active 10 days ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <!-- Tour 3-->
                    <article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(/themes/agency/images/visitor-2-370x417.jpg);" data-wow-delay=".1s">
                        <div class="tour-3-inner">
                            <div class="tour-3-main">
                                <h3 class="tour-3-title"><a href="person-page.html">Jane Peterson</a></h3>
                                <ul class="tour-3-meta">
                                    <li><span class="icon mdi material-design-clock100"></span><span>active 10 days ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <!-- Tour 3-->
                    <article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(/themes/agency/images/visitor-3-370x417.jpg);" data-wow-delay=".2s">
                        <div class="tour-3-inner">
                            <div class="tour-3-main">
                                <h3 class="tour-3-title"><a href="person-page.html">Emma Smith</a></h3>
                                <ul class="tour-3-meta">
                                    <li><span class="icon mdi material-design-clock100"></span><span>active 8 days ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <!-- Tour 3-->
                    <article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(/themes/agency/images/visitor-4-370x417.jpg);" data-wow-delay=".3s">
                        <div class="tour-3-inner">
                            <div class="tour-3-main">
                                <h3 class="tour-3-title"><a href="person-page.html">Kate McMillan</a></h3>
                                <ul class="tour-3-meta">
                                    <li><span class="icon mdi material-design-clock100"></span><span>active 5 days ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <!-- Tour 3-->
                    <article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(/themes/agency/images/visitor-5-370x417.jpg);" data-wow-delay=".4s">
                        <div class="tour-3-inner">
                            <div class="tour-3-main">
                                <h3 class="tour-3-title"><a href="person-page.html">Eric Cobb</a></h3>
                                <ul class="tour-3-meta">
                                    <li><span class="icon mdi material-design-clock100"></span><span>active 3 days ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <!-- Tour 3-->
                    <article class="tour-3 bg-image context-dark wow fadeIn" style="background-image: url(/themes/agency/images/visitor-6-370x417.jpg);" data-wow-delay=".5s">
                        <div class="tour-3-inner">
                            <div class="tour-3-main">
                                <h3 class="tour-3-title"><a href="person-page.html">Mary Ross</a></h3>
                                <ul class="tour-3-meta">
                                    <li><span class="icon mdi material-design-clock100"></span><span>active 2 days ago</span></li>
                                </ul>
                            </div>
                        </div>
                    </article>
                </div>
                <?php */ ?>
                <div class="col-12 wow fadeIn text-center" data-wow-delay=".6s"><a class="button button-sm button-gray-outline wow fadeIn" href="#" data-wow-delay=".07s">View more</a></div>
            </div>
        </div>
    </div>
<?php } ?>
<!--<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
=======
<?php /*
<!-- Why Travel with Sunway?-->
<section class="section">
    <div class="range">
        <div class="cell-lg-7 cell-xl-8 cell-xxl-9">
            <div class="cell-inner section-lg text-center text-sm-left">
                <h2 class="text-center text-xl-left wow fadeIn">Why Choose GO</h2>
                <p class="text-center text-xl-left wow fadeIn" data-wow-delay=".05s">Here’s why lots of people choose our website.</p>
                <!-- Owl Carousel-->
                <div class="owl-carousel owl-1 list-group-1 mt-lg-50" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="2" data-xl-items="3" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="false" data-margin="30" data-mouse-drag="false">
                    <article class="lg-1-item wow fadeIn">
                        <div class="icon lg-1-item-icon mdi mdi-account-search"></div>
                        <div class="lg-1-item-main">
                            <h3 class="lg-1-item-title font-weight-normal"><span class="lg-1-item-counter"></span>Simple to Use</h3>
                            <p>GO is very easy to use - just choose your and your partner’s gender, age, and location, and you’re all set!</p>
                        </div>
                    </article>
                    <article class="lg-1-item wow fadeIn" data-wow-delay=".025s">
                        <div class="icon lg-1-item-icon mdi material-design-compass106"></div>
                        <div class="lg-1-item-main">
                            <h3 class="lg-1-item-title font-weight-normal"><span class="lg-1-item-counter"></span>Smart Matching</h3>
                            <p>Our matching system is based on geolocation and interests. It helps you find the best people to meet or spend time with.</p>
                        </div>
                    </article>
                    <article class="lg-1-item wow fadeIn" data-wow-delay=".05s">
                        <div class="icon lg-1-item-icon mdi mdi-comment-processing"></div>
                        <div class="lg-1-item-main">
                            <h3 class="lg-1-item-title font-weight-normal"><span class="lg-1-item-counter"></span>Cool Community</h3>
                            <p>Besides being #1 dating service, we have a supportive and understanding community that’s always ready to help.</p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
        <div class="cell-lg-5 cell-xl-4 cell-xxl-3 height-fill">
            <div class="box-3 bg-image" style="background-image: url(/themes/agency/images/about-2.jpg);"><a class="box-3-play" data-lightgallery="item" href="https://www.youtube.com/watch?v=I5FlP07kdvM"><span class="icon mdi mdi-play"></span></a></div>
        </div>

    </div>
</section>
<!-- Popular Destinations-->
<section class="section section-lg bg-default">
    <div class="container">
        <div class="row row-50 flex-lg-row-reverse align-items-center justify-content-xl-between">
            <div class="col-lg-4 col-xl-5 col-xxl-4">
                <div class="block-7">
                    <h2 class="wow fadeIn">Meet Singles <br> in Your Area</h2>
                    <p class="ls-ng-1 text-md text-black-5 mt-md-20 mt-xxl-50 wow fadeIn" data-wow-delay=".025s">If you want to meet local singles for dating, companionship, friendship or even more, you have come to the right place.</p><a class="button button-lg button-icon button-icon-left button-primary offset-xl-t-45 wow fadeIn" href="#" data-wow-delay=".05s"><span class="icon mdi mdi-magnify"></span>Find your partner</a>
                </div>
            </div>
            <div class="col-lg-8 col-xl-7">
                <div class="row row-30">
                    <div class="col-sm-6 wow fadeIn"><a class="destination-1 context-dark" href="#">
                            <figure class="destination-1-figure">
                                <div class="destination-1-image bg-image" style="background-image: url(/themes/agency/images/destinations-1-363x260.jpg);"></div>
                            </figure>
                            <div class="destination-1-caption">
                                <p class="destination-1-decoration-title">Amsterdam</p>
                                <p class="heading-3 destination-1-title">Amsterdam</p>
                                <div class="destination-1-comment"><span>from $560</span></div>
                            </div></a>
                    </div>
                    <div class="col-sm-6 wow fadeIn" data-wow-delay=".025s"><a class="destination-1 context-dark" href="#">
                            <figure class="destination-1-figure">
                                <div class="destination-1-image bg-image" style="background-image: url(/themes/agency/images/destinations-2-363x260.jpg);"></div>
                            </figure>
                            <div class="destination-1-caption">
                                <p class="destination-1-decoration-title">London</p>
                                <p class="heading-3 destination-1-title">London</p>
                                <div class="destination-1-comment"><span>from $500</span></div>
                            </div></a>
                    </div>
                    <div class="col-sm-6 wow fadeIn" data-wow-delay=".025s"><a class="destination-1 context-dark" href="#">
                            <figure class="destination-1-figure">
                                <div class="destination-1-image bg-image" style="background-image: url(/themes/agency/images/destinations-3-363x260.jpg);"></div>
                            </figure>
                            <div class="destination-1-caption">
                                <p class="destination-1-decoration-title">Barcelona</p>
                                <p class="heading-3 destination-1-title">Barcelona</p>
                                <div class="destination-1-comment"><span>from $800</span></div>
                            </div></a>
                    </div>
                    <div class="col-sm-6 wow fadeIn" data-wow-delay=".05s"><a class="destination-1 context-dark" href="#">
                            <figure class="destination-1-figure">
                                <div class="destination-1-image bg-image" style="background-image: url(/themes/agency/images/destinations-4-363x260.jpg);"></div>
                            </figure>
                            <div class="destination-1-caption">
                                <p class="destination-1-decoration-title">New York</p>
                                <p class="heading-3 destination-1-title">New York</p>
                                <div class="destination-1-comment"><span>from $670</span></div>
                            </div></a>
                    </div>
                    <div class="col-sm-6 d-none d-lg-block"></div>
                    <div class="col-sm-6 wow fadeIn" data-wow-delay=".075s"><a class="destination-1 context-dark" href="#">
                            <figure class="destination-1-figure">
                                <div class="destination-1-image bg-image" style="background-image: url(/themes/agency/images/destinations-5-363x260.jpg);"></div>
                            </figure>
                            <div class="destination-1-caption">
                                <p class="destination-1-decoration-title">Paris</p>
                                <p class="heading-3 destination-1-title">Paris</p>
                                <div class="destination-1-comment"><span>from $400</span></div>
                            </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Special Offers-->
<section class="section cta-section position-relative section-sm context-dark bg-img-2">
    <div class="container">
        <div class="row justify-content-lg-end justify-content-center">
            <div class="col-md-8 col-lg-5 d-flex flex-column justify-content-center wow fadeInRight">
                <h2>Premium Membership</h2>
                <p class="text-white-7">Enjoy the benefits of our dating service’s membership! Use the psychological matching to find the best people and experience other great bonuses.</p>
                <div class="unit cta-unit unit-spacing-md flex-column flex-sm-row align-items-center offset-xl-t-40">
                    <div class="unit-left"><a class="button button-md button-light" href="#">view all options</a></div>
                    <div class="unit-body"><span class="text-white-7">14 days for free</span></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Latest News-->
<section class="section section-lg bg-default">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center wow fadeIn">Dating Tips & Advice</h2>
                <p class="offset-top-5 block-md text-center text-gray-800 wow fadeIn" data-wow-delay=".025s">Here are some of our latest dating articles written by our staff. We hope these tips will help you get more confident and find what you are looking for on GO.</p>
            </div>
        </div>
        <div class="row row-30">
            <div class="col-sm-6 col-lg-3 wow fadeIn">
                <!-- Post Creative-->
                <article class="post-creative"><a class="post-creative-image" href="blog-post.html" style="background-image: url(/themes/agency/images/home-2-8.jpg);"></a>
                    <div class="post-creative-main">
                        <p class="post-creative-title"><a href="blog-post.html">5 Reasons You’re Still Lonely Even Though You Use Dating Apps</a></p>
                        <time class="post-creative-time" datetime="2019">April 24, 2019</time>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 wow fadeIn" data-wow-delay=".025s">
                <!-- Post Creative-->
                <article class="post-creative"><a class="post-creative-image" href="blog-post.html" style="background-image: url(/themes/agency/images/home-2-9.jpg);"></a>
                    <div class="post-creative-main">
                        <p class="post-creative-title"><a href="blog-post.html">Can You Text Your Way into a Successful Relationship?</a></p>
                        <time class="post-creative-time" datetime="2019">May 12, 2019</time>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 wow fadeIn" data-wow-delay=".05s">
                <!-- Post Creative-->
                <article class="post-creative"><a class="post-creative-image" href="blog-post.html" style="background-image: url(/themes/agency/images/home-2-10.jpg);"></a>
                    <div class="post-creative-main">
                        <p class="post-creative-title"><a href="blog-post.html">9 Awesome Things Men Can Do to Score Points With Women</a></p>
                        <time class="post-creative-time" datetime="2019">May 14, 2019</time>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 wow fadeIn" data-wow-delay=".075s">
                <!-- Post Creative-->
                <article class="post-creative"><a class="post-creative-image" href="blog-post.html" style="background-image: url(/themes/agency/images/home-2-11.jpg);"></a>
                    <div class="post-creative-main">
                        <p class="post-creative-title"><a href="blog-post.html">Topics You Should Avoid Talking About When Trying To Date</a></p>
                        <time class="post-creative-time" datetime="2019">June 19, 2019</time>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<?php */ ?>