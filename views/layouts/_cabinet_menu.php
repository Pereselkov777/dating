<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\widgets\UserMenu;
use yii\widgets\Menu;
use yii\bootstrap4\Nav;
use app\models\Profile;

/**
 * @var dektrium\user\models\User $user
 */

//$user = Yii::$app->user->identity;$this->registerJsFile(Yii::$app->request->baseUrl . '/js/upload_photo.js?t=' . time(), ['depends' => [\app\assets\AppAsset::className()]]);
$profile = Profile::findOne(Yii::$app->user->id);

?>
<script type="text/javascript">
$(document).ready(function(){
    
    function checkMediaQuery() {
   
    
  // If the inner width of the window is greater then 768px
  if (window.innerWidth < 768) {
    $("#collapseExample").removeClass("show");
    var html =  '<a class="btn btn-primary" style="width:100%;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">\n'+
        '       <span>Show Menu</span><span class="icon mdi mdi-chevron-down"></span></span> \n'+
        '           </a> '
    $("#add").html(html);
    // Then log this message to the conso)le
  }
}

// Add a listener for when the window resizes
window.addEventListener('resize', checkMediaQuery);
      
    })

</script>
<div class="card">
    <div id="remove">
    <img id="add_av" class="card-img-top" src="<?=$profile->photo ?>" alt="<?=$profile->first_name ?>" data-id="<?=$profile-> avatar_id ?>" >
    </div>
    <div class="card-body">
        <div class="card-title">
            <h3>
                <?= $profile->first_name ?>
            </h3>
        </div>
            <p id="add"> 
            </p>
            <div class="collapse show" id="collapseExample">
                <div class="card">
                    <ul class="nav">
                        <li class="nav-item w-100 active"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/user/settings/profile">Profile</a></li>
                        <li class=" w-100"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/user/settings/account">Account</a></li>
                        <li class="nav-item w-100"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/user/settings/networks">Networks</a></li>
                        <li class="nav-item w-100"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/cabinet/balance">Balance (4.80)</a></li>
                        <li class="nav-item w-100"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/cabinet/photo">My photos</a></li>
                        <?php if (Yii::$app->user->identity->profile->sex == 2) { ?>
                            <li class="nav-item w-100"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/cabinet/private-photo">My private photos</a></li>
                        <?php } ?>
                        <li class="nav-item w-100"><a style="background-color:#007bff; width: 100%; font-size: 18px; padding-top: 6px; padding-bottom: 6px;" class="btn btn-primary mt-2" href="/cabinet">My blog</a></li>
                    </ul>  
                </div>
            </div>   
    </div>
</div>

