<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use app\assets\AppAsset;

// designs https://livedemo00.template-help.com/wt_prod-24530/index.html
// https://www.templatemonster.com/website-templates/go-dating-agency-elegant-multipage-html-website-template-84931.html

Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];

AppAsset::register($this);
$controller = Yii::$app->controller;
$default_controller = Yii::$app->defaultRoute;
$is_home = (($controller->id === $default_controller) && ($controller->action->id === $controller->defaultAction));
$is_home_search = false;
if (!Yii::$app->user->isGuest) {
    $is_home_search = $is_home ? true : false;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width height=device-height initial-scale=1.0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script type="text/javascript">
        var firebase_sender_id = '<?= getenv('FIREBASE_SENDER_ID') ?>';
    </script>
    <script type="text/javascript" src="/themes/agency/js/core.min.js"></script>
    <?php $this->head() ?>
    <style>
        .ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}
        @media (min-width: 1200px) {
            .container, .container-sm, .container-md, .container-lg, .container-xl {
                max-width: 1200px;
            }
        }
        .glyphicon {
            font-family: "Font Awesome 5 Free";
        }
        .glyphicon-eye-open:before {
            content: "\f06e";
            color: var(--success);
        }
        .glyphicon-pencil:before {
            content: "\f044";
            color: var(--primary);
        }
        .glyphicon-trash:before {
            content: "\f2ed";
            color: var(--danger);
        }

    </style>
</head>
<body>
<?php $this->beginBody() ?>
<div class="ie-panel">
    <a href="https://windows.microsoft.com/en-US/internet-explorer/">
        <img src="/themes/agency/images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>
</div>
<div class="preloader">
    <div class="loader">
        <div class="ball one">
            <div class="inner"></div>
        </div>
        <div class="ball two">
            <div class="inner"></div>
        </div>
        <div class="ball three">
            <div class="inner"></div>
        </div>
        <div class="ball four">
            <div class="inner"></div>
        </div>
        <div class="ball five">
            <div class="inner"></div>
        </div>
        <div class="ball six">
            <div class="inner"></div>
        </div>
        <div class="ball center">
            <div class="inner"></div>
        </div>
    </div>
</div>

<div class="page">
    <?php if ($is_home) {?>
    <!-- FScreen-->
    <section class="section jumbotron-2">
        <div class="jumbotron-2-content context-dark bg-image" style="background-image: url(/themes/agency/images/bg-image-1.jpg);">
            <?= $this->render('_main_menu') ?>
            <div class="container">
                <div class="jumbotron-2-content-inner<?= $is_home_search ? ' home-search' : ' without-home-search' ?>">
                    <div class="row justify-content-start">
                        <div class="col-xl-5">
                            <h1 class="text-white">Only real brides from Asia</h1>
                            <p class="text-white-8 text-md intro-subtitle ls-ng-1">
                                <?= Yii::t('app', 'asia-bride it is an online club for men who are looking for a girl, girlfriend, woman, bride, wife in the Asian region.') ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($is_home_search) { ?>
            <?= $content ?>
        <?php }  ?>
    </section>
    <?php } else { ?>
    <header class="section page-header page-header-1 context-dark">
        <div class="page-header-1-figure" style="background-image: url(/themes/agency/images/bg-image-2.jpg);"></div>
        <?= $this->render('_main_menu') ?>
        <section class="breadcrumbs-custom">
            <div class="breadcrumbs-custom-inner">
                <div class="container">
                    <div class="breadcrumbs-custom-main">
                        <?php if (isset($this->blocks['breadcrumbs-custom'])): ?>
                            <?= $this->blocks['breadcrumbs-custom'] ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </header>
    <?php }?>
        <?php /*
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'About', 'url' => ['/site/about']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
            '<li style="padding-top: 8px">'.LanguageDropdown::widget().'</li>',
            ['label' => 'Login', 'url' => ['/user/security/login'], 'visible' => Yii::$app->user->isGuest],
            [
                'label' => (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->username : 'Гость',
                'template' => '<a href="{url}" ><i class="ti-user"></i> {label} <i class="fa fa-angle-down"></i></a>',
                'items' => [
                    ['label' => Yii::t('app','Профиль'), 'url' => '/user/settings/profile'],
                    ['label' => Yii::t('app','Настройки'), 'url' => '/user/settings/account'],
                    ['label' => Yii::t('app','Социальные сети'), 'url' => '/user/settings/networks'],
                    ['label' => Yii::t('app', 'Мои записи'), 'url' => ['/cabinet'], 'visible' => !\Yii::$app->user->isGuest],
                    ['label' => Yii::t('app', 'Мои фотографии'), 'url' => ['/cabinet/photo'], 'visible' => !\Yii::$app->user->isGuest],
                    ['label' => Yii::t('app','Управление пользователями'), 'url' => '/user/admin', 'visible' => \Yii::$app->user->can('administrator')],
                    ['label' => Yii::t('app', 'Управление блогом'), 'url' => ['/blog/article'], 'visible' => \Yii::$app->user->can('administrator')],
                    ['label' => Yii::t('app', 'Выйти'), 'url' => ['/site/logout'],
                        'linkOptions' => [
                            'data' => ['method' => 'post']
                        ],
//                        'template' => '<a href="{url}" data-method="post" data-params="'.Yii::$app->request->csrfParam.'='.Yii::$app->request->getCsrfToken().'"><i class="ti-lock"></i> {label}</a>',
                        'template' => '<a href="{url}" data-method="post"><i class="ti-lock"></i> {label}</a>',
                    ]
                ],
                'visible' => !Yii::$app->user->isGuest,
            ],
        ],
    ]);
    NavBar::end();*/
    ?>

    <?php if (!$is_home_search) { ?>
        <?php if ($this->context->id == 'site' && in_array($this->context->action, ['index', 'about'])) { ?>
            <?= $content ?>
        <?php } else { ?>
            <section class="section bg-default">
                <div class="container">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </section>
        <?php } ?>
    <?php } ?>

</div>

<!-- Page Footer-->
<footer class="section footer-modern">
    <div class="footer-modern-main">
        <div class="container">
            <div class="row row-30 justify-content-lg-between">
                <div class="col-sm-5 col-md-3 col-lg-3">
                    <h3 class="footer-modern-title">Navigation</h3>
                    <div class="footer-modern-item-block">
                        <ul class="list list-1">
                            <li><a href="/site/about">About Us</a></li>
                            <?php /* ?>
                            <li><a href="our-team.html">Our Team</a></li>
                            <li><a href="testimonials.html">Testimonials</a></li>
                            <li><a href="grid-blog.html">Blog</a></li>
                            <?php */ ?>
                        </ul>
                    </div>
                </div>
                <?php /*
                <div class="col-sm-7 col-md-5 col-lg-4">
                    <h3 class="footer-modern-title">Popular Dating Locations</h3>
                    <div class="footer-modern-item-block" style="max-width: 300px;">
                        <div class="row row-13">
                            <div class="col-6">
                                <ul class="list list-1">
                                    <li><a href="#">Turkey</a></li>
                                    <li><a href="#">Greece</a></li>
                                    <li><a href="#">Italy</a></li>
                                    <li><a href="#">France</a></li>
                                    <li><a href="#">Croatia</a></li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="list list-1">
                                    <li><a href="#">England</a></li>
                                    <li><a href="#">New Zealand</a></li>
                                    <li><a href="#">India</a></li>
                                    <li><a href="#">Spain</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php */ ?>
                <div class="col-md-4 col-lg-3">
                    <h3 class="footer-modern-title">Contacts & Support</h3>
                    <div class="footer-modern-item-block">
                        <ul class="list list-1">
                            <li><a href="/site/contact">Get in Touch</a></li>
                            <?php /*
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Community Guidelines</a></li>
 <?php */ ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-modern-aside">
        <div class="container">
            <div class="row row-30">
                <div class="col-lg-4 d-flex flex-column align-items-lg-start justify-content-center align-items-center order-1">
                    <a class="brand" href="/" style="width: 170px;">
                        <img class="brand-logo-light" style="display: inline-block;" src="/img/logo-45.png" alt="Asia Bride" width="45" height="45" srcset="/img/logo-135.png 2x"/>
                        <?php /*
                           <img class="brand-logo-dark" src="/themes/agency/images/logo-inverse-95x40.png" alt="" width="95" height="40" srcset="/themes/agency/images/logo-inverse-95x40.png 2x"/>
                            <img class="brand-logo-light" src="/themes/agency/images/logo-default-107x45.png" alt="" width="107" height="45" srcset="/themes/agency/images/logo-default-321x135.png 2x"/></a>
                            <?php */ ?>
                        <span style="color: black; font-size: 16px; font-weight: bold">Asia Bride</span>
                    </a>
                </div>
                <div class="col-lg-4 d-flex flex-column align-items-lg-start justify-content-center order-3 order-lg-2">
                    <p class="rights"><span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>Asia Bride <?= date('Y') ?></span><span>. All rights reserved.&nbsp;</span><a href="privacy-policy.html">Privacy Policy</a></p>
                </div>
                <div class="col-lg-4 d-flex flex-column align-items-center text-center order-2 order-lg-3">
                    <?php if (Yii::$app->user->isGuest) { ?>
                    <a class="button btn-sm small button-default-outline wow fadeIn" href="<?= Yii::$app->urlManager->createUrl(['/user/registration/register']) ?>" data-wow-delay=".07s">Join Now</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="snackbars" id="form-output-global"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
