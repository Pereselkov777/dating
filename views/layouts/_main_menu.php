<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use app\widgets\Menu;
use app\components\LanguageDropdown;

/**
 * @var dektrium\user\models\User $user
 */

$user = Yii::$app->user->identity;
?>
<!-- RD Navbar-->
<div class="rd-navbar-wrap">
    <nav id="main_menu" class="rd-navbar rd-navbar-modern" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
        <div class="rd-navbar-main-outer">
            <div class="rd-navbar-main">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                    <!-- RD Navbar Toggle-->
                    <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                    <!-- RD Navbar Brand-->
                    <div class="rd-navbar-brand">
                        <a class="brand" href="/" style="width: 170px;">
                            <img class="brand-logo-dark" src="/themes/agency/images/logo-inverse-95x40.png" alt="" width="95" height="40" srcset="/themes/agency/images/logo-inverse-95x40.png 2x"/>
                            <img class="brand-logo-light" style="display: inline-block;" src="/img/logo-45.png" alt="Asia Bride" width="45" height="45" srcset="/img/logo-135.png 2x"/>
                            <?php /*
                            <img class="brand-logo-light" src="/themes/agency/images/logo-default-107x45.png" alt="" width="107" height="45" srcset="/themes/agency/images/logo-default-321x135.png 2x"/></a>
                            <?php */ ?>
                            <span class="logo-title">Asia Bride</span>
                        </a>
                    </div>
                </div>
                <div class="rd-navbar-nav-wrap">
                    <!-- RD Navbar Nav-->
                    <?php
                    //print_r(stripos(Yii::$app->request->url, '/blog') === 0); exit;
                    //print_r(LanguageDropdown::widget()); exit;
                    echo Menu::widget([
                        'items' => [
                            [
                                'label' => Yii::t('app','Home'),
                                'url' => ['/'],
                            ],
                            [
                                'label' => Yii::t('app','About'),
                                'url' => ['/site/about'],
                            ],
                            [
                                'label' => Yii::t('app','Blog'),
                                'url' => ['/blog'],
                            ],
                            [
                                'label' => Yii::t('app','Tariffs'),
                                'url' => ['/pages/tariffs'],
                            ],
                            [
                                'label' => Yii::t('app','Managers'),
                                'url' => ['/user-view-manager/index'],
                            ],
                            [
                                'label' => Yii::t('app','Chat'),
                                'url' => ['/chat'],
                            ],
                            LanguageDropdown::getLanguagesList(),
                            [
                                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username,
                                'visible' => !Yii::$app->user->isGuest,
                                'items' => [
                                    [
                                        'label' => Yii::t('app','Manage Users'),
                                        'url' => ['/user/admin/index'],
                                        'visible' => Yii::$app->user->can('administrator'),
                                    ],
                                    [
                                        'label' => Yii::t('app','Manage Blog'),
                                        'url' => ['/blog/article'],
                                        'visible' => Yii::$app->user->can('administrator'),
                                    ],
                                    [
                                        'label' => Yii::t('app','Account'),
                                        'url' => ['/user/settings/account'],
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Profile'),
                                        'url' => ['/user/settings/profile'],
                                    ],
                                    [
                                        'label' => Yii::t('app', 'My photos'),
                                        'url' => ['/cabinet/photo/index'],
                                    ],
                                    [
                                        'label' => Yii::t('app', 'My Blog'),
                                        'url' => ['/cabinet/default/index'],
                                    ],
                                    [
                                        'label' => Yii::t('app', 'Logout'),
                                        'url' => ['/user/security/logout'],
                                        'linkOptions' => ['data-method' => 'post'],
                                    ],
                                ],
                            ],
                            [
                                'label' => Yii::t('app', 'Login'),
                                'url' => ['/user/security/login'],
                                'visible' => Yii::$app->user->isGuest
                            ],
                            [
                                'label' => Yii::t('app', 'Register'),
                                'url' => ['/user/registration/register'],
                                'visible' => Yii::$app->user->isGuest
                            ],
                        ],
                        'options' => ['class' =>'rd-navbar-nav'], // set this to nav-tabs to get tab-styled navigation
                    ]);

                    ?>
                    <?php /*
                                <ul class="rd-navbar-nav">
                                    <li class="rd-nav-item active"><a class="rd-nav-link" href="/">Home</a>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="/site/about">About</a>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="/blog">Blog</a>
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="/chat">Chat</a>
                                    </li>
                                    <?= LanguageDropdown::widget() ?>

                                    <?php if (Yii::$app->user->isGuest) { ?>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="<?= Yii::$app->urlManager->createUrl(['/user/security/login']) ?>">Login</a>
                                        <!-- RD Navbar Megamenu-->
                                        <!--<ul class="rd-menu rd-navbar-megamenu">
                                            <li class="rd-megamenu-item">
                                                <p class="rd-megamenu-title">Elements</p>
                                                <div class="rd-megamenu-block">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="typography.html">Typography</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="buttons.html">Buttons</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="forms.html">Forms</a></li>
                                                    </ul>
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="icon-lists.html">Icon Lists</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="counters-and-accordions.html">Counters &amp; Accordions</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="rd-megamenu-item">
                                                <p class="rd-megamenu-title">Pages</p>
                                                <div class="rd-megamenu-block">
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="our-team.html">Our Team</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="testimonials.html">Testimonials</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="contact-us.html">Contact Us</a></li>
                                                    </ul>
                                                    <ul class="rd-megamenu-list">
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="404-page.html">404 Page</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="coming-soon.html">Coming Soon</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="privacy-policy.html">Privacy policy</a></li>
                                                        <li class="rd-megamenu-list-item"><a class="rd-megamenu-list-link" href="search-results.html">Search results</a></li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>-->
                                    </li>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="<?= Yii::$app->urlManager->createUrl(['/user/registration/register']) ?>">Register</a>
                                        <!-- RD Navbar Dropdown-->
                                        <!--<ul class="rd-menu rd-navbar-dropdown">
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="blog-post.html">Blog post</a></li>
                                        </ul>-->
                                    </li>
                                    <?php } else { ?>
                                    <li class="rd-nav-item"><a class="rd-nav-link" href="#"><?= Yii::$app->user->identity->username ?></a>
                                        <ul class="rd-menu rd-navbar-dropdown">
                                            <?php if (Yii::$app->user->can('administrator')) { ?>
                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= Yii::$app->urlManager->createUrl(['/user/admin/index']) ?>">Управление пользователями</a></li>
                                                <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= Yii::$app->urlManager->createUrl(['/blog/article/index']) ?>">Управление статьями</a></li>
                                            <?php } ?>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= Yii::$app->urlManager->createUrl(['/user/settings/account']) ?>">Account</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= Yii::$app->urlManager->createUrl(['/user/settings/profile']) ?>">Profile</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= Yii::$app->urlManager->createUrl(['/cabinet/default/index']) ?>">My Blog</a></li>
                                            <li class="rd-dropdown-item"><a class="rd-dropdown-link" href="<?= Yii::$app->urlManager->createUrl(['/user/security/logout']) ?>" data-method="post">Logout</a></li>
                                        </ul>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>

                            <?php */ ?>
                </div>
            </div>
    </nav>
</div>

