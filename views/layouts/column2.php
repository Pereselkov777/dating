<?php
use yii\helpers\Html;
use app\components\AuthorizationWidget;
use app\components\LocationWidget;
use app\widgets\Alert;

?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div class="row">
      <div class="col-sm-3">
            <?= \app\modules\blog\widgets\CategoryWidget::widget([]) ?>

            <?= \app\modules\blog\widgets\ArchiveWidget::widget([]) ?>

            <?= \app\modules\blog\widgets\TagWidget::widget([]) ?>

      </div>
      <div class="col-sm-9">
<?php echo Alert::widget() ?>
      <?= $content ?>

      </div>
</div>
<?php $this->endContent();