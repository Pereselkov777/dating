<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Photos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Photo'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'user_id',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'user_id',
                        'data' => \app\models\User::getFullList(),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        //'hideSearch' => true,
                        'options' => [
                            'placeholder' => 'Выберите пользователя...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]),
                    'value' => 'user.username',
                ],
                'title',
                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function ($data) {
                        return dosamigos\gallery\Gallery::widget(['items' => [
                            [
                                'url' => '/uploads/photos/full/' . $data->img,
                                'src' => '/uploads/photos/thumbs/' . $data->img,
                                'options' => array('title' => $data->img)
                            ],
                        ]]);
                    }
                ],
                [
                    'attribute' => 'status',
                    'filter' => \app\models\Photo::getStatusList(),
                    'value' => 'statusName',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
