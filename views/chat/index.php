<?php
/* @var $this yii\web\View */

$server_tz = (new DateTime('now', new DateTimeZone( date_default_timezone_get () )))->format('P');

$this->title = 'Chat';
?>
<div style="display: none">
    <?= \yii\bootstrap\Html::dropDownList('to_id', '', \app\models\User::getFullList(), [
            'prompt' => 'Выберите пользователя', 'class' => 'form-control', 'id' => 'to_id',
    ]) ?>
    <textarea id="chat" class="form-control"></textarea>
    
    <button id="send" class="btn btn-success">Send</button>
    <button id="list" class="btn btn-success">List</button>
    <button id="messages" class="btn btn-success">Messages</button>
    <button id="current" class="btn btn-primary">Get current Chat</button>
</div>

<?php /*
<script type="text/javascript" src="/js/socket.io.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.4.0/socket.io.min.js"></script>
 <?Php */ ?>
<script type="text/javascript" src="/js/socket.io.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<?php /*
<script type="text/javascript" src="https://fb.me/react-15.1.0.min.js"></script>
<script type="text/javascript" src="https://fb.me/react-dom 15.1.0.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
<?php */ ?>

<script type="text/javascript">
    var self_pers_id = 0;
    var user_id = <?= Yii::$app->user->id ?>;
    var user_name = '<?= Yii::$app->user->identity->username ?>';
    var chat_host = '<?= Yii::$app->params['chat_host'] ?>';
    var access_token = '<?= Yii::$app->user->identity->access_token ?>';
    var avatar = '<?= Yii::$app->user->identity->profile->photo ?>';
    var level = 0;
    var points = 0;
    var max_points = 50;
    var parent_id = 0;
    var server_tz = '<?= $server_tz /*'+07'*/ ?>';

   /* var socket = io.connect(chat_host);
    socket.on('connecting', function () {
        console.log('connecting!');
    });

    socket.on('connect', function(){
        console.log('send connect_chat');
        socket.emit('connect_chat', {'name': 'admin', 'access_token': access_token, 'user_id': user_id });
//            socket.emit('new-player', {'access_token': access_token });
    });

    socket.on("disconnect", function (data) {
        console.log("SOCKET DISCONNECTED!");
    });

    socket.on("connect_chat", function (data) {
        console.log('connect_chat', data)
    });

    socket.on("chat_message", function (data) {
        console.log('chat_message', data)
    });

    socket.on("load_chat_list", function (data) {
        console.log('load_chat_list', data)
    });

    socket.on("load_chat_messages", function (data) {
        console.log('load_chat_messages', data);
    });

    socket.on("get_chat_by_to_id", function (data) {
        console.log('get_chat_by_to_id', data);
        parent_id = data['parent_id'];
    });

    function send_chat() {
        var message = $('#chat').val();
        var to_id = $('#to_id').val()
        socket.emit('chat_message', {'access_token': access_token, 'user_id': user_id, 'to_id': to_id, 'message': message });
    }

    function get_list() {
        var to_id = $('#to_id').val()
        socket.emit('load_chat_list', {'access_token': access_token, 'user_id': user_id, 'to_id': to_id });
    }

    function get_messages() {
        if (parent_id) {
            socket.emit('load_chat_messages', {'access_token': access_token, 'parent_id': parent_id, 'user_id': user_id });
        }
    }

    function get_current_chat() {
        var to_id = $('#to_id').val()
        socket.emit('get_chat_by_to_id', {'access_token': access_token, 'user_id': user_id, 'to_id': to_id });
    }*/
</script>

<?php $this->registerJs(
    "$('#main_menu').addClass('rd-navbar--is-stuck');",
    \yii\web\View::POS_READY,
    'main-menu-chat-handler'
);

