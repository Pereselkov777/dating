<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use dektrium\user\widgets\Connect;
use dektrium\user\models\LoginForm;
use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap4\ActiveForm;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];

?>

<?php //echo $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

    <section class="section bg-default">
        <div class="container">
            <div class="row row-50 justify-content-center justify-content-xl-between">
                <div class="col-md-10 col-lg-6 col-xl-5">
                    <h3><?= Html::encode($this->title) ?></h3>
                    <!-- RD Mailform-->
                    <?php  $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'method' => 'post',
                        'options' => ['class' => 'rd-form signup-form form-register1'],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'validateOnBlur' => true,
                        'validateOnType' => true,
                        'validateOnChange' => true,
                        'fieldConfig' => [
                            'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n{endWrapper}",
                            'options' => [
                                'class' => 'form-wrap form-wrap-icon',
                            ],
                            'inputOptions' => [
                                'class' => 'form-input'
                            ],
                            'labelOptions' => [
                                'class' => 'form-label'
                            ],
                        ]
                    ]) ?>
                    <?= $form->errorSummary($model) ?>
                    <?= $form->field($model, 'login',[
                        'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n<div class=\"icon form-icon mdi mdi-account-outline\"></div>{endWrapper}"
                    ])->textInput(); ?>
                    <?= $form->field($model, 'password',[
                        'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n<div class=\"icon form-icon mdi mdi-key\"></div>{endWrapper}"
                    ])->passwordInput(); ?>
                    <?= $form->field($model, 'rememberMe')
                        ->checkbox(
                            ['template' => "{beginWrapper}\n<label class=\"checkbox-inline checkbox-modern\">{input}Remember Me</label>\n{endWrapper}"]
                        )->label(false) ?>
                    <div class="form-wrap form-wrap-group group-1">
                        <?= Html::submitButton(
                            Yii::t('user', 'Log in'),
                            ['class' => 'button button-lg button-primary', 'tabindex' => '4']
                        ) ?>
                        <?= Html::a(Yii::t('app', 'Create an Account'), ['/user/registration/register'], ['class' => 'button button-lg button-default-outline']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <p class="font-italic">or</p>
                    <div>
                        <div class="group-sm">
                            <a class="link-1 link-1-social icon mdi mdi-facebook" href="/user/auth?authclient=facebook"> </a>
                            <a class="link-1 link-1-social icon mdi mdi-vk" href="/user/auth?authclient=vkontakte"> </a>
                        </div>
                    </div>
                    <?php if ($module->enableConfirmation): ?>
                        <p class="text-left mb-3">
                            <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
                        </p>
                    <?php endif ?>

                </div>
            </div>
        </div>
    </section>
<?php /* ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="card">
            <div class="card-header">
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="card-body">
                <div class="card-text">
                    <?php  $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['class' => 'signup-form form-register1'],
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => true,
                        'validateOnBlur' => true,
                        'validateOnType' => true,
                        'validateOnChange' => true,
                        'fieldConfig' => ['template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}"]
                    ]) ?>


                    <?php if ($module->debug): ?>
                        <?= $form->field($model, 'login', [
                            'inputOptions' => [
                                'autofocus' => 'autofocus',
                                'class' => 'form-control',
                                'tabindex' => '1']])->dropDownList(LoginForm::loginList());
                        ?>

                    <?php else: ?>

                        <?= $form->field($model, 'login',
                           // ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]
                           ['enableLabel' => false])->textInput(array('placeholder' => 'Your name', 'class'=>'form-control text-left')
                        );
                        ?>

                    <?php endif ?>

                    <?php if ($module->debug): ?>
                        <div class="alert alert-warning">
                            <?= Yii::t('user', 'Password is not necessary because the module is in DEBUG mode.'); ?>
                        </div>
                        <?php else: ?>
                            <?= $form->field($model, 'password', ['enableLabel' => false])->passwordInput(array('placeholder' => 'Password', 'class'=>'form-control text-left')); ?>
                         <?php endif ?>
                    

                    
                    <?= $form->field($model, 'rememberMe')
                    ->checkbox(
                    ['template' => '<div class="form-group">{input}<label class="control-label ml-2">{label}</label></div>']
                    ) ?>
                    <div class="row">
                    <div class="col">
                    <?= Html::submitButton(
                        Yii::t('user', 'Sign in'),
                        ['class' => 'btn btn-primary', 'tabindex' => '4']
                    ) ?>
                    </div>
                    <div class="col">
                     <button class="btn btn-light">
                    <?= Html::a(Yii::t('user', 'SIGN UP'), ['/user/registration/register']) ?>
                     </button>
                     </div>
                    </div>
                   
                    
                    

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <?php if ($module->enableConfirmation): ?>
                <p class="text-center mb-3">
                    <?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['/user/registration/resend']) ?>
                </p>
            <?php endif ?>
           
            <div class="ml-auto mr-auto">
            <?= Connect::widget([
                'baseAuthUrl' => ['/user/security/auth'],
            ]) ?>
            </div>
           
        </div>
    </div>
</div>
<?php */ ?>