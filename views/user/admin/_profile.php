<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;

/**
 * @var yii\web\View $this
 * @var app\models\User $user
 * @var app\models\Profile $profile
 */

Yii::$app->assetManager->bundles['yii\bootstrap4\BootstrapAsset'] = [
    'sourcePath' => null,
    'css' => [],
    'js' => [],
];

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<?= $form->field($profile, 'kind_id')->dropDownList(\app\models\Profile::getKindList()) ?>

<?= $form->field($profile, 'agency_id')->dropDownList(\app\models\Agency::getFullList(), ['prompt' => 'Выберите агентство']) ?>

<?= $form->field($profile, 'manager_id') ?>


<?php /*echo $form->field($profile, 'photo')->widget(budyaga\cropper\Widget::className(), [
    'uploadUrl' => \yii\helpers\Url::toRoute('/user/settings/uploadPhoto'),
    'width' => '300',
    'height' => '400',
    'aspectRatio' => 0.75,
    'cropAreaWidth' => '400',
    'cropAreaHeight' => '500',
])*/ ?>

<?= $form->field($profile, 'birthday')->widget(\yii\jui\DatePicker::className(),[
    'language' => 'en-Gb',
    'dateFormat' => 'yyyy-MM-dd',
    'clientOptions' => [
        'defaultDate' => '1990-01-01',
        'changeMonth'=> true,
        'changeYear'=> true,
        'yearRange' => "-90:-18",
    ],
    'options' => ['class' => 'form-control',]
]) ?>



<?= $form->field($profile, 'sex')->dropDownList(\app\models\Profile::getSexList()) ?>

<?= $form->field($profile, 'first_name') ?>

<?= $form->field($profile, 'last_name') ?>

<?= $form->field($profile, 'marital_status')->dropDownList(\app\models\Profile::getMaritalStatusList()) ?>

<?= $form->field($profile, 'children_count') ?>

<?= $form->field($profile, 'age_from') ?>

<?= $form->field($profile, 'age_to') ?>

<?= $form->field($profile, 'height') ?>

<?= $form->field($profile, 'weight') ?>

<?= $form->field($profile, 'body_type')->dropDownList(\app\models\Profile::getBodyTypeList(), ['prompt' => 'Не выбрано']) ?>

<?= $form->field($profile, 'hair_type')->dropDownList(\app\models\Profile::getHairTypeList(), ['prompt' => 'Не выбрано']) ?>

<?= $form->field($profile, 'hair_color')->dropDownList(\app\models\Profile::getHairColorList(), ['prompt' => 'Не выбрано']) ?>

<?= $form->field($profile, 'eyes_color')->dropDownList(\app\models\Profile::getEyesColorList(), ['prompt' => 'Не выбрано']) ?>

<?= $form->field($profile, 'drinking')->dropDownList(\app\models\Profile::getDrinkingList(), ['prompt' => 'Не выбрано']) ?>

<?= $form->field($profile, 'smoking')->dropDownList(\app\models\Profile::getSmokingList(), ['prompt' => 'Не выбрано']) ?>

<?= $form->field($profile, 'about_me')->textarea() ?>

<?= $form->field($profile, 'about_ideal')->textarea() ?>

<?= $form->field($profile, 'country_id')->widget(Select2::classname(), [
    'data' => \app\components\GeoHelper::getCountries(),
    'options' => ['placeholder' => 'Выберите страну ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]) ?>

<?= $form->field($profile, 'region_id')->widget(Select2::classname(), [
    'data' => $profile->country_id ? \app\components\GeoHelper::getRegions($profile->country_id) : [],
    'options' => ['placeholder' => 'Выберите регион ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]) ?>

<?= $form->field($profile, 'city_id')->widget(Select2::classname(), [
    'data' => $profile->region_id ? \app\components\GeoHelper::getCities($profile->region_id) : [],
    'options' => ['placeholder' => 'Выберите город ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]) ?>


<?= $form->field($profile, 'facebook_link') ?>

<?= $form->field($profile, 'status')->dropDownList([0 => 'Not Active', 1 => 'Active']) ?>


<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
        <br>
    </div>
</div>

<?php
$script = <<< JS
    jQuery(document).ready(function () {
        $('#profile-country_id').change(function() {
            console.log('changed');
            $.ajax({
                      url: '/geo/get-regions',  //урл запроса
                      data: {country_id: + $(this).val()},
                      dataType: 'json',
                      success: function(data){ 
                          $('#profile-region_id option:gt(0)').remove();
                          //data.sort();
                          //console.log(data);
                          Object.keys(data).forEach(function(k){
                              $('#profile-region_id').append($("<option></option>")
                                 .attr("value", k).text(data[k]));
                          });
                    }}
             );
        });

        $('#profile-region_id').change(function() {
            console.log('changed');
            $.ajax({
                      url: '/geo/get-cities',  //урл запроса
                      data: {region_id: + $(this).val()},
                      dataType: 'json',
                      success: function(data){ 
                          $('#profile-city_id option:gt(0)').remove();
                          Object.keys(data).forEach(function(k){
                              $('#profile-city_id').append($("<option></option>")
                                 .attr("value", k).text(data[k]));
                          });
                    }}
             );
        });
    });

JS;
$this->registerJs($script);
?>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
