<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use dektrium\user\widgets\Connect;
use yii\bootstrap\ActiveForm;


/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];

if (empty($model->referrer_id)) {
    $model->referrer_id = isset(Yii::$app->request->cookies['referrer']) ? Yii::$app->request->cookies['referrer'] : 1;
}
?>
<?php //echo $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<style>
#select2-register-form-sex-container,
#select2-register-form-sex-results {
    border: 1px #c4c4c4 solid;
}
</style>
<section class="section bg-default">
    <div class="container">
        <div class="row row-50 justify-content-center justify-content-xl-between">
            <div class="col-md-10 col-lg-6 col-xl-5">
                <h3><?= Html::encode($this->title) ?></h3>
                <!-- RD Mailform-->
                <?php $form = ActiveForm::begin([
                    'id' => 'registration-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'options' => ['class' => 'rd-form'],
                    'fieldConfig' => [
                        'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n{endWrapper}",
                        'options' => [
                            'class' => 'form-wrap form-wrap-icon',
                        ],
                        'inputOptions' => [
                            'class' => 'form-input'
                        ],
                        'labelOptions' => [
                            'class' => 'form-label'
                        ],
                    ]
                ]); ?>
                <?= $form->errorSummary($model) ?>
                <?= $form->field($model, 'username',[
                    'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n<div class=\"icon form-icon mdi mdi-account-outline\"></div>{endWrapper}"
                ])->textInput(); ?>
                <?= $form->field($model, 'email',[
                    'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n<div class=\"icon form-icon mdi mdi-email-outline\"></div>{endWrapper}"
                ])->textInput(); ?>
                <?= $form->field($model, 'password',[
                    'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n<div class=\"icon form-icon mdi mdi-key\"></div>{endWrapper}"
                ])->passwordInput(); ?>
                <?= $form->field($model, 'sex',[
                    'template' => "{beginWrapper}\n{input}\n{label}\n{hint}\n{error}\n<div class=\"icon form-icon mdi mdi-human-handsup\"></div>{endWrapper}"
                ])->dropDownList([1 => 'I\'m man', 2 => 'I\'m woman']); ?>
                <?php echo $form->field($model, 'referrer_id')->hiddenInput()->label(false) ?>

                <?php
                echo '<div style="display: none;">';
                echo Html::textInput('email', '');
                echo Html::hiddenInput('validation_key', '', ['id' => 'validation_key']);
                echo '</div>';
                ?>

                <?= $form->field($model, 'acordul_tc')
                    ->checkbox(
                        ['template' => "{beginWrapper}\n<label class=\"checkbox-inline checkbox-modern\">{input}".Yii::t('app', 'I agree to the {link:create}Terms and Conditions{/link}',[
                            'link:create' => '<a href="/agreement" target="_blank">',
                            '/link' => '</a>'])."</label>\n{endWrapper}"]
                    )->label(false) ?>
                <div class="form-wrap form-wrap-group group-1 group-middle">
                    <?= Html::submitButton(Yii::t('user', 'Sign up'), ['id'=> 'cfb_2', 'class' => 'button button-lg button-primary']) ?>
                    <p class="font-italic">or</p>
                    <div>
                        <div class="group-sm">
                            <a class="link-1 link-1-social icon mdi mdi-facebook" href="/user/auth?authclient=facebook">
                            </a>
                            <a class="link-1 link-1-social icon mdi mdi-vk" href="/user/auth?authclient=vkontakte"> </a>
                        </div>
                    </div>
                </div>
                <p class="text-left mb-3">
                    <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login']) ?>
                </p>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>

<?php /*
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div class="card">
            <div class="card-header">
                <h3><?= Html::encode($this->title) ?></h3>
</div>
<div class="card-body">
    <div class="card-text">
        <?php $form = ActiveForm::begin([
                        'id' => 'registration-form',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                    ]); ?>

        <?= $form->field($model, 'email',['enableLabel' => false])->textInput(array('placeholder' => 'Your email', 'class'=>'form-control text-left')) ?>

        <?= $form->field($model, 'username',['enableLabel' => false])->textInput(array('placeholder' => 'Username', 'class'=>'form-control text-left')) ?>

        <?php if ($module->enableGeneratingPassword == false): ?>
        <?= $form->field($model, 'password', ['enableLabel' => false])->passwordInput(array('placeholder' => 'Password', 'class'=>'form-control text-left')); ?>
        <?php endif ?>

        <?php echo $form->field($model, 'referrer_id')->hiddenInput()->label(false) ?>

        <?php
                    echo '<div style="display: none;">';
                    echo Html::textInput('email', '');
                    echo Html::hiddenInput('validation_key', '', ['id' => 'validation_key']);
                    echo '</div>';
                    ?>

        <?= $form->field($model, 'acordul_tc',
                        ['options' => ['tag' => 'span'],
                            'template' => "{input}",
                            'labelOptions' => ['encode' => false]
                        ]
                    )
                        ->checkbox(['checked' => false, 'required' => true, 'label' =>Yii::t('app', 'I agree to the {link:create}Terms and Conditions{/link}',[
                            'link:create' => '<a href="/agreement" target="_blank">',
                            '/link' => '</a>'])
                        ]);
                    ?>
        <div class="row">
            <div class="col">
                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['id'=> 'cfb_2', 'class' => 'btn btn-primary']) ?>
            </div>

        </div>



        <?php ActiveForm::end(); ?>
    </div>
</div>
<p class="text-center mb-3">
    <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login']) ?>
</p>
<div class="ml-auto mr-auto">
    <?= Connect::widget([
                'baseAuthUrl' => ['/user/security/auth'],
            ]) ?>
</div>

</div>
</div>
</div>
<?php */?>
<?php
$validation_key = $this->context->validation_key;
$script = <<< JS
$(function(){ Phonem.New('phone').Create();})
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>