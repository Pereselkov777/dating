<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use dektrium\user\helpers\Timezone;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\components\cropper\Cropper;
use dosamigos\selectize\SelectizeTextInput;
use yii\web\View;
use app\models\Profile;

// https://github.com/sabirov/yii2-bootstrap4-cropper
// https://github.com/fengyuanchen/cropperjs/blob/master/README.md#options
// https://fengyuanchen.github.io/cropperjs/
/**
 * @var yii\web\View $this
 * @var yii\bootstrap4\ActiveForm $form
 * @var app\models\Profile $model
 */

$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;


$self = Profile::findOne(Yii::$app->user->id);
$hase_manager = $self->manager_id;
//var_dump($hase_manager);

Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];
Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
    'sourcePath' => null,
    'css' => [],
    'js' => [],
];

$uploadPath = Yii::getAlias('@web') . '/uploads/profile/photo';
$img_url = is_null($model->photo) ? null : /*$uploadPath . DIRECTORY_SEPARATOR . */ $model->photo;

$is_lite_profile = getenv('APP_IS_LITE_PROFILE');
echo('HERE');
$this->registerJs(
    " 
        window.fbAsyncInit = function() {
            FB.init({
              appId            : '282900393220413',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : 'v2.11'
            });
          }; 
        (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
    ",
    View::POS_BEGIN,
    'fb'
  );
  $this->registerJsFile('https://connect.facebook.net/en_US/sdk.js',  [
    'position' => $this::POS_BEGIN,
    'async defer crossorigin'=>"anonymous"
    
  ]);

?>

<div id="fb-root"></div>
<script type="text/javascript">  
$(document).ready(function(){
  //alert('HERE');
  //console.log("hello wourld");
  var manager = '<?php echo(int)$hase_manager; ?>';
  console.log(manager, 'MAN');
    if(manager > 0){
        console.log(manager,'HAVE');
        var html =
        '<div class="fb-customerchat"\n' +
        'page_id="100920135432987"\n' +
        'logged_in_greeting="Hi! How can we help you?"\n' +
        'logged_out_greeting="GoodBye!... Hope to see you soon."\n' +
        'greeting_dialog_display="show"\n' +
        'minimized="false">\n' +
        "</div>";
    console.log($("body"),"THIS_IS");
    $("body").append(html);
  };

});  
</script>

        <div class="card">
            <div class="card-header">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'profile-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                ]); ?>
                <?= $form->errorSummary($model) ?>
                <div class="form-group field-profile-bio">
                    <label class="col-lg-3 control-label">Referral link</label>
                    <div class="col-lg-9">
                        <?= Html::textInput('ref_link', Url::to(['/user/register', 'referrer' => $model->user_id], true),
                            ['readonly' => 'readonly', 'class' => 'form-control']) ?>
                    </div>
                    <div class="col-sm-offset-3 col-lg-9"><div class="help-block"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                        <br>
                    </div>
                </div>

                <?php if (!$is_lite_profile) { ?>

                     <?= $form->field($model, 'birthday')->widget(\yii\jui\DatePicker::className(),[
                    'language' => 'en-Gb',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'defaultDate' => '1990-01-01',
                        'changeMonth'=> true,
                        'changeYear'=> true,
                        'yearRange' => "-90:-18",
                    ],
                    'options' => ['class' => 'form-control',]
                ]) ?>

                <?php } ?>

                <?php
                if ($model->sex) { ?>
                    <div class="form-group field-profile-bio">
                        <label class="col-lg-3 control-label">Sex</label>
                        <div class="col-lg-9">
                            <?= Html::textInput('sex_readonly', $model->getSexTypeName(),
                                ['readonly' => 'readonly', 'class' => 'form-control']) ?>
                        </div>
                        <div class="col-sm-offset-3 col-lg-9"><div class="help-block"></div>
                        </div>
                    </div>
                <?php } else {
                    echo $form->field($model, 'sex')->dropDownList(\app\models\Profile::getSexList());
                }
                ?>

                <?= $form->field($model, 'first_name') ?>

                <?php if (!$is_lite_profile) { ?>

                <?= $form->field($model, 'last_name') ?>

                <?= $form->field($model, 'marital_status')->dropDownList(\app\models\Profile::getMaritalStatusList()) ?>

                <?= $form->field($model, 'children_count') ?>

                <?= $form->field($model, 'age_from') ?>

                <?= $form->field($model, 'age_to') ?>

                <?= $form->field($model, 'height') ?>

                <?= $form->field($model, 'weight') ?>

                <?= $form->field($model, 'body_type')->dropDownList(\app\models\Profile::getBodyTypeList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'hair_type')->dropDownList(\app\models\Profile::getHairTypeList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'hair_color')->dropDownList(\app\models\Profile::getHairColorList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'eyes_color')->dropDownList(\app\models\Profile::getEyesColorList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'drinking')->dropDownList(\app\models\Profile::getDrinkingList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'smoking')->dropDownList(\app\models\Profile::getSmokingList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?php } ?>

                <?= $form->field($model, 'about_me')->textarea() ?>

                <?php if (!$is_lite_profile) { ?>

                    <?= $form->field($model, 'about_ideal')->textarea() ?>

                <?php } ?>

                <?php echo $form->field($model, 'themes')->textarea() ?>

                <?= $form->field($model, 'lang_id')->dropDownList(\app\models\Profile::getLangList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'lang2_id')->dropDownList(\app\models\Profile::getLangList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'lang3_id')->dropDownList(\app\models\Profile::getLangList(), ['prompt' => Yii::t('app','Not chosen')]) ?>

                <?= $form->field($model, 'country_id')->widget(Select2::classname(), [
                    'data' => \app\components\GeoHelper::getCountries(),
                    'options' => ['placeholder' => 'Choose country ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>

                <?php if (!$is_lite_profile) { ?>

                    <?= $form->field($model, 'region_id')->widget(Select2::classname(), [
                        'data' => $model->country_id ? \app\components\GeoHelper::getRegions($model->country_id) : [],
                        'options' => ['placeholder' => 'Choose region ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]) ?>

                    <?= $form->field($model, 'city_id')->widget(Select2::classname(), [
                        'data' => $model->region_id ? \app\components\GeoHelper::getCities($model->region_id) : [],
                        'options' => ['placeholder' => 'Choose city ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]) ?>

                <?php } ?>

                <?= $form->field($model, 'facebook_link') ?>

                <?= $form->field($model, 'public_email') ?>

                <?php if ($model->photo) { ?>
                    <?= $form->field($model, 'status')->dropDownList([0 => 'Not Active', 1 => 'Active']) ?>
                <?php } else { ?>
                    <div class="alert alert-danger" role="alert">

                        Your profile is inactive. <?= Html::a('Add a photo', ['/cabinet/photo/index'], ['target' => '_blank']) ?> and make it your avatar, after which you can activate your profile.

                    </div>
                <?php } ?>
                <?=  $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
                    'loadUrl' => ['/profile-tag/list'],
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'plugins' => ['remove_button'],
                        'valueField' => 'title',
                        'labelField' => 'title',
                        'searchField' => ['title'],
                        'create' => true,
                    ],
                ])->hint('Use commas to separate tags')?>

                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
                        <br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
<?php
$script = <<< JS
    jQuery(document).ready(function () {
        $('#profile-country_id').change(function() {
            console.log('changed');
            $.ajax({
                      url: '/geo/get-regions',  //урл запроса
                      data: {country_id: + $(this).val()},
                      dataType: 'json',
                      success: function(data){ 
                          $('#profile-region_id option:gt(0)').remove();
                          //data.sort();
                          //console.log(data);
                          Object.keys(data).forEach(function(k){
                              $('#profile-region_id').append($("<option></option>")
                                 .attr("value", k).text(data[k]));
                          });
                    }}
             );
        });

        $('#profile-region_id').change(function() {
            console.log('changed');
            $.ajax({
                      url: '/geo/get-cities',  //урл запроса
                      data: {region_id: + $(this).val()},
                      dataType: 'json',
                      success: function(data){ 
                          $('#profile-city_id option:gt(0)').remove();
                          Object.keys(data).forEach(function(k){
                              $('#profile-city_id').append($("<option></option>")
                                 .attr("value", k).text(data[k]));
                          });
                    }}
             );
        });
    });

JS;
$this->registerJs($script);
?>