<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ClientOrder */

$this->title = Yii::t('app', 'Create Client Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Client Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
