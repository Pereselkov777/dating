<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DatingOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dating-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'order_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'contacts')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
