<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DatingOrder */

$this->title = Yii::t('app', 'Create Dating Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dating Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dating-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
