<?php
use yii\helpers\Html;
//var_dump($model);

/* @var $this yii\web\View */
/* @var $model bupy7\pages\models\Page */

if (empty($model->title_browser)) {
    $this->title = $model->title;
} else {
    $this->title = $model->title_browser;
}
if (!empty($model->meta_description)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_description), 'name' => 'description']);
}
if (!empty($model->meta_keywords)) {
    $this->registerMetaTag(['content' => Html::encode($model->meta_keywords), 'name' => 'keywords']);
}
?>

<?php if ($model->display_title) : ?>
    <div class="page-header mb-3 mt-3">
        <h1><?= Html::encode($model->title); ?></h1>
    </div>
<?php endif; ?>


<div class="card text-white bg-warning mb-3" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title" style="color:white !important;">Updated at:<?php echo $model['updated_at']?> </h4>
    <p class="card-text"><?php echo $model['content']?></p>
  </div>
</div>
<div class="clearfix"></div>





