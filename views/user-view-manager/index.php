<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Profile;
use app\modules\manager\models\Manager;
use yii\web\View;
use app\assets\FbAsset;
//use kartik\social\FacebookPlugin;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\manager\models\ManagerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Managers');
$this->params['breadcrumbs'][] = $this->title;


Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
    'sourcePath' => null,
    'js' => [],
];
//FbAsset::register($this);





/*$this->registerJs(
    " 
        window.fbAsyncInit = function() {
            FB.init({
              appId            : '282900393220413',
              autoLogAppEvents : true,
              xfbml            : true,
              version          : 'v2.11'
            });
          }; 
        (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
    ",
    View::POS_BEGIN,
    'fb'
);
$this->registerJsFile('https://connect.facebook.net/en_US/sdk.js',  [
    'position' => $this::POS_BEGIN,
    'async defer crossorigin'=>"anonymous"
    
]);*/




?>

<div class="manager-index">


    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'price',
            'about:ntext',
            [
                'attribute' => 'first_name',
                //'filter' => Profile::find()->select(['first_name'])->column(),
                'value'=>'profile.first_name',
            ],
            [
                'attribute' => 'facebook_link',
                //'filter' => Profile::find()->select(['facebook_link'])->column(),
                'value'=>'profile.facebook_link',
            ],
            [
                'attribute' => 'public_email',
                //'filter' => Profile::find()->select(['first_name'])->column(),
                'value'=>'profile.public_email',
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
        ],
    ]); ?>


</div>
