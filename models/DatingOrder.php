<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%dating_order}}".
 *
 * @property int $id ID
 * @property string $order_text Order text
 * @property string $contacts Contacts
 * @property string $created_at Дата создания
 */
class DatingOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%dating_order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_text', 'contacts'], 'required'],
            [['order_text'], 'string'],
            [['created_at'], 'safe'],
            [['contacts'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_text' => Yii::t('app', 'Order text'),
            'contacts' => Yii::t('app', 'Contacts'),
            'created_at' => Yii::t('app', 'Дата создания'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
    }

}
