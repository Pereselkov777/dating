<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photo".
 *
 * @property int $id ID
 * @property int $user_id Пользователь
 * @property string $title Название
 * @property string $img_full Полное изображение
 * @property string $img_prev Изображение превью
 * @property int $status Статус
 * @property string $create_time Создано
 * @property string $update_time Обновлено
 */
class Photo extends \yii\db\ActiveRecord
{
    public $file;
    public static $status_list = [
        0 => 'Не опубликовано',
        1 => 'Опубликовано',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['title', 'img_full', 'img_prev'], 'string', 'max' => 255],

            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'title' => 'Название',
            'img_full' => 'Полное изображение',
            'img_prev' => 'Изображение превью',
            'status' => 'Статус',
            'create_time' => 'Создано',
            'update_time' => 'Обновлено',
            'file' => 'Файл изображения',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->create_time = date('Y-m-d H:i:s');
            }
            $this->update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getStatusName() {
        return isset(self::$status_list[$this->status]) ? self::$status_list[$this->status] : '';
    }

    public function getFull() {
        if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $this->img_full)) {
            return '/uploads/photos/full/' . $this->img_full;
        }
        return '';
    }

    public function getPrew() {
        if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $this->img_prev)) {
            return '/uploads/photos/thumbs/' . $this->img_prev;
        }
        return '';
    }

    public function upload()
    {
        if ($this->validate()) {
            if (!empty($this->img)) {
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $this->img)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $this->img );
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/middle/' . $this->img)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/middle/' . $this->img );
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $this->img)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $this->img );
                }
            }
            $name = $this->user_id . '_' . time() . '.' . $this->file->extension;
            $this->img_full = $this->img_prev = $name;
            $this->file->saveAs(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);
            if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name)) {
                $image = imagecreatefromjpeg(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);

              //  $this->blur(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);
                
                $filename = Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $name;
//        var_dump($filename); exit;

                $thumb_width = 300;
                $thumb_height = 417;
//        $thumb_height = 225;

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ($original_aspect >= $thumb_aspect) {
                    // If image is wider than thumbnail (in aspect ratio sense)
                    $new_height = $thumb_height;
                    $new_width = $width / ($height / $thumb_height);
                } else {
                    // If the thumbnail is wider than the image
                    $new_width = $thumb_width;
                    $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

// Resize and crop
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                imagejpeg($thumb, $filename, 80);

                $filename = Yii::getAlias('@webroot') . '/uploads/photos/middle/' . $name;
//        var_dump($filename); exit;

                $thumb_width = 600;
                $thumb_height = 834;
//        $thumb_height = 450;

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ($original_aspect >= $thumb_aspect) {
                    // If image is wider than thumbnail (in aspect ratio sense)
                    $new_height = $thumb_height;
                    $new_width = $width / ($height / $thumb_height);
                } else {
                    // If the thumbnail is wider than the image
                    $new_width = $thumb_width;
                    $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

// Resize and crop
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                imagejpeg($thumb, $filename, 80);
            }
            return true;
        } else {
            return false;
        }
    }

    // https://stackoverflow.com/questions/42759135/php-best-way-to-blur-images
    // https://stackoverflow.com/questions/1248866/php-gd-gaussian-blur-effect
    protected function blur($file) {
        $image = imagecreatefromjpeg($file);

        /* Get original image size */
        list($w, $h) = getimagesize($file);

        /* Create array with width and height of down sized images */
        $size = array('sm'=>array('w'=>intval($w/4), 'h'=>intval($h/4)),
            'md'=>array('w'=>intval($w/2), 'h'=>intval($h/2))
        );

        /* Scale by 25% and apply Gaussian blur */
        $sm = imagecreatetruecolor($size['sm']['w'],$size['sm']['h']);
        imagecopyresampled($sm, $image, 0, 0, 0, 0, $size['sm']['w'], $size['sm']['h'], $w, $h);

        for ($x=1; $x <=40; $x++){
            imagefilter($sm, IMG_FILTER_GAUSSIAN_BLUR, 999);
        }

        imagefilter($sm, IMG_FILTER_SMOOTH,99);
        imagefilter($sm, IMG_FILTER_BRIGHTNESS, 10);

        /* Scale result by 200% and blur again */
        $md = imagecreatetruecolor($size['md']['w'], $size['md']['h']);
        imagecopyresampled($md, $sm, 0, 0, 0, 0, $size['md']['w'], $size['md']['h'], $size['sm']['w'], $size['sm']['h']);
        imagedestroy($sm);

        for ($x=1; $x <=25; $x++){
            imagefilter($md, IMG_FILTER_GAUSSIAN_BLUR, 999);
        }

        imagefilter($md, IMG_FILTER_SMOOTH,99);
        imagefilter($md, IMG_FILTER_BRIGHTNESS, 10);

        /* Scale result back to original size */
        imagecopyresampled($image, $md, 0, 0, 0, 0, $w, $h, $size['md']['w'], $size['md']['h']);
        imagedestroy($md);

// Apply filters of upsized image if you wish, but probably not needed
//imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR);
//imagefilter($image, IMG_FILTER_SMOOTH,99);
//imagefilter($image, IMG_FILTER_BRIGHTNESS, 10);

        imagejpeg($image, Yii::getAlias('@webroot') . '/uploads/photos/full/blur.1jpg');
        imagedestroy($image);

    }


    public static function getStatusList() {
       return self::$status_list;
    }
}
