<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%bill}}".
 *
 * @property int $id ID
 * @property int $user_id Пользователь
 * @property float $amount
 * @property int $payment_id
 * @property int $status
 * @property string $create_time
 * @property string $update_time
 * @property int $order_id Заказ
 */
class Bill extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%bill}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'payment_id', 'status', 'create_time', 'update_time'], 'required'],
            [['user_id', 'payment_id', 'status', 'order_id'], 'integer'],
            [['amount'], 'number'],
            [['create_time', 'update_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'amount' => Yii::t('app', 'Amount'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'status' => Yii::t('app', 'Status'),
            'create_time' => Yii::t('app', 'Create Time'),
            'update_time' => Yii::t('app', 'Update Time'),
            'order_id' => Yii::t('app', 'Заказ'),
        ];
    }
}
