<?php

namespace app\models;

use Yii;
use app\models\Profile;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "agency".
 *
 * @property int $id
 * @property string $description
 * @property string $name
 * @property float $percent
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'name', 'percent'], 'required'],
            [['description'], 'string'],
            [['percent'], 'number'],
            [['name'], 'string', 'max' => 12],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'name' => 'Name',
            'percent' => 'Percent',
        ];
    }

    public function getGirls($id){
        
        $q = 'SELECT COUNT(user_id) FROM `profile` WHERE ((`agency_id`=1) AND (`sex`=2)) AND (NOT (`manager_id`=0))';//те у кого есть менеджер
        //var_dump($q);exit();
        $girl_count = Yii::$app -> db -> createCommand($q)->queryScalar();

        return $girl_count;



    }


    public function getTable($id)

    {   
        
        $timeNow = date('Y-m-d H:i:s');
        //var_dump($timeNow);
        $timeThen = date('Y-m-01 00:00:00');
        //var_dump($timeThen);
        //var_dump($id);
        $q= 'SELECT girl_id, SUM(amount) as sum FROM client_order WHERE agency_id = ' . $id . ' AND create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '" GROUP BY girl_id';
        //var_dump($q);
        $table = Yii::$app -> db -> createCommand($q)->queryAll();
        //var_dump($table);
        //exit();
        /*foreach ($table as $key => $value) {
           // var_dump($value['girl_id']);
            $profile = Profile ::findOne($value['girl_id']);
            $table['name']=$profile->user->username;
            //var_dump($profile->user->username);

        };*/
       
        
        return $table;
           
    }

    public function getDetail($girl_id,$id)

    {   
        
        $timeNow = date('Y-m-d H:i:s');
        //var_dump($timeNow);
        $timeThen = date('Y-m-01 00:00:00');
        //var_dump($timeThen);
        //var_dump($id);
        $q= 'SELECT girl_id, SUM(amount) as sum FROM client_order WHERE agency_id = ' . $id . ' AND create_time BETWEEN "' . $timeThen . '" AND "' . $timeNow . '" GROUP BY girl_id';
        //var_dump($q);
        $table = Yii::$app -> db -> createCommand($q)->queryAll();
        //var_dump($table);
        //exit();
        /*foreach ($table as $key => $value) {
           // var_dump($value['girl_id']);
            $profile = Profile ::findOne($value['girl_id']);
            $table['name']=$profile->user->username;
            //var_dump($profile->user->username);

        };*/
       
        
        return $table;
           
    }

    public static function getFullList() {
        $models =  self::find()->all();
        return ArrayHelper::map($models,'id','name');
    }


}
