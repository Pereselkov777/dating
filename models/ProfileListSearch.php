<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Profile;

/**
 * ProfileSearch represents the model behind the search form of `app\models\Profile`.
 */
class ProfileListSearch extends Profile
{
    public $min_age;
    public $max_age;
    public $tag_id;
    public $is_consultant;
    public $manager_id;
    public $agency_id;
    //public $tags;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','referrer_id','country_id','region_id','city_id','zodiac','sex','language',
                'age_from','age_to','height','weight','body_type','hair_type','hair_color','eyes_color','eyes_color',
                'nationality', 'occupation', 'religion','children_count', 'marital_status','drinking','smoking',
                'agency_id', 'manager_id', 'min_age', 'max_age', 'status','lang2_id','lang3_id',], 'integer'],
            [['photo','credit','referrer_id','birthday','about_me','about_ideal', 'last_active', 'themes','lang_id','tag_id',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query =  Profile::find()->orderBy('user_id DESC');

       
        //var_dump($models);
        //exit();

        // add conditions that should always apply here
        //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        //exit();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['user_id' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);
        //var_dump($params);exit();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if($this->user_id){
           // echo('HERE');
            $query->andFilterWhere([
                'user_id' => $this->user_id,
                
            ]);
           // var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
           // exit();
    

        }
        $query->andFilterWhere([
            //'user_id' => $this->user_id,
            'sex' => $this->sex,
            'country_id' => $this->country_id,
            'status' => $this->status,
            
        ]);


        if ($this->tag_id){
           // var_dump($this->tag_id);
            $query->innerJoin('{{%profile_via_tag}} as rs','rs.profile_id = {{%profile}}.user_id')->distinct();
            $query->innerJoin('{{%profile_tag}} as s','s.id = rs.tag_id')->distinct();
            $query->andFilterWhere(['s.id' => $this->tag_id]);
            //var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            //exit();
        }

        if (!empty($this->themes)) {
            //die('no empty');
            $query->andFilterWhere(['OR LIKE', 'themes', $this->themes]);
        }
        if (!empty($this->lang_id)) {
           /* $query->orFilterWhere(['like', 'lang_id', $this->lang_id])
            ->orFilterWhere(['like', 'lang2_id', $this->lang_id])
            ->orFilterWhere(['like', 'lang3_id', $this->lang_id]);
            //die('no empty');*/
            $query->andFilterWhere(['or',
            ['or like', 'lang_id', $this->lang_id],
            ['or like', 'lang2_id', $this->lang_id],
            ['or like', 'lang3_id', $this->lang_id],
            ]);
            /*var_dump($query->prepare(\Yii::$app->db->queryBuilder)->createCommand()->rawSql);
            exit();*/
           
        }

       // $q_age = '';
        if (!empty($this->min_age)) {
            $stripInt = (int) $this->min_age;
            $ageYear = date('Y', strtotime("-$stripInt years"));
            //$query->andFilterWhere(['>=','YEAR(birthday)', $ageYear]);
            $query->andWhere('YEAR(birthday) <=' . $ageYear);
           // $q_age = 'YEAR(birthday) >=' . $ageYear;
        }
        if (!empty($this->max_age)) {
            $stripInt = (int) $this->max_age;
            $ageYear = date('Y', strtotime("-$stripInt years"));
 //           $query->andFilterWhere(['<=','YEAR(birthday)', $ageYear]);
            $query->andWhere('YEAR(birthday) >=' . $ageYear);
          /*  if (!empty($q_age)) {
                $q_age .= ' AND ';
            }
            $q_age .= 'YEAR(birthday) <=' . $ageYear;*/
        }
       /* if (!empty($q_age)) {
            $query->andWhere($q_age);
        }*/
        $query->andFilterWhere(['like', 'about_me', $this->about_me])
            ;
        //echo print_r($query); exit;

        return $dataProvider;
    }
}
