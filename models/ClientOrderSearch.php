<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientOrder;

/**
 * ClientOrderSearch represents the model behind the search form of `app\models\ClientOrder`.
 */
class ClientOrderSearch extends ClientOrder
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'man_id', 'girl_id', 'agency_id', 'service_id', 'product_id'], 'integer'],
            [['amount', 'amount_agency'], 'number'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'man_id' => $this->man_id,
            'girl_id' => $this->girl_id,
            'agency_id' => $this->agency_id,
            'service_id' => $this->service_id,
            'product_id' => $this->product_id,
            'amount' => $this->amount,
            'amount_agency' => $this->amount_agency,
            'create_time' => $this->create_time,
        ]);

        return $dataProvider;
    }
}
