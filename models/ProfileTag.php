<?php

namespace app\models;
use yii\behaviors\SluggableBehavior;

use Yii;

/**
 * This is the model class for table "profile_tag".
 *
 * @property int $id
 * @property string $title Название
 * @property string $created_at Создано
 */
class ProfileTag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile_tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['created_at'], 'safe'],
            [['title','slug'], 'string', 'max' => 255],
            [['title'], 'unique'],
            [['slug'], 'unique'],
            [['weight'], 'integer'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Название'),
            'created_at' => Yii::t('app', 'Создано'),
            'slug' => Yii::t('app', 'Слаг'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                // 'slugAttribute' => 'slug',
            ],
     /*       'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'title',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ]*/
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                // Да это новая запись (insert)
                //var_dump($insert); exit();
                $this->created_at = date('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }


     /**
     * Returns tag names and their corresponding weights.
     * Only the tags with the top weights will be returned.
     * @param integer the maximum number of tags that should be returned
     * @return array weights indexed by tag names.
     */
    public static function findTagWeights($limit=20)
    {
        $models = self::find()->orderBy(['weight' => SORT_DESC])->limit($limit)->all();
        $total = 0;
        foreach($models as $model)
            $total+=$model->weight;

        $tags=[];
        if($total > 0)
        {
            foreach($models as $model)
            $tags[$model->slug]= ['weight' => 12+(int)(16*$model->weight/($total+10)), 'title' => $model->title];
            ksort($tags);
        }
        return $tags;
    }

}
