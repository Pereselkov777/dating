<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%client_order}}".
 *
 * @property int $id ID
 * @property int $man_id Мужчина
 * @property int $girl_id Девушка
 * @property int $agency_id Агентство
 * @property int $service_id Услуга
 * @property int $product_id Товар
 * @property float $amount Сумма
 * @property float $amount_agency Отчисления агентству
 * @property string $create_time Дата создания
 */
class ClientOrder extends \yii\db\ActiveRecord
{
    const SERVICE_PERSONAL_DATA = 1;
    const SERVICE_PRIVATE_PHOTO = 2;
    const SERVICE_MESSAGE_CHAT = 3;

    public static $service_list = [
        self::SERVICE_PERSONAL_DATA => 'Access by personal data',
        self::SERVICE_PRIVATE_PHOTO => 'Access by private photo',
        self::SERVICE_MESSAGE_CHAT => 'Send message to chat',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%client_order}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['man_id', 'girl_id', 'agency_id', 'service_id', 'product_id', 'amount', 'amount_agency', 'create_time'], 'required'],
            [['man_id', 'girl_id', 'agency_id', 'service_id'], 'integer'],
            [['amount', 'amount_agency'], 'number'],
            [['create_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'man_id' => Yii::t('app', 'Man'),
            'girl_id' => Yii::t('app', 'Girl'),
            'agency_id' => Yii::t('app', 'Agency'),
            'service_id' => Yii::t('app', 'Service'),
            'product_id' => Yii::t('app', 'Product'),
            'amount' => Yii::t('app', 'Amount'),
            'amount_agency' => Yii::t('app', 'Agency deductions'),
            'create_time' => Yii::t('app', 'Created at'),
        ];
    }
}
