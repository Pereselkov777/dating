<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * BalanceForm is the model behind the login form.
 *
 * @property-read User|null $user This property is read-only.
 *
 */
class BalanceForm extends Model
{
    public $amount;
    public $method_id;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['amount', 'method_id'], 'required'],
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function attributeLabels()
    {
        return [
            'amount' => 'Amount',
            'method_id' => 'Payment Method',
        ];
    }

}
