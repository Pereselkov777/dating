<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%push_token}}".
 *
 * @property string $token_id Токен
 * @property int $user_id Пользователь
 */
class PushToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%push_token}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token_id', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['token_id'], 'string', 'max' => 255],
            [['token_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'token_id' => Yii::t('app', 'Токен'),
            'user_id' => Yii::t('app', 'Пользователь'),
        ];
    }
}
