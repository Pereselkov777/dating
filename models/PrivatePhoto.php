<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "private_photo".
 *
 * @property int $id ID
 * @property int $user_id Пользователь
 * @property string $title Название
 * @property string $img_full Полное изображение
 * @property string $img_prev Изображение превью
 * @property string $img_blur Заблюренное изображение
 * @property int $status Статус
 * @property string $create_time Создано
 * @property string $update_time Обновлено
 */
class PrivatePhoto extends \yii\db\ActiveRecord
{
    public $file;
    public static $status_list = [
        0 => 'Не опубликовано',
        1 => 'Опубликовано',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'private_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['title', 'img_full', 'img_prev', 'img_blur'], 'string', 'max' => 255],

            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'title' => 'Название',
            'img_full' => 'Полное изображение',
            'img_prev' => 'Изображение превью',
            'img_blur' => 'Заблюренное изображение',
            'status' => 'Статус',
            'create_time' => 'Создано',
            'update_time' => 'Обновлено',
            'file' => 'Файл изображения',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->create_time = date('Y-m-d H:i:s');
            }
            $this->update_time = date('Y-m-d H:i:s');
            return true;
        }
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getStatusName() {
        return isset(self::$status_list[$this->status]) ? self::$status_list[$this->status] : '';
    }

    public function getFull() {
        if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $this->img_full)) {
            return '/uploads/photos/full/' . $this->img_full;
        }
        return '';
    }

    public function openAccess() {
        $is_access = false;
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->id != $this->user_id) {
                $q = 'SELECT access_data FROM private_photo_access WHERE photo_id = ' . $this->id . ' AND man_id=' . Yii::$app->user->id;
                $access_data = Yii::$app->db->createCommand($q)->queryScalar();
                if ($access_data) {
                    $is_access = true;
                }
            } else {
                $is_access = true;
            }
        }
        return $is_access;
    }

    public function can() {
        $is_access = false;
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->id != $this->user_id) {
                $q = 'SELECT access_data FROM private_photo_access WHERE photo_id = ' . $this->id . ' AND man_id=' . Yii::$app->user->id;
                $access_data = Yii::$app->db->createCommand($q)->queryScalar();
                if ($access_data) {
                    $is_access = true;
                }
            } else {
                $is_access = true;
            }
        }
        return $is_access;
    }

    public function getPrew() {
        if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $this->img_prev)) {
            return '/uploads/photos/thumbs/' . $this->img_prev;
        }
        return '';
    }

    public function getBlur() {
        if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $this->img_blur)) {
            return '/uploads/photos/blurs/' . $this->img_blur;
        }
        return '';
    }

    // https://stackoverflow.com/questions/42759135/php-best-way-to-blur-images
    // https://stackoverflow.com/questions/1248866/php-gd-gaussian-blur-effect
    public function blur($file) {
        if (mime_content_type($file) == 'image/png') {
            $image = imagecreatefrompng($file);
            $ext = 'png';
        } else {
            $image = imagecreatefromjpeg($file);
            $ext = 'jpg';
        }
        $filename = $this->user_id . '_' . Yii::$app->security->generateRandomString(30) . '.' . $ext;

        /* Get original image size */
        list($w, $h) = getimagesize($file);

        /* Create array with width and height of down sized images */
        $size = array('sm'=>array('w'=>intval($w/4), 'h'=>intval($h/4)),
            'md'=>array('w'=>intval($w/2), 'h'=>intval($h/2))
        );

        /* Scale by 25% and apply Gaussian blur */
        $sm = imagecreatetruecolor($size['sm']['w'],$size['sm']['h']);
        imagecopyresampled($sm, $image, 0, 0, 0, 0, $size['sm']['w'], $size['sm']['h'], $w, $h);

        for ($x=1; $x <=40; $x++){
            imagefilter($sm, IMG_FILTER_GAUSSIAN_BLUR, 999);
        }

        imagefilter($sm, IMG_FILTER_SMOOTH,99);
        imagefilter($sm, IMG_FILTER_BRIGHTNESS, 10);

        /* Scale result by 200% and blur again */
        $md = imagecreatetruecolor($size['md']['w'], $size['md']['h']);
        imagecopyresampled($md, $sm, 0, 0, 0, 0, $size['md']['w'], $size['md']['h'], $size['sm']['w'], $size['sm']['h']);
        imagedestroy($sm);

        for ($x=1; $x <=25; $x++){
            imagefilter($md, IMG_FILTER_GAUSSIAN_BLUR, 999);
        }

        imagefilter($md, IMG_FILTER_SMOOTH,99);
        imagefilter($md, IMG_FILTER_BRIGHTNESS, 10);

        /* Scale result back to original size */
        imagecopyresampled($image, $md, 0, 0, 0, 0, $w, $h, $size['md']['w'], $size['md']['h']);
        imagedestroy($md);

// Apply filters of upsized image if you wish, but probably not needed
//imagefilter($image, IMG_FILTER_GAUSSIAN_BLUR);
//imagefilter($image, IMG_FILTER_SMOOTH,99);
//imagefilter($image, IMG_FILTER_BRIGHTNESS, 10);
        if ($ext == 'png') {
            imagepng($image, Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $filename);
        } else {
            imagejpeg($image, Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $filename);
        }
        imagedestroy($image);
        return $filename;
    }


    public static function getStatusList() {
       return self::$status_list;
    }
}
