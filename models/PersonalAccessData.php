<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal_access_data".
 *
 * @property int $man_id Мужчина
 * @property int $girl_id Девушка
 * @property string $access_data Дата доступа
 */
class PersonalAccessData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%personal_access_data}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['man_id', 'girl_id', 'access_data'], 'required'],
            [['man_id', 'girl_id'], 'integer'],
            [['access_data'], 'safe'],
            [['man_id', 'girl_id'], 'unique', 'targetAttribute' => ['man_id', 'girl_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'man_id' => Yii::t('app', 'Мужчина'),
            'girl_id' => Yii::t('app', 'Девушка'),
            'access_data' =>  Yii::t('app', 'Дата доступа'),
        ];
    }

    
}
