<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\models;

use Yii;
use dektrium\user\models\Profile as BaseProfile;
use dektrium\user\models\User;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use  yii\db\Query;
use dosamigos\taggable\Taggable;
use app\modules\manager\models\Manager;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string  $name
 * @property string  $public_email
 * @property string  $gravatar_email
 * @property string  $gravatar_id
 * @property string  $location
 * @property string  $website
 * @property string  $bio
 * @property string  $timezone
 * @property string  $photo
 * @property float   $credit
 * @property integer $referrer_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birthday
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $city_id
 * @property integer $zodiac
 * @property integer $sex
 * @property integer $language
 * @property integer $age_from
 * @property integer $age_to
 * @property string $last_active
 * @property integer $status
 * @property string $facebook_link
 * @property integer $agency_id
 * @property integer $manager_id
 * @property integer $lang_id
 * @property integer $lang2_id
 * @property integer $lang3_id
 * @property integer $kind_id
 * @property string $themes
 * @property User    $user
 * @property ProfileTag[] $tags Теги
 *  @property Manager    $manager
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class Profile extends BaseProfile
{   
    //public $tagNames;
    public function behaviors()
    {
        return [
            'tagNames' => [
                'class' => Taggable::className(),
                'attribute' => 'tagNames',
                'name' => 'title',
                'frequency' => 'weight'
            ]
            // for different configurations, please see the code
            // we have created tables and relationship in order to
            // use defaults settings
        ];
    }
    public $_avatar;    // variable to get the picture

    public static $sex_list = [
        0=>'Please choose your value...',
        1 => 'Male',
        2 => 'Female'
    ];

    public static $marital_status_list = [
        1 => 'Not married',
        2 => 'Divorced',
        3 => 'Widower',
        4 => 'Married',
    ];

    public static $body_type_list = [
        1 => 'Slim',
        2 => 'Normal',
        3 => 'Corpulent',
    ];

    public static $hair_type_list = [
        1 => 'Short',
        2 => 'Normal',
        3 => 'Long',
        4 => 'Bald',
    ];

    public static $hair_color_list = [
        1 => 'Black',
        2 => 'Dark',
        3 => 'Blond',
        4 => 'Red',
    ];

    public static $eyes_color_list = [
        1 => 'Black',
        2 => 'Brown',
        3 => 'Blue',
        4 => 'Gray',
    ];

    public static $drinking_list = [
        1 => 'Not drinking',
        2 => 'Rarely',
        3 => 'Often',
    ];

    public static $smoking_list = [
        1 => 'Not smoking',
        2 => 'Rarely',
        3 => 'Often',
    ];

    public static $lang_list = [
        1 => 'Russian',
        2 => 'English',
        3 => 'French',
        4 => 'German',
        5 => 'Japanese',
        6 => 'Chinese',
        7 => 'Spanish',
    ];

    public static $kind_list = [
        1 => 'Обычный',
        2 => 'Разрешение получено',
        3 => 'Разрешение не получено',
    ];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] =  [['photo','first_name','last_name', 'facebook_link'], 'string', 'max' => 255];
        $rules[] =  [['photo','credit','referrer_id','birthday','about_me','about_ideal','themes','_avatar', 'last_active','tagNames',], 'safe'];
        $rules[] =  [['referrer_id','country_id','region_id','city_id','zodiac','sex','language',
            'age_from','age_to','height','weight','body_type','hair_type','hair_color','eyes_color','eyes_color',
            'nationality', 'occupation', 'religion','children_count', 'marital_status','drinking','smoking',
            'agency_id', 'manager_id', 'status', 'lang_id', 'lang2_id', 'lang3_id', 'kind_id'], 'integer'];
        //$rules[] =  [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'];
        //$rules[] = [['_avatar'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'];
        $rules[] =  [['first_name','sex'], 'required'];
        $rules[] =  [['tagNames'], 'safe'];

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['photo'] = Yii::t('app', 'Avatar');
        $labels['credit'] = Yii::t('app', 'Credits');
        $labels['referrer_id'] = 'Реферер';
        $labels['first_name'] = Yii::t('app', 'First Name');
        $labels['last_name'] = Yii::t('app', 'Last Name');
        $labels['birthday'] = Yii::t('app', 'Birthday');
        $labels['zodiac'] = Yii::t('app', 'Zodiac');
        $labels['sex'] = Yii::t('app', 'Sex');
        $labels['language'] = Yii::t('app', 'Language');
        $labels['about_me'] = Yii::t('app', 'About Me');
        $labels['about_ideal'] = Yii::t('app', 'About Ideal');
        $labels['age_from'] = Yii::t('app', 'Partner`s Minimum Age');
        $labels['age_to'] = Yii::t('app', 'Partner`s Maximum Age');
        $labels['height'] = Yii::t('app', 'Height, sm');
        $labels['weight'] = Yii::t('app', 'Weight, kg');
        $labels['body_type'] = Yii::t('app', 'Body Type');
        $labels['hair_type'] = Yii::t('app', 'Hair Type');
        $labels['hair_color'] = Yii::t('app', 'Hair Color');
        $labels['eyes_color'] = Yii::t('app', 'Eyes Color');
        $labels['nationality'] = Yii::t('app', 'Nationality');
        $labels['country_id'] = Yii::t('app', 'Country');
        $labels['region_id'] = Yii::t('app', 'Region');
        $labels['city_id'] = Yii::t('app', 'City');
        $labels['occupation'] = Yii::t('app', 'Occupation');
        $labels['religion'] = Yii::t('app', 'Religion');
        $labels['children_count'] = Yii::t('app', 'Children Count');
        $labels['marital_status'] = Yii::t('app', 'Marital Status');
        $labels['drinking'] = Yii::t('app', 'Drinking');
        $labels['smoking'] = Yii::t('app', 'Smoking');
        $labels['agency_id'] = Yii::t('app', 'Агенство');
        $labels['manager_id'] = Yii::t('app', 'Менеджер');
        $labels['last_active'] = Yii::t('app', 'Последняя активность');
        $labels['status'] = Yii::t('app', 'Status');
        $labels['facebook_link'] = Yii::t('app', 'Facebook Link');
        $labels['lang_id'] = Yii::t('app', 'Language 1');
        $labels['lang2_id'] = Yii::t('app', 'Language 2');
        $labels['lang3_id'] = Yii::t('app', 'Language 3');
        $labels['themes'] = Yii::t('app', 'Themes');
        $labels['kind_id'] = Yii::t('app', 'Kind');
        return $labels;
    }

      /*  public function beforeSave($insert)
    {
        if (is_string($this->_avatar) && strstr($this->_avatar, 'data:image')) {
            $uploadPath = Yii::getAlias('@webroot') . '/uploads/profile/photo';    // set a directory to save picture
            $data = $this->_avatar;
            $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
            $fileName = Yii::$app->security->generateRandomString() . '.png';   // generate picture name
            file_put_contents($uploadPath . DIRECTORY_SEPARATOR . $fileName, $data);

            if (!empty($this->avatar)) {    // "avatar" model attribute which stores picture name
                unlink(Yii::getAlias($uploadPath . DIRECTORY_SEPARATOR . $this->avatar));   // delete old picture
            }

        

            $this->photo = '/uploads/profile/photo/' . $fileName;  // set new picture name to attribute
        }

        return parent::beforeSave($insert);
    }
    
   /* public function upload()
    {
        echo('HERE');
        if ($this->validate()) {
            $name = $this->user_id . '_' . time() . '.' . $this->_avatar->extension;
            $this->_avatar->saveAs(Yii::getAlias('@webroot') . '/uploads' . $name);
            return true;
        } else {
            return false;
        }
    }*/
    
    public function accessPersonalData( $girl_id) {
       
        $is_access = false;// кнопку будет видно только в случае если $is_access = true
        if (!Yii::$app->user->isGuest) {
          //  echo($girl_id);
            $man_id = (Yii::$app->user->id);
           // echo($man_id);
            $q = 'SELECT access_data FROM personal_access_data WHERE  girl_id = ' . $girl_id . '  AND man_id=' . $man_id;
            $access_data = Yii::$app->db->createCommand($q)->queryScalar();
            //die('0');
            if($access_data){
              //  echo('Norm');
              //  var_dump($access_data);
                $is_access = true;//меняем на true потому что уже купил получается не показываем
                //die('1');
                return $is_access;
            } else {
              //  echo('NO');
               // var_dump($girl_id == $man_id);
                if($girl_id == $man_id){
                    $is_access = true;//меняем на true потому что man = girl чтобы не показал кнопку
                   // die('2');
                    return $is_access;
                }
               // var_dump($girl_id, $man_id);
                //die('3');
                $is_access = false;//не меняем на true потому что man != girl можно показывать
                return $is_access;
            }
           //echo($this->user_id);
           //echo($girl_id);
        } else
        {
            //die('4');
            $is_access = false;//показываем 
            return $is_access;
        }
        
       
        
    }


    public function getBalance()
        {           
            $man_id = Yii::$app->user->id;    
            
            /*$credit = (new \yii\db\Query())
            ->select(['credit'])
            ->from('profile')
            ->where(['user_id' => $man_id ])
            ->limit(10)
            ->queryScalar();*/
            $credit = Yii::$app->db->createCommand('SELECT credit FROM profile WHERE user_id =' .(int)$man_id . '')
            ->queryScalar();

            $credit = ((int)$credit);
            if($credit >= 5){
                
                $b = "YES";
                return $b;
            } else{
                //echo($credit);
                $b = "NO";
                return $b;
            }
        }
     
        
    public function getAvatarId($id)
    {           
        $user_id = Yii::$app->user->id;    
        
        /*$credit = (new \yii\db\Query())
        ->select(['credit'])
        ->from('profile')
        ->where(['user_id' => $man_id ])
        ->limit(10)
        ->queryScalar();*/
        $avatar = Yii::$app->db->createCommand('SELECT avatar_id FROM profile WHERE user_id =' .(int)$user_id . '')
        ->queryScalar();

        $avatar = ((int)$credit);
        if($cavatar != (int)$id){
            
            $a = "YES";
            return $a;
        } else{
            //echo($credit);
            $a = "NO";
            return $a;
        }
    }

    public function getAvatarUrl($size = 200)
    {
        if (!empty($this->photo)) {
            return $this->photo;
            
        } else  {
            return parent::getAvatarUrl($size);
        } 
    }

    public function getReferrer()
    {
        return $this->hasOne(User::className(), ['id' => 'referrer_id']);
    }

    public static function getSexList() {
        $result = [];
        foreach (self::$sex_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getMaritalStatusList() {
        $result = [];
        foreach (self::$marital_status_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getBodyTypeList() {
        $result = [];
        foreach (self::$body_type_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getHairTypeList() {
        $result = [];
        foreach (self::$hair_type_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getHairColorList() {
        $result = [];
        foreach (self::$hair_color_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getEyesColorList() {
        $result = [];
        foreach (self::$eyes_color_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getDrinkingList() {
        $result = [];
        foreach (self::$drinking_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getSmokingList() {
        $result = [];
        foreach (self::$smoking_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getLangList() {
        $result = [];
        foreach (self::$lang_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }

    public static function getKindList() {
        $result = [];
        foreach (self::$kind_list as $key=>$val) {
            $result[$key] = Yii::t('app', $val);
        }
        return $result;
    }


    public function getAge() {
        //echo($this->children_count);
        //echo('HERE');
        //var_dump($this);
        //$birhday = new DateTime($this ->birthday);
        //$to   = new DateTime('today');

        //$age =$birhday->diff($to)->y; а почему так нельзя ?
        $age = date_diff(date_create($this ->birthday), date_create('today'))->y;
        //echo($age);
        //echo($age);
        return $age;
    }

    public function getHairTypeName() {
        if (isset($this->hair_type)) {
            return self::$hair_type_list[$this->hair_type];
        }else{
            return null;
        }
         
    }

    public function getBodyTypeName() {
        if (isset($this->body_type)) {
            return self::$body_type_list[$this->body_type];
        }else{
            return null;
        }
        
    }

   public function getEyesColorName() {
        if (isset($this->eyes_color)) {
            return self::$eyes_color_list[$this->eyes_color];
            } else{
                return null;
            }

    }

    public function getHairColorName() {
        if (isset($this->hair_color)) {
            return self::$hair_color_list[$this->hair_color];
        } else{
            return null;
        }

    }

    public function getDrinkingTypeName() {
        if (isset($this->drinking)) {
            return self::$drinking_list[$this->drinking];
            } else{
                return null;
            }

    }

    public function getSexTypeName() {
        if (isset($this->sex)) {
            return self::$sex_list[$this->sex];
            } else{
                return null;
            }

    }

    public function getMaritalStatusName() {
        if (isset($this->marital_status)) {
            //var_dump($this->marital_status);
            return self::$marital_status_list[$this->marital_status];
            } else{
                return null;
            }

    }

    public function getSmokingTypeName() {
        if (isset($this->smoking)) {
            return self::$smoking_list[$this->smoking];
            } else{
                return null;
            }

    }

    public function getLangName() {
        if (isset($this->lang_id)) {
            return self::$lang_list[$this->lang_id];
        } else{
            return null;
        }

    }

    public function getLang2Name() {
        if (isset($this->lang2_id)) {
            return self::$lang_list[$this->lang2_id];
        } else{
            return null;
        }

    }

    public function getLang3Name() {
        if (isset($this->lang3_id)) {
            return self::$lang_list[$this->lang3_id];
        } else{
            return null;
        }

    }

    public function getCountryTypeName() {
        if (isset($this->country_id)) {
      $a =    \app\components\GeoHelper::getCountries()[$this->country_id];
      //var_dump($a); 
      return $a;
        } else{
            return null;
        }   
           
            
    }
    public function getRegionTypeName() {
        if ($this->country_id !== null || $this->region_id !== null ) {
        //var_dump($this->region_id);
        $b =    \app\components\GeoHelper::getRegions($this->country_id)[$this->region_id];
        //var_dump($b);
        //$c =  \app\components\GeoHelper::getRegions($this->country_id)[$this->region_id];
        //var_dump($c);
        //1506272

        return $b;
        } else{
            return null;
        }   
             
              
    }

    public function getCityTypeName() {
        if ($this->region_id !== null || $this->city_id !== null ) {
        //var_dump($this->region_id);
        $d =    \app\components\GeoHelper::getCities($this->region_id)[$this->city_id];
        //var_dump($b);
        //$e =  \app\components\GeoHelper::getCities($this->country_id)[$this->region_id];
        //var_dump($c);
        //1506272

        return $d;
        } else{
            return null;
        }

        
        
        
             
              
    }

    public function getPhoto(){
        return $this->hasMany(Photo::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(ProfileTag::className(), ['id' => 'tag_id'])->viaTable('profile_via_tag', ['profile_id' => 'user_id']);
    }

    public function getManagers(){
        return $this->hasMany(Manager::className(), ['id' => 'user_id']);
    }





    
    

    
}


   

    


