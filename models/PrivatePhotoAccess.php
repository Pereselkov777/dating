<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%private_photo_access}}".
 *
 * @property int $photo_id Фото
 * @property int $man_id Мужчина
 * @property int $girl_id Девушка
 * @property string $access_data Дата доступа
 */
class PrivatePhotoAccess extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%private_photo_access}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo_id', 'man_id', 'girl_id', 'access_data'], 'required'],
            [['photo_id', 'man_id', 'girl_id'], 'integer'],
            [['access_data'], 'safe'],
            [['photo_id', 'man_id'], 'unique', 'targetAttribute' => ['photo_id', 'man_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'photo_id' => Yii::t('app', 'Фото'),
            'man_id' => Yii::t('app', 'Мужчина'),
            'girl_id' => Yii::t('app', 'Девушка'),
            'access_data' => Yii::t('app', 'Дата доступа'),
        ];
    }
}
