<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\models;

use dektrium\user\models\User;
use Yii;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;

/**
 * Registration form collects user input on registration process, validates it and creates new User model.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RegistrationForm extends BaseRegistrationForm
{
    public $acordul_tc;
    public $referrer_id;
    public $sex;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] =
            [['sex'], 'required'];
        $rules[] =
            [['referrer_id', 'sex'], 'integer'];
        /*$rules[] =
            [['referrer_id'], 'checkReferrer'];*/
        $rules[] =
            ['acordul_tc', 'required', /*'on' => ['register'],*/ 'requiredValue' => 1,
                'message' => Yii::t('app','Please Tick Above To Agree Terms && Condition')];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'    => Yii::t('user', 'Email'),
            'username' => Yii::t('user', 'Username'),
            'password' => Yii::t('user', 'Password'),
            'referrer_id' => Yii::t('user', 'Реферер'),
            'sex' => Yii::t('app', 'Sex'),
            'acordul_tc' => Yii::t('app', 'I agree to the {link:create}Terms and Conditions{/link}',[
                'link:create' => '<a href="/agreement" target="_blank">',
                '/link' => '</a>']),
        ];
    }

    public function checkReferrer($attribute_name, $params)
    {
        $model = User::findOne($this->{$attribute_name});
        if ($model === null) {
            $this->addError($attribute_name, Yii::t('user', 'Такой реферер не найден'));
            return false;
        }

        return true;
    }

    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     */
    public function register()
    {
        parent::register();
 //       if (isset(Yii::$app->request->cookies['referrer'])) {
            $user = \dektrium\user\models\User::findOne(['username' => $this->username]);
           // var_dump($user);
            if ($user !== null) {
                $profile = Profile::findOne(['user_id' => $user->id]);
                $profile->referrer_id = $this->referrer_id;
                $profile->sex = $this->sex;
                $profile->status = 0;
//            $profile->referrer_id = (int)Yii::$app->request->cookies['referrer']->value;
                $profile->save(false);
           }
//        }
        return true;
    }


}
