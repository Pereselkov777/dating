<?php
namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Обертка для использования Geo
 *
 * @link https://github.com/php-telegram-bot
 *
 */
class VKHelper extends \yii\base\Component
{
    const VERSION = 5.103;
    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
    }

    public static function getUserInfo($user_id) {
        $service_url = 'https://api.vk.com/method/users.get';
        $access_token = Yii::$app->params['vk_access_token'];
        $curl_get_data = [
            "user_ids" => $user_id,
            'fields' => 'photo_50,city,verified,sex,bdate,domain',
            "name_case" => 'Nom',
            "v" => self::VERSION,
            "access_token" => $access_token,
        ];
        $user_info = self::query($service_url, $curl_get_data);
        sleep(4);
        if (isset($user_info['response'][0])) {
            return $user_info['response'][0];
        }
        return $user_info;
    }

    public static function query($service_url,$curl_get_data = []){
        $s_data = '?';
        foreach ($curl_get_data as $key=>$val){
            $s_data .=$key.'='.$val.'&';
        }
        $s_data = substr($s_data,0,-1);
        $url = $service_url.$s_data;
        echo $url;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $headers = ['Content-Type: application/json; encoding=utf-8', 'Accept: application/json'];
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $curl_response = curl_exec($curl);
        if (!$curl_response || curl_errno($curl) !== CURLE_OK)
        {
            echo 'cURL error (' . curl_errno($curl) . '): ' . curl_error($curl);
        }
        curl_close($curl);
        $result = json_decode($curl_response, true);
        return $result;
    }

}