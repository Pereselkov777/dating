<?php
namespace app\components;

use Yii;
use yii\bootstrap4\Dropdown;
use yii\bootstrap4\ButtonDropdown;

class LanguageDropdown extends Dropdown
{
    private static $_labels;

    private $_isError;
    private $current;

    public static function getLanguagesList() {
        $current = 'en';
        $route = Yii::$app->controller->route;
        if ($route == 'site/index') {
            $route = '';
        }
        $appLanguage = Yii::$app->language;
        $params = $_GET;
        array_unshift($params, '/' . $route);

        foreach (Yii::$app->urlManager->languages as $language) {
            $isWildcard = substr($language, -2) === '-*';
            if (
                $language === $appLanguage ||
                // Also check for wildcard language
                $isWildcard && substr($appLanguage, 0, 2) === substr($language, 0, 2)
            ) {
                $current = $language;
                continue;   // Exclude the current language
            }
            if ($isWildcard) {
                $language = substr($language, 0, 2);
            }
            $params['language'] = $language;
            $items[] = [
                'label' => self::label($language),
                'url' => $params,
                'class' => 'rd-megamenu-item',
            ];
        }
        return [
          'label' => self::$_labels[$current],
          'items' => $items,
        ];

    }
    public function init()
    {
        $this->current = 'en';
        $route = Yii::$app->controller->route;
        $appLanguage = Yii::$app->language;
        $params = $_GET;
        $this->_isError = $route === Yii::$app->errorHandler->errorAction;

        array_unshift($params, '/' . $route);

        foreach (Yii::$app->urlManager->languages as $language) {
            $isWildcard = substr($language, -2) === '-*';
            if (
                $language === $appLanguage ||
                // Also check for wildcard language
                $isWildcard && substr($appLanguage, 0, 2) === substr($language, 0, 2)
            ) {
                $this->current = $language;
                continue;   // Exclude the current language
            }
            if ($isWildcard) {
                $language = substr($language, 0, 2);
            }
            $params['language'] = $language;
            $this->items[] = [
                'label' => self::label($language),
                'url' => $params,
                'class' => 'rd-megamenu-item',
            ];
        }
        parent::init();
    }

    public function run()
    {
        // Only show this widget if we're not on the error page
        if ($this->_isError) {
            return '';
        } else {
           /* echo ButtonDropdown::widget([
                'label' =>   self::$_labels[$this->current],
                'dropdown' => [
                    'items' => $this->items,
                ],
            ]);*/
         /*   echo '<li class="rd-nav-item"><a class="rd-nav-link" href="#">' . self::$_labels[$this->current] . '</a>';
            echo '<ul class="rd-menu rd-navbar-dropdown">';
            foreach ($this->items as $item) {
                echo '<li class="rd-dropdown-item"><a class="rd-dropdown-link" href="'.Yii::$app->urlManager->createUrl($item['url']).'">'.$item['label'].'</a></li>';
            }
            echo '</ul></li>';*/
            return [
              'label' => self::$_labels[$this->current],
              'items' => $this->items,
            ];
           // return parent::run();
        }
    }

    public static function label($code)
    {
        if (self::$_labels === null) {
            self::$_labels = [
                'ru' => Yii::t('language', 'Russian'),
                'de' => Yii::t('language', 'German'),
//                'fr' => Yii::t('language', 'French'),
                'en' => Yii::t('language', 'English'),
            ];
        }

        return isset(self::$_labels[$code]) ? self::$_labels[$code] : null;
    }
}