<?php
namespace app\components;


use MenaraSolutions\Geographer\City;
use MenaraSolutions\Geographer\Country;
use MenaraSolutions\Geographer\Earth;
use MenaraSolutions\Geographer\State;
use yii\helpers\ArrayHelper;

/**
 * Обертка для использования Geo
 *
 * @link https://github.com/php-telegram-bot
 *
 */
class GeoHelper extends \yii\base\Component
{

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
    }

    public static function getCountries() {
        $earth = new \MenaraSolutions\Geographer\Earth();
        $country_list = ArrayHelper::map($earth->getCountries()->setLocale(strtoupper(\Yii::$app->language))->sortBy('name')->toArray(), 'geonamesCode', 'name');
/*        $res = [];
        foreach ($country_list as $item) {
            $res[] = ['id' => $item['geonamesCode'], 'name' => $item['geonamesCode']];
        }*/
        return $country_list;
    }

    public static function getCountryName($id) {
        $earth = new \MenaraSolutions\Geographer\Earth();
        $country = $earth->getCountries()->setLocale(strtoupper(\Yii::$app->language))->findOne(['geonamesCode' => $id]);
        if (empty($country)){
            return '';
        }
        return $country->getName();
    }

    public static function getCountryId($name) {
        $earth = new \MenaraSolutions\Geographer\Earth();

        $country = $earth->getCountries()->setLocale(strtoupper(\Yii::$app->language))->findOne(['name' => $name]);
        $result = $country['geonamesCode'];
        return $result;
    }

    public static function getRegions($country_id) {
        $earth = new Earth();
        $country = $earth->getCountries()->findOne(['geonamesCode' => $country_id]);
        if ($country) {
            $region_arr = $country->getStates()->setLocale(strtoupper(\Yii::$app->language))->sortBy('name')->toArray();
            $regions = ArrayHelper::map($region_arr, 'geonamesCode', 'name');
        } else {
             file_put_contents('telegram.log', 'not found $country_id = ' . print_r($country_id, true) ."\n", FILE_APPEND | LOCK_EX);
            $regions = [];
        }
/*        var_dump($region_arr);
        var_dump($regions);

        exit;*/

        return $regions;
    }

    public static function getRegionName($id) {
        if (empty($id)){
            return '';
        }
        $region = State::build($id);
        if (empty($region)){
            return '';
        }
        return $region->setLocale(strtoupper(\Yii::$app->language))->getName();
    }

    public static function getRegionId($country_id, $name) {
        $result = null;
        $earth = new \MenaraSolutions\Geographer\Earth();
        $country = $earth->getCountries()->setLocale(strtoupper(\Yii::$app->language))->findOne(['geonamesCode' => $country_id]);
        if ($country) {
            $result = $country->getStates()->setLocale(strtoupper(\Yii::$app->language))->findOne(['name' => $name]);
        }
        return $result ? $result ['geonamesCode'] : null;
    }

    public static function getCityName($id) {
        if (empty($id)) {
            return '';
        }
        $city = City::build($id);
        if (empty($city)){
            return '';
        }
        return $city->setLocale(strtoupper(\Yii::$app->language))->getName();
    }

    public static function getCityId($region_id, $name) {
        $region = State::build($region_id);
        $result = $region->getCities()->setLocale(strtoupper(\Yii::$app->language))->findOne(['name' => $name]);
        return $result ? $result ['geonamesCode'] : null;
    }

    public static function getCities($region_id) {
        $region = State::build($region_id);
 //       var_dump($region); exit;
        $cities = ArrayHelper::map($region->getCities()->setLocale(strtoupper(\Yii::$app->language))->sortBy('name')->toArray(), 'geonamesCode', 'name');
        return $cities;
    }

}