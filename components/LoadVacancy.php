<?php
namespace app\components;

use app\models\VkAds;
use app\models\VkGroup;
use app\modules\telegram\models\TelegramChannelVk;
use Yii;
use yii\base\Component;
use app\components\GenerateAlias;
use yii\helpers\ArrayHelper;

class LoadVacancy  extends Component
{
    public static $profession;
    public static $title_key = ['!','?',':',"\n"];
    public static $feature_job = [
        'работ'=>[
            'before'=>[
                'опыт'=>1,
                'стаж'=>1,
                'график'=>1,
                'место'=>1,
                'преимущества'=>1,
                'выход'=>2,
                'условия'=>5,
                'предлагае'=>5,
                'приглашае'=>5,
            ],
            'after'=>[
                'с'=>1,
                'в'=>1,
                'для'=>1,
            ],
        ],
        'з/п'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],
        'плат'=>[
            'before'=>[
                'заработн'=>1,
            ],
            'after'=>[
            ],
        ],
        'доход'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],
        'ваканси'=>[
            'before'=>[
                'открыт'=>1,
                'нов'=>1,
            ],
            'after'=>[
            ],
        ],
        'оклад'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],
        'трудоустройство'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],
        'ваканс'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],
        'резюме'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],
        'требу'=>[
            'before'=>[
            ],
            'after'=>[
            ],
        ],

    ];

    public static $feature_resume = [
        'ищу'=>[
            'before'=>[
            ],
            'after'=>[
                'работ'=>4,
            ],
        ],
        'ищю'=>[
            'before'=>[
            ],
            'after'=>[
                'работ'=>4,
            ],
        ],
        'ищет'=>[
            'before'=>[
            ],
            'after'=>[
                'работ'=>4,
            ],
        ],
        'ищем'=>[
            'before'=>[
            ],
            'after'=>[
                'работ'=>4,
            ],
        ],
    ];

    public static function loadSocial()
    {
        $q = 'select * from {{last_read}} where is_read=0';
   //     $q = 'select * from {{last_read}} where id=47';
        $resul = Yii::$app->db->createCommand($q)->queryAll();
        $sovpad = 0;
        if (is_array($resul) && count($resul)>0){
            foreach ($resul as $res ){
                $social = $res['social'];
                $group_id = $res['group_id'];
                $id = $res['id'];

                $cond = 'social="'.$social.'" and group_id='.$group_id.' and id='.$id;
           echo $cond."\n";
                $text = $res['text_array'];
                $vacancy = json_decode($text);
                $sovpad += LoadVacancy::index($vacancy,$social,$group_id);
                $q = 'update {{last_read}} set is_read=1 where '.$cond;
                $res = Yii::$app->db->createCommand($q)->execute();
            }
        }
   //     var_dump($sovpad); exit;
        return $sovpad;

    }

    public static function index($vacancies,$social = 0, $group = 0)
    {
        $sovpad =$sovpad_resume =  10;
        if (is_array($vacancies) && count($vacancies)>0){
            $social =  (empty($social)) ? 'VK' : $social;
            $group = (empty($group)) ? -12248221 : $group;
/*            $q = 'select city_id from {{vk_group}} WHERE social="'.$social.'" and group_id='.$group;
            $city_id = Yii::$app->db->createCommand($q)->queryScalar();*/
            $city_id = 1;
            if (empty($city_id)){
                echo 'city_id empty for social='.$social.' and group_id='.$group;
                exit;
            }
//            self::getProfession();
            $sovpad = $sovpad_resume = 0;
            //$i = 1;
            foreach ($vacancies as $vacancy){
                $id = $vacancy->id;
                $date_social = date('Y-m-d H:i:s',$vacancy->date);
//if ($id == 551694){
                $text = $vacancy->text;
                //if (! empty($text)){
                   /* $sentes_title = explode('.',$text);
                    $text = mb_strtolower($text);
                    $sentes = explode('.',$text);
                    foreach ($sentes as $val){
                        $arr_word = explode(' ',$val);
                        $arr_sentes[] =$arr_word;
                    }
                    $title = self::get_title($sentes_title);
                    $slug = GenerateAlias::generateSlugVacancy(0,$title);
                    $is_job = true;
                  //  $is_resume = self::is_job($arr_sentes,1);
                    if (empty($is_resume)){
                        $is_job = self::is_job($arr_sentes);
                    }else{
                        $is_job = 0;
                    }*/
                   $attachments = isset($vacancy->attachments) ? json_encode($vacancy->attachments) : json_encode([]);
                   $signer_id =  isset($vacancy->signer_id) ? $vacancy->signer_id : 0;
                    $q = 'insert ignore {{%vk_ads}} (social,group_id,id,text,city_id,date_create,date_social, attachments, signer_id) values ';
                    $q .= '("'.$social.'",'.$group.','.$id.',:text,'.$city_id.',NOW(),"'.$date_social.'", :attachments, :signer_id)';
                    $res = Yii::$app->db->createCommand($q,[':text'=>$text, ':attachments' => $attachments, ':signer_id' => $signer_id])->execute();
                    if ($res) {
                        $modelGroup = VkGroup::findOne(['group_id' => $group]);
                        if ($modelGroup !== null) {
                            $channels = TelegramChannelVk::findAll(['vk_group_id' => $modelGroup->id]);
                            foreach ($channels as $modelChannel) {
                                $channel_id = $modelChannel->channel_id;
                                $q = 'insert ignore {{%vk_ads_telegram_channel}} (channel_id,ads_id,bot_id,vk_group_id,is_send) values ';
                                $q .= '('.$channel_id.','.$id.','.$modelChannel->channel->bot_id.', '.$modelGroup->id.',0)';
                                $res_ads = Yii::$app->db->createCommand($q)->execute();
                            }

                            $q = "SELECT id FROM dating_vk WHERE status = 1 AND group_id = :group limit 1";
                            $id_dating = Yii::$app->db->createCommand($q,[':group'=>$modelGroup->id])->queryScalar();
                            if (!empty($id_dating) && !empty($signer_id)) {
                                $res_ancet = LoadVacancy::setAncet($text,$id,$signer_id,$group,$date_social, $attachments);
                            }
                        }
                    }
                  /*  if ($i >= 19) {
                        $i = 0;
                        echo '19 message send. Sleep 2s' . "\n";
                        sleep(2);
                    }
                    $i++;*/
                    if ( !$res){ $sovpad ++; }
               // }

//}
            }

        }
        return $sovpad;
    }

/*
    public static function indexStart($vacancies,$social = 0, $group = 0)
    {
        $social =  (empty($social)) ? 'VK' : $social;
        $group = (empty($group)) ? -12248221 : $group;
        $q = 'select city_id from {{vacancy_group}} WHERE social="'.$social.'" and group_id='.$group;
        $city_id = Yii::$app->db->createCommand($q)->queryScalar();
        if (empty($city_id)){
            echo 'city_id empty for social='.$social.' and group_id='.$group;
            exit;
        }
        //   var_dump(self::$profession); exit;
        self::getProfession();
        $no_category = '';
        foreach ($vacancies as $vacancy){
            $id = $vacancy->id;
            $date_social = $vacancy->date;
            $text = preg_replace("/ +/", " ", $vacancy->text);
            $arr_sentes = [];
            if (! empty($text)){
                $sentes_title = explode('.',$text);
                $text = mb_strtolower($text);
                $sentes = explode('.',$text);
                foreach ($sentes as $val){
                    $arr_word = explode(' ',$val);
                    $arr_sentes[] =$arr_word;
                }
                $title = self::get_title($sentes_title);
                $slug = GenerateAlias::generateSlugVacancy(0,$title);
                $is_job = self::is_job($arr_sentes);
            }else{
                $is_job = 0;
            }
//if ($id == 551989){
            // var_dump($text);

            $table = 'vacancy';
            $new_text = ($vacancy->text);
            if ($is_job){
                $category_list = self::get_category($arr_sentes);
                if (empty($category_list)){
                    //    echo 'no category for id='.$id."\n";
                    $no_category .= $id.',';
                    $is_category = 0;
                }else{
                    foreach ($category_list as $category){
                        $q = 'insert ignore {{vacancy_category}} (category_id,social,group_id,id) values ';
                        $q .= '('.$category['category_id'].',"'.$social.'",'.$group.','.$id.')';
                        $res = Yii::$app->db->createCommand($q)->execute();
                    }
                    $is_category = 1;
                }
//                $raw_text = addslashes($new_text);
                $q = 'insert ignore {{'.$table.'}} (social,group_id,id,text,city_id,date_create,is_category,title,slug,date_social) values ';
                $q .= '("'.$social.'",'.$group.','.$id.',:text,'.$city_id.',NOW(),'.$is_category.',:title,:slug,"'.$date_social.'")';
                $res = Yii::$app->db->createCommand($q,[':text'=>$new_text,':title'=>$title,':slug'=>$slug])->execute();
                //        var_dump($res);
            }else{
                $table .= '_no';
                $q = 'insert ignore {{'.$table.'}} (social,group_id,id,text,city_id,date_create,date_social) values ("'.$social.'",';
                $q .= $group.','.$id.',:text,'.$city_id.',NOW(),"'.$date_social.'")';
                $res = Yii::$app->db->createCommand($q,[':text'=>$new_text])->execute();
            }
//}
        }
        if (empty($no_category)){
            return 0;
        }else{
            $no_category = substr($no_category,0,-1);
            return $no_category;
        }
    }
*/

    public static function setAncet($text, $vk_ads_id, $signer_id, $group_id,$date_social, $attachments)
    {
        $user_info = VKHelper::getUserInfo($signer_id);
        if (isset($user_info['deactivated'])) {
            if ($user_info['deactivated'] == 'deleted' || ($user_info['deactivated'] == 'banned')) {
                return false;
            }
        }
        $name = (isset($user_info['first_name']) ? $user_info['first_name'] : '') . ' ' . (isset($user_info['last_name']) ? $user_info['last_name'] : '');
        $login = (isset($user_info['domain']) ? $user_info['domain'] : '');
        $birthday = (isset($user_info['bdate']) ? $user_info['bdate'] : '');
        if ($birthday) {
            $arr = explode('.', $birthday);
            if (count($arr) == 3) {
                $birthday = date('Y-m-d', strtotime($birthday));
            } else {
                $birthday = '';
            }
        }
        if (isset($user_info['city'])) {
            if (isset($user_info['city']['id'])) {
                $city = $user_info['city']['id'];
            }
            if (isset($user_info['city']['title'])) {
                $city_name = $user_info['city']['title'];
            }
        } else {
            $city = '';
            $city_name = '';
        }

        $partern = '#(\=+)(\s*)Подписывайтесь(.+)(\s*)(.+)#';
        $text = preg_replace($partern,'',$text);

        $partern = '/[＃|#]([а-яА-Я-]+)[ |\s]{0,1}/iu';
        $res_match = preg_match($partern,$text,$match);
        $city_vrem = (isset($match[1]) ? $match[1] : '');
        if (!empty($city_vrem)) {
            $q = 'SELECT city_id FROM _cities WHERE title_ru = :city LIMIT 1';
            $city_vrem_id = Yii::$app->db_geodata->createCommand($q, [':city' => $city_vrem])->queryScalar();
            if ($city_vrem_id) {
                $city = $city_vrem_id;
                $city_name = $city_vrem;
            }
//            var_dump( $city_vrem_id);
//            var_dump( $city_vrem);
//            exit;
        }

        if (!empty($birthday)) {
            $age = self::getAge($birthday);
        } else {
            $parterns = ['/(＃|#)(\d+)/iu',
                '/Возраст[^\d]+?(\d+)/iu',
                '/Мне[^\d]+?(\d+)/iu',
                '#(\d+)[ ]*лет[ |\s]*#iu',
                '#(\d+)[ ]*год #iu',
            ];
            foreach ($parterns as $partern) {
                $res_match = preg_match($partern,$text,$match);
                $max = count($match);
                $age = ($max > 1 ? $match[$max-1] : 0);
                if (!empty($age) && $age > 15) {
                    break;
                }
            }
            if (!empty($age)) {
                $now = new \DateTime();
                $now->modify('-'.$age.' years');
                $birthday = $now->format('Y-m-d');
            } else {
                $partern = '/родил[^\d]+(\d{1,2}\.\d{1,2}\.\d{4})[ ]*год/iu';
                $res_match = preg_match($partern,$text,$match);
//        var_dump($match); exit;
                $birthday = (isset($match[1]) ? $match[1] : 0);
                if (!empty($birthday)){
                    list($day,$month,$year) = explode('.',$birthday);
                    $birthday = $year.'-'.$month.'-'.$day;
                    $age = self::getAge($birthday);
                } else {
                    $birthday = null;
                    $age = null;
                }
            }
        }

    //    var_dump($text); var_dump($birthday); var_dump($age); exit;
        $partern = '#Рост[^\d]+(\d+)#iu';
        $res_match = preg_match($partern,$text,$match);
        $height = (isset($match[1]) ? $match[1] : 0);
        if (empty($height)) {
            $partern = '#(\d+)[ ]*см#iu';
            $res_match = preg_match($partern,$text,$match);
            $height = (isset($match[1]) ? $match[1] : 0);
        }

        $partern = '/Вес[^\d]+(\d+)/iu';
        $res_match = preg_match($partern,$text,$match);
        $weight = (isset($match[1]) ? $match[1] : 0);
        if (empty($weight)) {
            $partern = '#(\d+)[ ]*кг#iu';
            $res_match = preg_match($partern,$text,$match);
            $weight = (isset($match[1]) ? $match[1] : 0);
        }

        $publish_time = $date_social;
        $status = 1;
        $q = 'SELECT id, alias, group_id FROM vk_group WHERE social="VK" AND group_id='.$group_id;
        $vkGroup = Yii::$app->db->createCommand($q)->queryOne();
        $vk_link = 'https://vk.com/'.$vkGroup['alias'].'?w=wall'.$vkGroup['group_id'].'_'.$vk_ads_id;
        //1- женищина 2 - мужчина
        $sex = (isset($user_info['sex']) ? $user_info['sex'] : 0);
        $q = "INSERT IGNORE INTO ancet SET `name`= :name,login = :login,birthday = :birthday,age = :age, city=:city,city_name = :city_name,
              text = :text,height = :height,weight = :weight, publish_time = :publish_time,status = :status,create_time = NOW(),
               	vk_ads_id = :vk_ads_id, vk_link = :vk_link,signer_id = :signer_id,vk_group_id = :vk_group_id, sex = :sex  ";
        $res = Yii::$app->db->createCommand($q,[':name'=>$name,':login'=>$login,':birthday'=>$birthday,':age'=>$age,
            ':city'=>$city,':city_name'=>$city_name,':text'=>$text,':height'=>$height,':weight'=>$weight,
            ':publish_time'=>$publish_time,':status'=>$status,':vk_ads_id'=>$vk_ads_id,':vk_link'=>$vk_link,
            ':signer_id'=>$signer_id, ':vk_group_id' =>$group_id ,':sex'=>$sex])->execute();

        $ancet_id = Yii::$app->db->lastInsertID;
        $photos = json_decode($attachments, true);
        foreach ($photos as $photo) {
            if ($photo['type'] == 'photo') {
                $small = ''; $full = ''; $full_y = ''; $full_x = '';
                foreach ($photo['photo']['sizes'] as $k=>$size) {
                    if ($size['type'] == 'q') {
                        $small = $size['url'];
                    } elseif ($size['type'] == 'z') {
                        $full = $size['url'];
                    } elseif ($size['type'] == 'y') {
                        $full_y = $size['url'];;
                    } elseif ($size['type'] == 'x') {
                        $full_x = $size['url'];;
                    }
                }
                if (empty($full)) {
                    if (!empty($full_y)) {
                        $full = $full_y;
                    } elseif (!empty($full_x)) {
                        $full = $full_x;
                    } else {
                        $full = $small;
                    }
                }
                $q = "INSERT INTO ancet_photo SET `ancet_id`= :ancet_id, `preview` = :preview, `full` = :full_img";
                $res = Yii::$app->db->createCommand($q, [':ancet_id' => $ancet_id, ':preview' => $small, ':full_img' => $full])->execute();
                $photo_id = Yii::$app->db->lastInsertID;
                if ($small) {
                    $ext =  pathinfo($small)['extension'];
                    file_put_contents(Yii::getAlias('@webroot') . '/photo/thumbs/photo_'. $ancet_id . '_' . $photo_id. '.' . $ext, file_get_contents($small));
                }
                if ($full) {
                    $ext =  pathinfo($full)['extension'];
                    file_put_contents(Yii::getAlias('@webroot') . '/photo/full/photo_'. $ancet_id . '_' . $photo_id. '.' . $ext, file_get_contents($full));
                    $q = "UPDATE ancet_photo SET `img` = :img WHERE id = :id";
                    $res = Yii::$app->db->createCommand($q, [':id' => $photo_id,':img' => 'photo_'. $ancet_id . '_' . $photo_id. '.' . $ext])->execute();
                }
            }
        }
        return $ancet_id;
    }

    public static function getAge($birthday)
    {
        $now = new \DateTime();
        $b = new \DateTime($birthday);
        $interval = $now->diff( $b );
        $age = $interval->y;
        return $age;
    }

    public static function is_job($text,$is_resume = 0)
    {
        $is_find = false;
        $feature = $is_resume ? self::$feature_resume : self::$feature_job;
        //   var_dump($text); exit;
        foreach ($text as $sentes){
            foreach ($sentes as $nom=>$word){
                $word_l= mb_strtolower($word);

                foreach ($feature as $name=>$sign){
                    if ( mb_strpos($word_l,$name) !== false){
                        if (count($sign['before'])>0){ // before
                            foreach ($sign['before'] as $name_before=>$kol){
                                $i = 0;
                                while (($i < $kol) && ($nom>=$kol)){
                                    $i++;
                                    $before_i = $nom-$i;
                                    if ($before_i < 0){
                                        break;
                                    }
                                    $old_word = $sentes[$before_i];
                                    if ( mb_strpos($old_word,$name_before) !== false){
                                        $is_find = true;
                                        break 5;
                                    }
                                }
                            }
                        }

                        if (count($sign['after'])>0){ // _after
                            $max = count($sentes)-1;
                            foreach ($sign['after'] as $name_after=>$kol){
                                $i = 0;
                                while (($i < $kol) ){
                                    $i++;
                                    $after_i = $nom+$i;
                                    if ($after_i > $max){
                                        break;
                                    }
                                    $old_word = $sentes[$after_i];
                                    if ( mb_strpos($old_word,$name_after) !== false){
                                        $is_find = true;
                                        break 5;
                                    }
                                }
                            }
                        }else{
                            $is_find = true;
                            break 3;
                        }
                    }
                }
            }
        }
        return $is_find;
    }

    public static  function get_title($arr_sentes)
    {
        if (count($arr_sentes)>0){
            $max = count($arr_sentes);
            $i = 0;
            $title = '';
            while( $i < $max ){
                $sente = $arr_sentes[$i];
                if (strlen($sente)<=7){
                    $i++;
                    continue;
                }

                foreach (self::$title_key as $key){
                    if (empty($title)){
                        $arr = explode($key,$sente);
                    }else{
                        $arr = explode($key,$title);
                    }
                    if (count($arr)>1 ){
                        $title = $arr[0];
                        if (strlen($title)>252){
                            $title = substr($title,0,250);
                        }
                    }
                }
                if (empty($title)){
                    $title = $sente;
                    if (strlen($title)>252){
                        $title = substr($title,0,250);
                    }
                }
                break;
            }
            return $title;
        }
        return 0;
    }

    public static function get_category($text)
    {
        $id_find = [];
        foreach ($text as $sentes) {
            foreach ($sentes as $nom => $word) {
                foreach (self::$profession as $name => $id) {
                    if (strpos($word, $name) !== false) {
                        $id_find[$id] = $id;
                        //break 3;
                    }
                }
            }
        }
        if (count($id_find)>0){
            $s = '';
            foreach ($id_find as $id){
                $s .= $id.',';
            }
            $s = substr($s,0,-1);
            $q = 'select category_id from {{profession_category}} where profession_id in ('.$s.') group by category_id';
            $res = Yii::$app->db->createCommand($q)->queryAll();
            if (is_array($res) && count($res)> 0){
                return $res;
            }
        }
        return [];
    }

    public static function getNoCategory()
    {
        $q = 'select * from {{vacancy}} where is_category=0';
        $res = Yii::$app->db->createCommand($q)->queryAll();
        if ( is_array($res) && count($res)>0){
                foreach ($res as $row){
                    echo 'id='.$row['id'].' text='.$row['text'].'<br/>';
                }
        }
        echo '<br/> finish';
    }

    public static function getProfession()
    {
        $q = 'select * from {{profession}}';
        $profession = Yii::$app->db->createCommand($q)->queryAll();
        self::$profession = [];
        foreach ($profession as $row){
            self::$profession[$row['name']] = $row['id'];
        }
    }

    public static function UpdateNoVacancy()
    {
        $q = 'select * from {{vacancy_no}} order by social,group_id,id  ';
        $res = Yii::$app->db->createCommand($q)->queryAll();
//        var_dump($res);

        if (is_array($res) && count($res)>0 ){
            LoadVacancy::getProfession();
            foreach ($res as $row){
                $social = $row['social'];
                $group_id = $row['group_id'];
                $id = $row['id'];
                $q = 'select city_id from {{vk_group}} WHERE social="'.$social.'" and group_id='.$group_id;
                $city_id = Yii::$app->db->createCommand($q)->queryScalar();

                $text = preg_replace("/ +/", " ", $row['text']);
                $text = mb_strtolower($text);
                $sentes = explode('.',$text);
                $arr_sentes = [];
// if ($read_id ==39) var_dump($text);
                foreach ($sentes as $val){
                    $arr_word = explode(' ',$val);
                    $arr_sentes[] =$arr_word;
                }
                $is_job = self::is_job($arr_sentes);
                $table = 'vacancy';
                if ($is_job){
                    $category_list = self::get_category($arr_sentes);
                    if (empty($category_list)){
                        echo 'no category for id='.$id."\n";
                        $is_category = 0;
                    }else{
                        $is_category = 1;
                        foreach ($category_list as $category){
                            $q = 'insert ignore {{vacancy_category}} (category_id,social,group_id,id) values ';
                            $q .= '('.$category['category_id'].',"'.$social.'",'.$group_id.','.$id.')';
                            $res = Yii::$app->db->createCommand($q)->execute();
                        }
                        $q = 'delete from {{vacancy_no}} where social="'.$social.'" 
                              and group_id='.$group_id.' and id='.$id;
                        $res = Yii::$app->db->createCommand($q)->execute();
                    }
                    $q = 'insert ignore {{'.$table.'}} (social,group_id,id,text,city_id,date_create,is_category) values 
                    ("'.$social.'",'.$group_id.','.$id.',:text,'.$city_id.',NOW(),'.$is_category.')';
                    $res = Yii::$app->db->createCommand($q,[':text'=>$row['text']])->execute();
                }
                echo 'id='.$id.' no_category='.$is_job.'<br/>';
            }
        }
        //  var_dump($res);
        exit;
    }

    public static function UpdateNoCategory()
    {
        $q = 'select * from {{vacancy}} where is_category =0 ';
        $res = Yii::$app->db->createCommand($q)->queryAll();
      //  var_dump($res);

        if (is_array($res) && count($res)>0 ){
            LoadVacancy::getProfession();
            foreach ($res as $vacancy){
                $social = $vacancy['social'];
                $group_id = $vacancy['group_id'];
                $text = preg_replace("/ +/", " ", $vacancy['text']);
                $text = mb_strtolower($text);
                $sentes = explode('.',$text);
                $arr_sentes = [];
                $id = $vacancy['id'];
// if ($read_id ==39) var_dump($text);
                foreach ($sentes as $val){
                    $arr_word = explode(' ',$val);
                    $arr_sentes[] =$arr_word;
                }
     //           $is_job = self::is_job($arr_sentes);
//var_dump($is_job); var_dump($arr_sentes); exit;
                $category_list = LoadVacancy::get_category($arr_sentes);
                if (empty($category_list)){
                    echo 'no category for id='.$id."<br/>";
                    $is_category = 0;
                }else{
                    foreach ($category_list as $category){
                        $q = 'insert ignore {{vacancy_category}} (category_id,social,group_id,id) values ';
                        $q .= '('.$category['category_id'].',"'.$social.'",'.$group_id.','.$id.')';
                        $res = Yii::$app->db->createCommand($q)->execute();
                    }
                    $is_category = 1;
                }
                $q = 'update {{vacancy}} set is_category='.$is_category.' where social="'.$social.'" 
                    and group_id='.$group_id.' and id='.$id;
                $res = Yii::$app->db->createCommand($q)->execute();
                echo 'id='.$id.' is_category='.$is_category.'<br/>';
            }
        }
        exit;
    }



}
