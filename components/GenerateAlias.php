<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;

class GenerateAlias  extends Component
{

    public static function generateSlugVacancy( $id, $slug )
    {
        $slug = self::transliteration($slug);
//var_dump($alias); exit;
        if ( self::checkUniqueVacancy( $id, $slug ) ) return $slug;
        else {
            $q = 'delete from vacancy where slug = "'.$slug.'"';
            $res = Yii::$app->db->createCommand($q)->execute();
            return $slug;
/*            for ( $suffix = 1; ! self::checkUniqueVacancy( $id, $new_slug = $slug . '-' . $suffix ); $suffix++ );
            return $new_slug;*/
        }
    }

    public static function generateAliasJob( $id, $alias )
    {
        $alias = self::transliteration($alias);
//var_dump($alias); exit;
        if ( self::checkUniqueJob( $id, $alias ) ) return $alias;
        else {
            for ( $suffix = 1; ! self::checkUniqueJob( $id, $new_alias = $alias . '-' . $suffix ); $suffix++ );
                var_dump($new_alias);
            return $new_alias;
        }
    }

    public static function updateAliasCategory()
    {
        $q = 'select id,ru from {{category_job}}';
        $res = Yii::$app->db->createCommand($q)->queryAll();
        foreach ($res as $row){
            $alias = self::generateAliasJob( $row['id'], $row['ru']);
            $q = 'update {{category_job}} set alias = "'.$alias.'" where id= '.$row['id'];
            $res_up = Yii::$app->db->createCommand($q)->execute();

        }
        return true;
    }


    public static function generateAlias( $id, $alias, $is_region )
    {
        $alias = self::transliteration($alias);
//var_dump($alias); exit;
        if ($is_region){
            if (self::checkUniqueRegion($id, $alias))  return $alias;
            else{
                for ( $suffix = 1; ! self::checkUniqueRegion( $id, $new_alias = $alias . '-' . $suffix ); $suffix++ ) {}
                return $new_alias;
            }
        }else{
            if ( self::checkUniqueCity( $id, $alias ) ) return $alias;
            else {
                $new_alias = $alias . '-' . $id;
                var_dump($new_alias);
                if (self::checkUniqueRegion($id, $new_alias))  return $new_alias;
                else{
                    echo 'yet such -'.$new_alias; exit;
                }
                /*                for ( $suffix = 1; ! self::checkUniqueCity( $id, $new_alias = $alias . '-' . $suffix ); $suffix++ )
                                { var_dump($new_alias);}
                                return $new_alias;*/
            }
        }
    }


    public static function checkUniqueVacancy( $id,$slug )
    {
        if (empty($id)){
            $q = 'select count(*) from vacancy where slug = "'.$slug.'"';
        }else{
            $q = 'select count(*) from vacancy where id <> '.$id.' and slug = "'.$slug.'"';
        }
        $kol = Yii::$app->db->createCommand($q)->queryScalar();
        return (! empty($kol)) ? false : true;
    }

    public static function checkUniqueJob( $id, $alias )
    {
        if (empty($id)){
            $q = 'select count(*) from category_job where alias = "'.$alias.'"';
        }else{
            $q = 'select count(*) from category_job where id <> '.$id.' and alias = "'.$alias.'"';
        }
        $kol = Yii::$app->db->createCommand($q)->queryScalar();
        return (! empty($kol)) ? false : true;
    }

    public static function checkUniqueCity( $id, $alias )
    {
        $q = 'select count(*) from geodata._cities where city_id <> '.$id.' and alias = "'.$alias.'"';
        $kol = Yii::$app->db->createCommand($q)->queryScalar();
        return ($kol > 0) ? false : true;
    }

    public static function checkUniqueRegion( $id, $alias )
    {
        $q = 'select count(*) from geodata._regions where region_id <> '.$id.' and alias = "'.$alias.'"';
        $kol = Yii::$app->db->createCommand($q)->queryScalar();
        return ($kol > 0) ? false : true;
    }

    public static function updateAlias( $id, $alias, $is_region )
    {
        $alias = self::generateAlias( $id, $alias, $is_region);
        if ($is_region){
            $q = 'update geodata._regions set alias = "'.$alias.'" where region_id = '.$id;
        }else{
            $q = 'update geodata._cities  set alias = "'.$alias.'"  where city_id = '.$id;
        }
//     var_dump($q); exit;
        $res = Yii::$app->db->createCommand($q)->execute();
        return $res;
    }

    public static function getRegions()
    {
        $q = 'select region_id, title_ru from geodata._regions ' ;
        $res = Yii::$app->db->createCommand($q)->queryAll();
        return $res;
    }

    public static function getCities()
    {
        $q = 'select city_id, title_ru from geodata._cities order by city_id ASC  ' ;
        $res = Yii::$app->db->createCommand($q)->queryAll();
        return $res;
    }

    public static function transliteration($str)
    {
        // ГОСТ 7.79B
        $transliteration = array(
            'А' => 'A', 'а' => 'a',
            'Б' => 'B', 'б' => 'b',
            'В' => 'V', 'в' => 'v',
            'Г' => 'G', 'г' => 'g',
            'Д' => 'D', 'д' => 'd',
            'Е' => 'E', 'е' => 'e',
            'Ё' => 'Yo', 'ё' => 'yo',
            'Ж' => 'Zh', 'ж' => 'zh',
            'З' => 'Z', 'з' => 'z',
            'И' => 'I', 'и' => 'i',
            'Й' => 'J', 'й' => 'j',
            'К' => 'K', 'к' => 'k',
            'Л' => 'L', 'л' => 'l',
            'М' => 'M', 'м' => 'm',
            'Н' => "N", 'н' => 'n',
            'О' => 'O', 'о' => 'o',
            'П' => 'P', 'п' => 'p',
            'Р' => 'R', 'р' => 'r',
            'С' => 'S', 'с' => 's',
            'Т' => 'T', 'т' => 't',
            'У' => 'U', 'у' => 'u',
            'Ф' => 'F', 'ф' => 'f',
            'Х' => 'H', 'х' => 'h',
            'Ц' => 'Cz', 'ц' => 'cz',
            'Ч' => 'Ch', 'ч' => 'ch',
            'Ш' => 'Sh', 'ш' => 'sh',
            'Щ' => 'Shh', 'щ' => 'shh',
            'Ъ' => 'ʺ', 'ъ' => 'ʺ',
            'Ы' => 'Y`', 'ы' => 'y`',
            'Ь' => '', 'ь' => '',
            'Э' => 'E`', 'э' => 'e`',
            'Ю' => 'Yu', 'ю' => 'yu',
            'Я' => 'Ya', 'я' => 'ya',
            '№' => '#', 'Ӏ' => '‡',
            '’' => '`', 'ˮ' => '¨',
        );

        $str = strtr($str, $transliteration);
        $str = mb_strtolower($str, 'UTF-8');
        $str = preg_replace('/[^0-9a-z\-\s]/', '', $str);
        $str = preg_replace('|([-]+)|s', '-', $str);
        $str = preg_replace('|([\s]+)|', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

}  //////////////////////////////////////////
