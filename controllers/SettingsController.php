<?php

namespace app\controllers;

use Yii;
use app\models\Profile;
use app\models\SettingsForm;
use dektrium\user\controllers\SettingsController as BaseSettingsController;
use yii\data\ActiveDataProvider;

class SettingsController extends BaseSettingsController
{
    public $layout = '//cabinet';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'][] = [
            'allow'   => true,
            'actions' => ['uploadPhoto'],
            'roles'   => ['@'],
        ];
        $behaviors['access']['rules'][] = [
            'allow'   => true,
            'actions' => ['referrals'],
            'roles'   => ['@'],
        ];
        return $behaviors;
    }

    public function actions()
    {
        return [
            'uploadPhoto' => [
                'class' => 'budyaga\cropper\actions\UploadAction',
                'url' => '/uploads/profile/photo',
                'path' => '@app/web/uploads/profile/photo',
            ]
        ];
    }

    public function actionProfile()
    {
        return parent::actionProfile(); // TODO: Change the autogenerated stub
    }
    /**
     * Displays page where user can update account settings (username, email or password).
     *
     * @return string|\yii\web\Response
     */
    public function actionAccount()
    {
        /** @var SettingsForm $model */
        $model = \Yii::createObject(SettingsForm::className());
        //$model->tel_username = Yii::$app->user->identity->tel_username;
        $model->tel_id = Yii::$app->user->identity->tel_id;
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_ACCOUNT_UPDATE, $event);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', \Yii::t('user', 'Your account details have been updated'));
            $this->trigger(self::EVENT_AFTER_ACCOUNT_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('account', [
            'model' => $model,
        ]);
    }


    public function actionReferrals () {
        $query = Profile::find()->where(['referrer_id' => \Yii::$app->user->id] );
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'defaultOrder' => [
                    'user_id' => SORT_DESC,
                ]
            ],
        ]);
        return $this->render('referrals', [
            'dataProvider' => $dataProvider
        ]);
    }
}