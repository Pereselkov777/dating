<?php

namespace app\controllers;

use app\models\Profile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\ClientOrder;



use Yii;



class DataController extends Controller


/**
 * @inheritdoc
 * 
 * 
 */
{
    public function behaviors()
    {
        return [
           /* 'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'data'],
                'rules' => [
                    [
                        'actions' => ['index', 'data'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'data') {    
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    
   
    public function actionIndex()
    {  $id =  (Yii::$app->user->id );
        $model = Profile::findOne($id);
        //var_dump($model);
        //$this->render('/user/profile/show',[ 'id' => '2'] );

        return $this->redirect(['/user/profile/show',
            'id' => $id,
        ]);
    }


    public function actionData()

    {   
            $man_id =  (Yii::$app->user->id );
            $profile = Profile::findOne($man_id);
            $data = Yii::$app->request->post('txt');
        
           if(isset($data)) {
            $txt = "Ajax Worked!";
            
            $girl_id = (int)$data;
            
            if ($profile === null) {
                echo('profile === null');
                return [
                    'result' => 'fail',
                    'message' => 'Profile not found!'
                ];
            }
    
            if ($profile->accessPersonalData($girl_id)) {
                //echo('метод модели');
                //echo($data);
            }else {
                echo('Списание');
                //var_dump($profile);
                $cost = 5.00;
                if(!$profile->credit){
                    echo('1');
                    return [
                        'result' => 'fail',
                        'message' => 'Not enough money!'
                    ];
                    
                } else{
                    echo('2');
                    //echo($man_id);
                    //echo($girl_id);
                    if ($profile->credit - $cost < 0) {
                        die('3');
                        $txt = "Not enough money!";
                        //return \yii\helpers\Json::encode($txt);     
                    }else{
                        echo('4');
                        $profile->credit = round($profile->credit - $cost, 2);
                        $profile->save(false);
                        //echo($profile->credit);
                        $comission = 0.3;
                        $amount_agency = $cost * $comission;
                        echo($amount_agency);
                        $girl = Profile::findOne(['user_id' => $girl_id]);
                        var_dump($girl->agency_id);
                
        
                        $q = 'INSERT IGNORE INTO personal_access_data SET girl_id = ' . $girl_id .
                        ', man_id=' . $man_id . ', access_data = NOW()';
                        Yii::$app->db->createCommand($q)->execute(); //запись в personal_access_data
    
                        $q = 'INSERT INTO client_order SET man_id=' . $man_id . ', girl_id = ' . $girl_id .
                        ', agency_id = ' . $girl->agency_id . ', service_id = ' . ClientOrder::SERVICE_PERSONAL_DATA .
                        ', product_id = '.$girl_id.', amount = ' . $cost . ', amount_agency = ' . $amount_agency . ', create_time = NOW()';
                        Yii::$app->db->createCommand($q)->execute(); // запись в client_order
                    }
                }
                //$profile->credit = round($profile->credit - $cost, 2);

                //$profile->save(false);    
        }
        } else {
            $txt = "Ajax failed";
        }
        return \yii\helpers\Json::encode($txt);
    }
    
}
    
       
        
    
