<?php

namespace app\controllers;

use app\models\Profile;
use yii\web\Controller;
use yii\filters\VerbFilter;
use Yii;

class UploadAvatarController extends Controller

/**
 * @inheritdoc
 * 
 * 
 */
{
    public function behaviors()
    {
        return [
           /* 'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'data'],
                'rules' => [
                    [
                        'actions' => ['index', 'data'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'save' || $action->id === 'change' ) {    
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 

 
    public function actionSave()

    {   
        if (Yii::$app->request->isAjax) {
            //ho('AJAX');
            $data = Yii::$app->request->post();
            echo('<pre>');
            var_dump($data);
            echo('</pre>');
            //var_dump($data);
            //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if(isset($data)) {
               // echo('SUCCESS');
                //echo($data);
                $user_id =  $data['user_id'];
                $photo = $data['link'];
                $avatar_id = $data['id'];
                //Yii::$app->db->createCommand('INSERT profile SET photo = ' . $photo . '  WHERE user_id=' . $user_id . '')->execute();
                Yii::$app->db->createCommand()->update('profile', ['photo' => $photo], 'user_id=' . (int)$user_id . '')->execute();
                Yii::$app->db->createCommand()->update('profile', ['avatar_id' => (int)$avatar_id], 'user_id=' . (int)$user_id . '')->execute();
                //Yii::$app->db->createCommand()->batchInsert('profile', ['avatar_id', 'user_id'], [
                /*    [$data['id'], $user_id],
                ])->execute();
                //Yii::$app->db->createCommand('INSERT INTO profile SET avatar_id= :id WHERE user_id =:user_id')->bindValue(':id', $avatar_id)->bindValue(':user_id', Yii::$app->user->id)->execute();*/
                $txt = "Ajax Worked!";
               
               
            }
        }else{
            echo('FAIL');
            $txt = "FAIL";
            

        }
        return \yii\helpers\Json::encode($txt);

    }  
    public function actionChange()

    {   
        if (Yii::$app->request->isAjax) {
            //ho('AJAX');
            $data = Yii::$app->request->post();
            echo('<pre>');
            var_dump($data);
            echo('</pre>');
            //var_dump($data);
            //Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if(isset($data)) {
               // echo('SUCCESS');
                //echo($data);
                $user_id =  $data['user_id'];
                $photo = $data['link'];
                $avatar_id = $data['id'];
                //Yii::$app->db->createCommand('INSERT profile SET photo = ' . $photo . '  WHERE user_id=' . $user_id . '')->execute();
                Yii::$app->db->createCommand()->update('photo', ['photo' => $photo], 'user_id=' . (int)$user_id . '')->execute();
                Yii::$app->db->createCommand()->update('profile', ['avatar_id' => (int)$avatar_id], 'user_id=' . (int)$user_id . '')->execute();
                //Yii::$app->db->createCommand()->batchInsert('profile', ['avatar_id', 'user_id'], [
                /*    [$data['id'], $user_id],
                ])->execute();
                //Yii::$app->db->createCommand('INSERT INTO profile SET avatar_id= :id WHERE user_id =:user_id')->bindValue(':id', $avatar_id)->bindValue(':user_id', Yii::$app->user->id)->execute();*/
                $txt = "Ajax Worked!";
               
               
            }
        }else{
            echo('FAIL');
            $txt = "FAIL";
            

        }
        return \yii\helpers\Json::encode($txt);

    }  
           
    
}
    
       
        
    

