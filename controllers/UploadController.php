<?php

namespace app\controllers;

use app\models\Photo;
use app\models\PrivatePhoto;
use yii\base\Security;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use Yii;



class UploadController extends Controller


/**
 * @inheritdoc
 * 
 * 
 */
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        if ($action->id === 'sample') {    
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 

   
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            //$data = Yii::$app->request->post();
            if(isset($_FILES['file'], $_POST['upload_user_id'], $_POST['upload_photo_id'])) {
                $is_private_photo = isset($_POST['is_private_photo']) ? $_POST['is_private_photo'] : 0;
                $photo_id = $_POST['upload_photo_id'];
                //var_dump($_POST);
                //exit();
                if ($photo_id) {
                    if ($is_private_photo) {
                        $model = PrivatePhoto::findOne(['id' => $photo_id]);
                    } else {
                        $model = Photo::findOne(['id' => $photo_id]);
                    }
                } else {
                    if ($is_private_photo) {
                        $model = new PrivatePhoto();
                        $model->img_blur = '0.jpg';
                    } else {
                        $model = new Photo();
                    }
                    $model->user_id = $_POST['upload_user_id'];
                    $model->status = 1;
                    $model->img_full = $model->img_prev = '0.jpg';
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $model->img_full)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $model->img_full );
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev );
                }
                if ($is_private_photo) {
                    if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur)) {
                        unlink(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur );
                    }
                }

                $path_parts = pathinfo($_FILES['file']['name']);
                $ext = $path_parts['extension'];
                //var_dump($_POST['upload_user_id']);
                //exit();
                $name = $_POST['upload_user_id'] . '_' . time() . '.' . $ext;
                $uploadDirFull = '/uploads/photos/full/';
                move_uploaded_file($_FILES['file']['tmp_name'], Yii::getAlias('@webroot') . $uploadDirFull . $name);
                if ($ext == 'png') {
                    $image = imagecreatefrompng(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);
                } else {
                    $image = imagecreatefromjpeg(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);
                }

                //  $this->blur(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);

                $filename = Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $name;
//        var_dump($filename); exit;

                $thumb_width = 300;
                $thumb_height = 400;
//        $thumb_height = 225;

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ($original_aspect >= $thumb_aspect) {
                    // If image is wider than thumbnail (in aspect ratio sense)
                    $new_height = $thumb_height;
                    $new_width = $width / ($height / $thumb_height);
                } else {
                    // If the thumbnail is wider than the image
                    $new_width = $thumb_width;
                    $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

// Resize and crop
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                if ($ext == 'png') {
                    imagepng($thumb, $filename);
                } else {
                    imagejpeg($thumb, $filename, 80);
                }

                if ($is_private_photo) {
                    $model->img_blur = $model->blur($filename);
                }

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->img_full = $model->img_prev = $name;
                if (!$model->save()) {
                    var_dump($model->getErrors());
                };
                return [
                   'result' => 'success',
                    'id' => $model->id,
                    'file_full' => $uploadDirFull . $name,
                    'file_prev' => '/uploads/photos/thumbs/' . $name,
                ];
               /* $fileName = $_FILES['file']['name'];
             
                echo 'Файл: ' . $fileName . '<br>';
             
                //Загрузка файла на сервер
                $uploadDir = Yii::getAlias('@webroot') . '/uploads/new/'; //Директория на сервере, для загружаемых файлов
             
                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir . $fileName)) {
                        echo 'Загрузка прошла успешно <br><pre>';
                        
                        print_r($_FILES); 
                        print_r($_POST); 
                        echo '</pre>';
                        die; 
                } else {
                    echo 'Загрузка файла не удалась!<br>';
                    var_dump($_FILES);
                }*/
            }
        }
    }

    public function actionUrl()
    {
        if (Yii::$app->request->isAjax) {
            //$data = Yii::$app->request->post();
            if(isset($_POST['url'], $_POST['upload_user_id'], $_POST['upload_photo_id'])) {
                $is_private_photo = isset($_POST['is_private_photo']) ? $_POST['is_private_photo'] : 0;
                $photo_id = $_POST['upload_photo_id'];
                if ($photo_id) {
                    if ($is_private_photo) {
                        $model = PrivatePhoto::findOne(['id' => $photo_id]);
                    } else {
                        $model = Photo::findOne(['id' => $photo_id]);
                    }
                } else {
                    if ($is_private_photo) {
                        $model = new PrivatePhoto();
                        $model->img_blur = '0.jpg';
                    } else {
                        $model = new Photo();
                    }
                    $model->user_id = $_POST['upload_user_id'];
                    $model->status = 1;
                    $model->img_full = $model->img_prev = '0.jpg';
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $model->img_full)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $model->img_full );
                }
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev );
                }
                if ($is_private_photo) {
                    if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur)) {
                        unlink(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur );
                    }
                }

                $tmp_file = Yii::getAlias('@webroot') . '/uploads/tmp' . Yii::$app->security->generateRandomString(20);
                $this->downloadImage($_POST['url'], $tmp_file);
                //move_uploaded_file($_FILES['file']['tmp_name'], Yii::getAlias('@webroot') . $uploadDirFull . $name);
                $mime_type =  image_type_to_mime_type(exif_imagetype($tmp_file));
                //$mime_type = mime_content_type($_POST['url']);
                $content = file_get_contents($tmp_file);

                if ($mime_type == 'image/png') {
                    $ext = 'png';
                    $name = $_POST['upload_user_id'] . '_' . time() . '.' . $ext;
                    $uploadDirFull = '/uploads/photos/full/';
                    file_put_contents(Yii::getAlias('@webroot') . $uploadDirFull . $name, $content);
                    $image = imagecreatefrompng(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);
                } elseif ($mime_type == 'image/jpeg') {
                    $ext = 'jpg';
                    $name = $_POST['upload_user_id'] . '_' . time() . '.' . $ext;
                    $uploadDirFull = '/uploads/photos/full/';
                    file_put_contents(Yii::getAlias('@webroot') . $uploadDirFull . $name, $content);
                    $image = imagecreatefromjpeg(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);
                } else {
                    var_dump($mime_type);
                    echo 'fatal error: no support type'; exit;
                }

                //  $this->blur(Yii::getAlias('@webroot') . '/uploads/photos/full/' . $name);

                $filename = Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $name;
//        var_dump($filename); exit;

                $thumb_width = 300;
                $thumb_height = 400;
//        $thumb_height = 225;

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ($original_aspect >= $thumb_aspect) {
                    // If image is wider than thumbnail (in aspect ratio sense)
                    $new_height = $thumb_height;
                    $new_width = $width / ($height / $thumb_height);
                } else {
                    // If the thumbnail is wider than the image
                    $new_width = $thumb_width;
                    $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

// Resize and crop
                imagecopyresampled($thumb,
                    $image,
                    0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                    0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                if ($ext == 'png') {
                    imagepng($thumb, $filename);
                } else {
                    imagejpeg($thumb, $filename, 80);
                }

                if ($is_private_photo) {
                    $model->img_blur = $model->blur($filename);
                }

                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $model->img_full = $model->img_prev = $name;
                if (!$model->save()) {
                    var_dump($model->getErrors());
                };
                unlink($tmp_file);
                return [
                    'result' => 'success',
                    'id' => $model->id,
                    'file_full' => $uploadDirFull . $name,
                    'file_prev' => '/uploads/photos/thumbs/' . $name,
                ];
                /* $fileName = $_FILES['file']['name'];

                 echo 'Файл: ' . $fileName . '<br>';

                 //Загрузка файла на сервер
                 $uploadDir = Yii::getAlias('@webroot') . '/uploads/new/'; //Директория на сервере, для загружаемых файлов

                 if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir . $fileName)) {
                         echo 'Загрузка прошла успешно <br><pre>';

                         print_r($_FILES);
                         print_r($_POST);
                         echo '</pre>';
                         die;
                 } else {
                     echo 'Загрузка файла не удалась!<br>';
                     var_dump($_FILES);
                 }*/
            }
        }
    }

    public function actionCrop()
    {
        if (Yii::$app->request->isAjax && isset($_POST['upload_photo_id'])) {
            $is_private_photo = isset($_POST['is_private_photo']) ? $_POST['is_private_photo'] : 0;
            $photo_id = $_POST['upload_photo_id'];
            if ($photo_id) {
                if ($is_private_photo) {
                    $model = PrivatePhoto::findOne(['id' => $photo_id]);
                } else {
                    $model = Photo::findOne(['id' => $photo_id]);
                }
            } else {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result' => 'fail',
                    'message' => 'Photo not found',
                ];
            }
            if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev)) {
                unlink(Yii::getAlias('@webroot') . '/uploads/photos/thumbs/' . $model->img_prev );
            }
            if ($is_private_photo) {
                if (file_exists(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur)) {
                    unlink(Yii::getAlias('@webroot') . '/uploads/photos/blurs/' . $model->img_blur );
                }
            }
            if (mime_content_type($_FILES['file']['tmp_name']) == 'image/png') {
                $ext = 'png';
            } else {
              $ext = 'jpg';
            }
            //$path_parts = pathinfo($_FILES['file']['name']);
            //$ext = $path_parts['extension'];
            $name = $_POST['upload_user_id'] . '_' . time() . '.' . $ext;
            $model->img_prev = $name;
            $uploadDirFull = '/uploads/photos/thumbs/';
            $file = Yii::getAlias('@webroot') . $uploadDirFull . $model->img_prev;
            move_uploaded_file($_FILES['file']['tmp_name'], $file);

            //обрезаем до 300х400
            if (mime_content_type($file) == 'image/png') {
                $image = imagecreatefrompng($file);
            } else {
                $image = imagecreatefromjpeg($file);
            }
            $thumb_width = 300;
            $thumb_height = 400;
//        $thumb_height = 225;

            $width = imagesx($image);
            $height = imagesy($image);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            if ($original_aspect >= $thumb_aspect) {
                // If image is wider than thumbnail (in aspect ratio sense)
                $new_height = $thumb_height;
                $new_width = $width / ($height / $thumb_height);
            } else {
                // If the thumbnail is wider than the image
                $new_width = $thumb_width;
                $new_height = $height / ($width / $thumb_width);
            }

            $thumb = imagecreatetruecolor($thumb_width, $thumb_height);

// Resize and crop
            imagecopyresampled($thumb,
                $image,
                0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                0, 0,
                $new_width, $new_height,
                $width, $height);
            if (mime_content_type($file) == 'image/png') {
                imagepng($thumb, $file);
            } else {
                imagejpeg($thumb, $file, 80);
            }
            //конец обрезки

            if ($is_private_photo) {
                $model->img_blur = $model->blur($file);
            }

            if (!$model->save()) {
                var_dump($model->getErrors()); exit;
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => 'success',
                'id' => $model->id,
                'file' => $uploadDirFull . $model->img_prev,
            ];
        }
    }

    
    public function actionDelete($id)
    {
        if (Yii::$app->request->isAjax && isset($_POST['upload_user_id'])) {
            if ($id) {
                $model = Photo::findOne(['id' => $id]);
            } else {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return [
                    'result' => 'fail',
                    'message' => 'Photo not found',
                ];
            }
            if ($model->user_id == $_POST['upload_user_id']) {
                $model->delete();
            }
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => 'success',
            ];
        }
    }

    private function downloadImage($image_url, $filename)
    {
        // Proceed to download
        // Open file to save
        $file = fopen($filename, 'w');
        // Use curl
        $ch = curl_init($image_url);
        // Set options
        curl_setopt($ch, CURLOPT_FILE, $file);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        // Execute
        $data = curl_exec($ch);
        // Close curl
        curl_close($ch);
        // Close file
        fclose($file);
    }

}