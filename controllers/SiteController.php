<?php

namespace app\controllers;

use app\models\PostSearch;
use app\models\ProfileSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Url;

class SiteController extends Controller
{
    public $validation_key = 'asdas5fdwer38iyeuit4ueh';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            $searchModel = null;
            $dataProvider = null;
        } else {
            //var_dump($_GET); exit;
           
            $searchModel = new ProfileSearch();
            $searchModel->status = 1;
            //$searchModel->themes = 'tests';
            $searchModel->min_age = 18;
            $searchModel->max_age = 45;
            $searchModel->sex = Yii::$app->user->identity->profile->sex == 1 ? 2 : 1;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            //print_r(Yii::$app->request->queryParams); exit;
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $ghost_email = Yii::$app->request->post('email');
        if (!empty($ghost_email)) {
            Yii::$app->session->setFlash('danger', 'Доступ запрещен. Вы не прошли защиту от ботов');
            return $this->refresh();
        }
        $validation_key = Yii::$app->request->post('validation_key');
        if (Yii::$app->request->isPost && ($validation_key != $this->validation_key)) {
            Yii::$app->session->setFlash('danger', 'Доступ запрещен. Вы не прошли защиту от ботов. Проверьте, включен ли в вашем браузере javascript');
            return $this->refresh();
        }

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['contactEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAgreement()
    {
        return $this->render('agreement');
    }


}
