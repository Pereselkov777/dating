<?php

namespace app\controllers;

use app\models\PrivatePhoto;
use app\models\Profile;
use Yii;
use app\models\ClientOrder;
use app\models\ClientOrderSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClientOrderController implements the CRUD actions for ClientOrder model.
 */
class ClientOrderController extends Controller
{
    public $layout = '//admin';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'create', 'view', 'delete',
                    'buy-photo', 'admin'],
                'rules' => [
                    [
                        'actions' => ['index', 'buy-photo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin', 'update', 'create', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['administrator'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClientOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '//cabinet';
        $searchModel = new ClientOrderSearch();
        $searchModel->man_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all ClientOrder models.
     * @return mixed
     */
    public function actionAdmin()
    {
        $searchModel = new ClientOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('admin', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClientOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ClientOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClientOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClientOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClientOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionBuyPhoto(int $private_photo_id) {
        $this->layout = '//cabinet';
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $photo = PrivatePhoto::findOne(['id' => $private_photo_id]);
        if ($photo === null) {
            return [
                'result' => 'fail',
                'message' => 'Photo not found!'
            ];
        }
        $girl = Profile::findOne(['user_id' => $photo->user_id]);
        if ($girl === null) {
            return [
                'result' => 'fail',
                'message' => 'Girl not found!'
            ];
        }
        $profile = Profile::findOne(['user_id' => Yii::$app->user->id]);
        if ($profile === null) {
            return [
                'result' => 'fail',
                'message' => 'Profile not found!'
            ];
        }
        $cost = 0.05;
        if ($profile->credit - $cost < 0) {
            return [
                'result' => 'fail',
                'message' => 'Not enough money!'
            ];
        }
        $profile->credit = round($profile->credit - $cost, 2);
        $profile->save(false);
        $comission = 0.3;
        $amount_agency = $cost * $comission;

        $q = 'INSERT IGNORE INTO private_photo_access SET photo_id = ' . $private_photo_id .
            ', man_id=' . Yii::$app->user->id . ', girl_id = ' . $photo->user_id . ', access_data = NOW()';
        Yii::$app->db->createCommand($q)->execute();
        $q = 'INSERT INTO client_order SET man_id=' . Yii::$app->user->id . ', girl_id = ' . $photo->user_id .
            ', agency_id = ' . $girl->agency_id . ', service_id = ' . ClientOrder::SERVICE_PRIVATE_PHOTO .
            ', product_id = '.$photo->id.', amount = ' . $cost . ', amount_agency = ' . $amount_agency . ', create_time = NOW()';
        Yii::$app->db->createCommand($q)->execute();
        return [
            'result' => 'success',
            'message' => 'Access is allow',
            'full' => $photo->getFull(),
            'prev' => $photo->getPrew(),
        ];
    }

    /**
     * Finds the ClientOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
