<?php

namespace app\controllers;

use app\models\Profile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;
use yii\helpers\Url;




use Yii;



class NotificationController extends Controller


/**
 * @inheritdoc
 * 
 * 
 */
{
    public function behaviors()
    {
        return [
           /* 'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'data'],
                'rules' => [
                    [
                        'actions' => ['index', 'data'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'push') {    
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    } 


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    
   
    

    public function actionPush()

    {       
        $notifications = [
            [
                'subscription' => Subscription::create([
                    'endpoint' => 'https://updates.push.services.mozilla.com/push/abc...', // Firefox 43+,
                    'publicKey' => 'BPcMbnWQL5GOYX/5LKZXT6sLmHiMsJSiEvIFvfcDvX7IZ9qqtq68onpTPEYmyxSQNiH7UD/98AUcQ12kBoxz/0s=', // base 64 encoded, should be 88 chars
                    'authToken' => 'CxVX6QsVToEGEcjfYPqXQw==', // base 64 encoded, should be 24 chars
                ]),
                'payload' => 'hello !',
            ], [
                'subscription' => Subscription::create([
                    'endpoint' => 'https://fcm.googleapis.com/fcm/send/abcdef...', // Chrome
                ]),
                'payload' => null,
            ], [
                'subscription' => Subscription::create([
                    'endpoint' => 'https://example.com/other/endpoint/of/another/vendor/abcdef...',
                    'publicKey' => '(stringOf88Chars)',
                    'authToken' => '(stringOf24Chars)',
                    'contentEncoding' => 'aesgcm', // one of PushManager.supportedContentEncodings
                ]),
                'payload' => '{msg:"test"}',
            ], [
                  'subscription' => Subscription::create([ // this is the structure for the working draft from october 2018 (https://www.w3.org/TR/2018/WD-push-api-20181026/) 
                      "endpoint" => "https://example.com/other/endpoint/of/another/vendor/abcdef...",
                      "keys" => [
                          'p256dh' => '(stringOf88Chars)',
                          'auth' => '(stringOf24Chars)'
                      ],
                  ]),
                  'payload' => '{"msg":"Hello World!"}',
              ],
        ];

        $man_id =  (Yii::$app->user->id );
        $profile = Profile::findOne($man_id);
        $data = Yii::$app->request->post('txt');
        

        // here I'll get the subscription endpoint in the POST parameters
        // but in reality, you'll get this information in your database
        // because you already stored it (cf. push_subscription.php)
        
        $auth = array(
            'VAPID' => array(
                'subject' => 'https://github.com/Minishlink/web-push-php-example/',
                'publicKey' => file_get_contents(Yii::$app->basePath . '/push/web-push-php-example/keys/public_key.txt'), // don't forget that your public key also lives in app.js
                'privateKey' => file_get_contents(Yii::$app->basePath . '/push/web-push-php-example/keys/private_key.txt'), // in the real world, this would be in a secret file
            ),
        );

        //$webPush = new WebPush($auth);
       
        /*$report = $webPush->sendOneNotification(
            
            " ",
            "Hello! 👋"
        );*/

        $webPush = new WebPush();

// send multiple notifications with payload
        foreach ($notifications as $notification) {
            $webPush->queueNotification(
                $notification['subscription'],
                $notification['payload'] // optional (defaults null)
            );
        }

        foreach ($webPush->flush() as $report) {
            $endpoint = $report->getRequest()->getUri()->__toString();
        
            if ($report->isSuccess()) {
                echo "[v] Message sent successfully for subscription {$endpoint}.";
            } else {
                echo "[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}";
            }
        }
        
    
        $data = Yii::$app->request->post('txt');
        if(Yii::$app->request->isAjax){
            
        }
        return [
            'result' => 'success',
             'data' => $data
         ];
       
    }
    
}
    
       
        
    
