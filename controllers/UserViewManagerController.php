<?php

namespace app\controllers;
use app\modules\manager\models\Manager;
use app\modules\manager\models\ManagerSearch;
use Yii;

class UserViewManagerController extends \yii\web\Controller
{

    public $layot = '//cabinet';
    
    public function actionIndex()
    {
        
        $searchModel = new ManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {   
        $model = Manager::findOne($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }


}
