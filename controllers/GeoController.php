<?php

namespace app\controllers;

use app\components\GeoHelper;
use MenaraSolutions\Geographer\Country;
use MenaraSolutions\Geographer\Earth;
use MenaraSolutions\Geographer\State;
use yii\helpers\ArrayHelper;

class GeoController extends \yii\web\Controller
{
    public function actionGetCities($region_id)
    {
        return json_encode(GeoHelper::getCities($region_id));
    }

    public function actionGetRegions($country_id)
    {
        return json_encode(GeoHelper::getRegions($country_id), JSON_FORCE_OBJECT );
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

}
