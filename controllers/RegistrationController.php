<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace app\controllers;

use dektrium\user\controllers\RegistrationController as BaseRegistrationController;


/**
 * RegistrationController is responsible for all registration process, which includes registration of a new account,
 * resending confirmation tokens, email confirmation and registration via social networks.
 *
 * @property \dektrium\user\Module $module
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class RegistrationController extends BaseRegistrationController
{
    public $layout = '//main';

    public $validation_key = 'asdas5fdwer38iyeuit4ueh';

    public function actionRegister()
    {
        if (!\Yii::$app->request->isAjax) {
            $ghost_email = \Yii::$app->request->post('email');
            if (!empty($ghost_email)) {
                \Yii::$app->session->setFlash('danger', 'Доступ запрещен. Вы не прошли защиту от ботов');
                return $this->refresh();
            }
            $validation_key = \Yii::$app->request->post('validation_key');
            if (\Yii::$app->request->isPost && ($validation_key != $this->validation_key)) {
                \Yii::$app->session->setFlash('danger', 'Доступ запрещен. Вы не прошли защиту от ботов. Проверьте, включен ли в вашем браузере javascript');
                return $this->refresh();
            }
        }
        return parent::actionRegister();
    }
}
