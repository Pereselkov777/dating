<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => getenv('APP_ID'),
    'name' => 'Asia Bride',
    'basePath' => dirname(__DIR__),
    'language' => 'en',
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'on beforeRequest' => function ($event) {
        $referrer = Yii::$app->request->get('referrer', null);
        if ($referrer) {
            Yii::$app->response->cookies->add(new \yii\web\Cookie([
                'name' => 'referrer',
                'value' => $referrer
            ]));
        }

    },
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'RegistrationForm' => 'app\models\RegistrationForm',
                'User' => 'app\models\User',
                'Profile' => 'app\models\Profile',
                'SettingsForm' => 'app\models\SettingsForm',
            ],
            'controllerMap' => [
                'settings' => 'app\controllers\SettingsController',
                'registration' => 'app\controllers\RegistrationController',
                'admin' => [
                   'class' => 'dektrium\user\controllers\AdminController',
                  'layout' => '//admin',
                ],
                //    'security' => 'app\controllers\SecurityController',
            ],
            'adminPermission' => 'administrator',
         //'admins' => ['admin']
        ],
        'social' => [
            // the module class
            'class' => 'kartik\social\Module',
     
            // the global settings for the Facebook plugins widget
            'facebook' => [
                'appId' => '282900393220413',
                'secret' => 'EAAEBSZBFx2T0BAIANQoMaZBX2ItIDzW2fAAUZBlUEtzvaDwAakKclE6MY4jSTZCBxamPoANOxuB4H2ZCp5hGRZA1IQ2Qt8UcGGjZA07lr04RZA93lIEpS6XZA9GnybB5yjMspfAUa8ZBlt1Vb3FiVRTlHK9yFZANqJDfk2LGZApIafZCeqAZDZD',
            ],
     
           
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
        'cabinet' => [
            'class' => 'app\modules\cabinet\Module',
        ],
        'pages' => [
            'class' => 'bupy7\pages\Module',
            'tableName' => '{{%page}}',
            'controllerMap' => [
                'manager' => [
                    'class' => 'bupy7\pages\controllers\ManagerController',
                    'as access' => [
                        'class' => \yii\filters\AccessControl::className(),
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['administrator'],
                            ],
                        ],
                    ],
                    'layout' => '//admin',
                ],
            ],
            'imperaviLanguage' => 'ru',
            'pathToImages' => '@webroot/uploads/images',
            'urlToImages' => '@web/uploads/images',
            'pathToFiles' => '@webroot/uploads/files',
            'urlToFiles' => '@web/uploads/files',
            'uploadImage' => true,
            'uploadFile' => true,
            'addImage' => true,
            'addFile' => true,
        ],
        'blog' => [
            'class' => 'app\modules\blog\Module',
        ],
        'comment' => [
            'class' => 'yii2mod\comments\Module',
            'controllerMap' => [
                'manage' => [
                    'class' => 'yii2mod\comments\controllers\ManageController',
                    'layout' => '//admin',
                    'accessControlConfig' => [
                        'class' => 'yii\filters\AccessControl',
                        'rules' => [
                            [
                                'allow' => true,
                                'roles' => ['administrator'],
                            ],
                        ],
                    ],
                ],
            ],
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => '//admin',
        ],
        'agency' => [
            'class' => 'app\modules\agency\Module',
            'layout' => '//agency',
        ],
        'telegram' => [
            'class' => 'app\modules\telegram\Module',
        ],
        'manager' => [
            'class' => 'app\modules\manager\Module',
            'layout' => '//agency',
        ],
    ],
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
            //'forceCopy' => true,
            'bundles' => [
                /*'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],*/
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-purple',
                ],
            ],
        ],
        'view' => [
            'theme' => [
                //'basePath' => '@app/themes/freelancer',
                //'baseUrl' => '@web/themes/freelancer',
                'pathMap' => [
                    '@app/views' => '@app/themes/freelancer/views',
                    '@app/modules'=>'@app/themes/freelancer/modules',
                    '@app/components/views' => '@app/themes/freelancer/components',
                    '@app/widgets/views' => '@app/themes/freelancer/widgets',
                    '@dektrium/user/views' => '@app/views/user',
                    '@app/vendor/bupy7/yii2-pages/src/views' => '@app/views/bupy7',
                    
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'xK59rkkEv6NOWWJRIJS5Hg96hZb_dcSU',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => getenv('REDIS_DB'),
        ],
        'authManager'=>[
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['user'],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
//            'defaultRoles' => ['admin', 'viewer'],
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity',
                'httpOnly' => false,
                'domain' => '.' . $params['domain'],
            ],
            'loginUrl' => ['/user/security/login'],
        ],
        'authClientCollection' => [
            'class'   => \yii\authclient\Collection::className(),
            'clients' => [
                'facebook' => [
                    'class'        => 'dektrium\user\clients\Facebook',
                    'clientId'     => getenv('F_CLIENT_ID'),
                    'clientSecret' => getenv('F_CLIENT_SECRET'),
                ],
                /*'google' => [
                    'class'        => 'dektrium\user\clients\Google',
                    'clientId'     => getenv('G_CLIENT_ID'),
                    'clientSecret' => getenv('G_CLIENT_SECRET'),
                ],*/
                'vkontakte' => [
                    'class'        => 'dektrium\user\clients\VKontakte',
                    'clientId'     => getenv('VK_CLIENT_ID'),
                    'clientSecret' => getenv('VK_CLIENT_SECRET'),
                ]
                // here is the list of clients you want to use
                // you can read more in the "Available clients" section
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'transport' => [
                    'class' => 'Swift_SmtpTransport',
                    'host' => 'smtp.yandex.ru',
                    'username' => getenv('APP_MAILER_USERNAME'),
                    'password' => getenv('APP_MAILER_PASSWORD'),
                    'port' => 465,
                    'encryption' => 'ssl',
             ],
            'enableSwiftMailerLogging' =>false,
            'useFileTransport' => false,
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app'       => 'app.php',
                        'app/error' => 'error.php',
                        'language'  => 'language.php',
                    ],
                ],
                'yii2mod.comments' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii2mod/comments/messages',
                ],
           ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => ['ru', 'en', 'de'],
            'ignoreLanguageUrlPatterns' => [
                // route pattern => url pattern
                '#^user/security/auth#' => '#^(user/auth)#',
                '#^chat#' => '#^(chat/)#',
//                '#^api/#' => '#^api/#',
            ],//            'languages' => ['ru'],
//            'enableDefaultLanguageUrlCode' => false,
            'enableLanguagePersistence' => false,
            'enableLanguageDetection' => false,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //          'enableStrictParsing' => true,
            'rules' => [
                'pages/<page:[\w-]+>' => 'pages/default/index',
                '/agreement' => 'site/agreement',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['api/default']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['api/post']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['api/category']
                ],
                [
                    'pattern' => 'blog/<slug:[-a-zA-Z _0-9]+>',
                    'route' => 'blog/default/view',
                    'defaults' => ['slug' => null],
                    'suffix'=>'.html'
                ],
                [
                    'pattern' => '/blog/category/<slug:[-a-zA-Z _0-9]+>',
                    'route' => 'blog/default/category',
                    'defaults' => ['slug' => null],
                    'suffix'=>'/'
                ],
                [
                    'pattern' => '/blog/tag/<slug:[-a-zA-Z _0-9]+>',
                    'route' => 'blog/default/tag',
                    'defaults' => ['slug' => null],
                    'suffix'=>'/'
                ],
                [
                    'pattern' => '/blog/archive/<year:[0-9]+>/<month:[0-9]+>',
                    'route' => 'blog/default/archive',
                    'defaults' => ['year' => null,'month' => null],
                    'suffix'=>'/'
                ],
                
           ],
        ],
        'payeer' => [
            'class' => 'yarcode\payeer\Merchant',
            'shopId' => '1311220909',
            'secret' => getenv('APP_PAYEER_SECRET'),
            //'currency' => '<default shop currency>' // by default Merchant::CURRENCY_USD
        ],
        'notifications' => [
            'class' => 'webzop\notifications\Module',
            'channels' => [
                'screen' => [
                    'class' => 'webzop\notifications\channels\ScreenChannel',
                ],
                'email' => [
                    'class' => 'webzop\notifications\channels\EmailChannel',
                    'message' => [
                        'from' => 'example@email.com'
                    ],
                ],
                'web' => [
                    'class' => 'webzop\notifications\channels\WebChannel',
                    'enable' => true,                                       // OPTIONAL (default: true) enable/disable web channel
                    'config' => [
                        'serviceWorkerFilepath' => '/service-worker.js',    // OPTIONAL (default: /service-worker.js) is the service worker filename
                        'serviceWorkerScope' => '/app',                     // OPTIONAL (default: './' the service worker path) the scope of the service worker: https://developers.google.com/web/ilt/pwa/introduction-to-service-worker#registration_and_scope
                    ],
                    'auth' => [
                        'VAPID' => [
                            'subject' => 'mailto:me@website.com',           // can be a mailto: or your website address
                            'publicKey' => '~88 chars',                     // (recommended) uncompressed public key P-256 encoded in Base64-URL
                            'privateKey' => '~44 chars',                    // (recommended) in fact the secret multiplier of the private key encoded in Base64-URL
                            'pemFile' => 'path/to/pem',                     // if you have a PEM file and can link to it on your filesystem
                            'pem' => 'pemFileContent',                      // if you have a PEM file and want to hardcode its content
                            'reuseVAPIDHeaders' => true                     // OPTIONAL (default: true) you can reuse the same JWT token them for the same flush session to boost performance using
                        ],
                    ],
                ],
            ],
        ],


    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
