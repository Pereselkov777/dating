<?php
use Workerman\Worker;
use Workerman\Lib\Timer;
use PHPSocketIO\SocketIO;
//use app\models\Auction;

require(__DIR__ . '/vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

/*require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/config/console.php');
$application = new yii\console\Application($config);*/

// php worker.php start

/*$task = new Worker();
$task->onWorkerStart = function($task){
    $time_interval = 5;
    $timer_id = Timer::add($time_interval,
        function(){
            echo 'tick...'."\n";
            $endedAuctionExist = Auction::find()->where(['ended_at'=>2])->exists();
            if($endedAuctionExist){
                $auctions = Auction::find()->where(['ended_at'=>2])->all();
                foreach($auctions as $auction){
                    $auction->winner_id = 10;
                    $auction->save();
                }
            }
        }
    );
};*/

// listen port 2021 for socket.io client
// ssl context
if (getenv('CHAT_HOST') == 'https://chat.asia-bride.com:8443/') {
    $context = array(
        'ssl' => array(
            'local_cert' => '/etc/letsencrypt/live/chat.asia-bride.com/fullchain.pem',
            'local_pk' => '/etc/letsencrypt/live/chat.asia-bride.com/privkey.pem',
            'verify_peer' => false,
        )
    );
    $io = new SocketIO('8443', $context);
} else {

//$io->reusePort = true;

    $io = new SocketIO('8443');
}

//$fightHelper = new FightHelper();
//$io->attach()
//$io->on('connection', function($socket)use($io){
$users = [];
$chatSql = new \chat\ChatSql();
if (getenv('CHAT_HOST') == 'https://chat.asia-bride.com:8443/') {
    $timer = Timer::add(60, function () use (&$chatSql) {
        $chatSql->ping();
    });
}

$io->on('connection', function($client)use($io,  &$users, &$chatSql){
    echo 'connecting...'."\n";
    $client->on('connect_chat', function($data)use($io, $client,  &$users, &$chatSql){
        echo "\n".'user_id = ' . $data['user_id'] . 'socket_id = ' . $client->id."\n";
        print_r($data);
        try {
            $user_data  = $chatSql->getUserByAccessToken($data['access_token']);
            $user_id = $user_data['id'];
            $tel_id = $user_data['tel_id'];
        } catch (Exception $e) {
            print_r($e->getMessage());
            $user_id = 0;
        }
        $is_error = false;
        if ($user_id != $data['user_id']) {
            echo 'guest' . "\n";
            $is_error = true;
            $chatSql = null;
            $client->disconnect();
        } else {
            echo 'auth' . "\n";
        }
        if (!$is_error) {
            if (!isset($users[$data['user_id']])) {
                $users[$data['user_id']] = [
                    'user_id' => $data['user_id'],
                    'name' => $data['name'],
                    'tel_id' => $tel_id,
                    'parent_id' => 0,
                    'sockets' => [$client->id],
                ];
            } else {
                $users[$data['user_id']]['sockets'][] = $client->id;
            }
            $client->username = $data['name'];
            $client->room = $data['user_id'];
            $chatSql->updateLastActive($data['user_id']);
//        usernames[username] = username;
            $client->join($data['user_id']);
            echo 'test...';
            $client->emit('connect_chat', 1);
            $s = sprintf('Memory: %.2f Кб',
                    memory_get_usage(true) / 1024) . "\r\n";
            echo $s . "\n";
        }

//        callback();
        //$client->broadcast->to(GAME_SERVER)->emit('adduser', 2);
//        $io->emit('adduser', 2);
    });

    $client->on('disconnect', function($data)use($io, $client, &$users, &$chatSql){
        if (isset($client->room) && !empty($client->room)) {
            $user_id = (int)$client->room;
            $chatSql->updateLastActive($user_id);
        }
        if (isset($users[$client->room]['sockets'])) {
            if (($key = array_search($client->id, $users[$client->room]['sockets'])) !== false) {
                unset($users[$client->room]['sockets'][$key]);
            }
            if (empty($users[$client->room]['sockets'])) {
                unset($users[$client->room]);
            } else {
                $users[$client->room]['parent_id'] = 0;
            }
        }
    });

    $client->on('load_chat_list', function($data)use($io, $client, &$chatSql, &$users){
        echo 'load_chat_list' . "\n";
        var_dump($data);
        $user_id = $data['user_id'];
        $limit = isset($data['limit']) ? $data['limit'] : 20;
        $page = isset($data['page']) ? $data['page'] : 1;
        $offset = $page * $limit - $limit;
        try {
            $max_count = $chatSql->getCountLists($data['user_id']);
            //echo 'max_count = ' . $max_count . "\n";
            $max_page = ceil($max_count/$limit);
            $rows = $chatSql->getLists($data['user_id'], $limit, $offset);
            foreach ($rows as $key=>$row) {
                if (empty($row['photo'])) {
                    $rows[$key]['photo'] = '/img/no-user.jpg';
                }
                if (empty($row['first_name'])) {
                    $rows[$key]['first_name'] = $row['username'];
                }
            }
            $client->emit('load_chat_list', ['user_id' => $user_id, 'limit' => $limit, 'page' => $page, 'max_page' => $max_page, 'rows' => $rows]);
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    });

    $client->on('load_chat_messages', function($data)use($io, $client, &$chatSql){
        var_dump($data);
        $parent_id = $data['parent_id'];
        $limit = isset($data['limit']) ? $data['limit'] : 20;
        $page = isset($data['page']) ? (int)$data['page'] : 1;
        if ($page == 1) {
            if ($data['user_id'] > $data['to_id']) {
                $prefix = 'second';
            } else {
                $prefix = 'first';
            }
            echo 'update parent_id = ' . $parent_id . ' prefix = ' . $prefix;
            $chatSql->updateReading($parent_id, $data['to_id'], $prefix);
        }
        $offset = $page * $limit - $limit;
        try {
            $rows = $chatSql->getMessages($parent_id, $limit, $offset);
            $client->emit('load_chat_messages', ['parent_id' => $parent_id, 'limit' => $limit, 'page' => $page, 'rows' => $rows]);
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    });

    $client->on('get_chat_by_to_id', function($data)use($io, $client, &$users, &$chatSql){
        echo 'get_chat_by_to_id' . "\n";
        //var_dump($data);
        try {
            $list = $chatSql->getListByIds($data['user_id'], $data['to_id']);
            if ($list['id']) {
                $parent_id = (int)$list['id'];
                if (empty($list['photo'])) {
                    $list['photo'] = '/img/no-user.jpg';
                }
                if (empty($list['first_name'])) {
                    $list['first_name'] = $list['username'];
                }
                $avatar = $list['photo'];
                $name = $list['first_name'] . ' ' . $list['last_name'];
                $last_active = $list['last_active'];
            } else {
                $profile = $chatSql->getProfileByUserId($data['to_id']);
                $parent_id = 0;
                if (empty($profile['photo'])) {
                    $profile['photo'] = '/img/no-user.jpg';
                }
                if (empty($profile['first_name'])) {
                    $profile['first_name'] = $profile['username'];
                }
                $avatar = $profile['photo'];
                $name = $profile['first_name'] . ' ' . $profile['last_name'];
                $last_active = $profile['last_active'];
            }
            $users[$client->room]['parent_id'] = $parent_id;
            //print_r($users);
            if ($data['user_id'] > $data['to_id']) {
                $prefix = 'second';
            } else {
                $prefix = 'first';
            }
            $chatSql->updateReading($parent_id, $data['to_id'], $prefix);
            echo 'send get_chat_by_to_id parent_id = ' . $parent_id . "\n";
            $client->emit('get_chat_by_to_id', [
                'parent_id' => $parent_id,
                'user_id' => $data['user_id'],
                'to_id' => $data['to_id'],
                'avatar' => $avatar,
                'name' => $name,
                'last_active' => $last_active,
            ]);
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    });

    $client->on('chat_message', function($data)use($io, $client,  &$users, &$chatSql){
        var_dump($data);
        try {
            $profile = $chatSql->getProfileByUserId($data['user_id']);
            if ($profile['sex'] == 1) {
                $amount = 0.01;
                if ($profile['credit'] - $amount < 0) {
                    $client->emit('error_message', [
                       'code' => 100,
                       'message' => 'Not enough money to send the letter. $ '.$amount.' required',
                    ]);
                    return false;
                }
            }

            $list = $chatSql->getListByIds($data['user_id'], $data['to_id']);
            $is_read = 1;
            if ($list['id']) {
                $list_id = (int)$list['id'];
                if ($data['user_id'] > $data['to_id']) {
                    $first_id = $data['to_id'];
                    $second_id = $data['user_id'];
                    $first_count_unread = $list['first_count_unread'] + 1;
                    $second_count_unread = $list['second_count_unread'];
                    //print_r($users);
                    if (isset($users[$first_id]['parent_id']) && $users[$first_id]['parent_id'] == $list_id) {
                        echo 'parent_id = ' . $users[$first_id]['parent_id'] . ' list_id = ' . $list_id . "\n";
                        $is_first_count_unread = 0;
                        $is_read = 1;
                    } else {
                        $is_first_count_unread = 1;
                        $is_read = 0;
                    }
                    $is_second_count_unread = 0;
                } else {
                    $second_id = $data['to_id'];
                    $first_id = $data['user_id'];
                    $first_count_unread = $list['first_count_unread'];
                    $second_count_unread = $list['second_count_unread'] + 1;
                   // print_r($users);
                    echo 'parent_id = ' . $users[$first_id]['parent_id'] . ' list_id = ' . $list_id . "\n";
                    if (isset($users[$second_id]['parent_id']) && $users[$second_id]['parent_id'] == $list_id) {
                        $is_second_count_unread = 0;
                        $is_read = 1;
                    } else {
                        $is_second_count_unread = 1;
                        $is_read = 0;
                    }
                    $is_first_count_unread = 0;
                }
                $chatSql->updateList($list_id, $data['user_id'],$data['message'], $is_first_count_unread, $is_second_count_unread);
            } else {
                $is_read = 0;
                if ($data['user_id'] > $data['to_id']) {
                    $first_id = $data['to_id'];
                    $second_id = $data['user_id'];
                    $first_count_unread = 1;
                    $second_count_unread = 0;
                } else {
                    $second_id = $data['to_id'];
                    $first_id = $data['user_id'];
                    $first_count_unread = 0;
                    $second_count_unread = 1;
                }
                $list_id = $chatSql->addList($data['user_id'], $data['to_id'], $data['user_id'],$data['message']);
            }

            $message_id = $chatSql->addMessage($list_id, $data['user_id'], $data['to_id'], $data['message'], $is_read);
            if ($profile['sex'] == 1) {
                $profile['credit'] = $profile['credit'] - $amount;
                $amount_agency = $amount * 0.3;
                $chatSql->spentCost($data['user_id'], $data['to_id'], $message_id, $amount, $amount_agency);
            }

            $client->emit('chat_message', [
                'id' => $message_id,
                'parent_id' => (int)$list_id,
                'user_id' => (int)$data['user_id'],
                'from_id' => (int)$data['user_id'],
                'to_id' => (int)$data['to_id'],
                'first_id' => (int)$first_id,
                'second_id' => (int)$second_id,
                'first_name' => $profile['first_name'],
                'last_name' => $profile['last_name'],
                'photo' => $profile['photo'],
                'last_active' => $profile['last_active'],
                'message' => $data['message'],
                'first_count_unread' => $first_count_unread,
                'second_count_unread' => $second_count_unread,
                'is_read' => $is_read,
                'credit' => round($profile['credit'], 2),
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $client->broadcast->to($data['user_id'])->emit('chat_message', [
                'id' => $message_id,
                'parent_id' => (int)$list_id,
                'user_id' => (int)$data['user_id'],
                'from_id' => (int)$data['user_id'],
                'to_id' => (int)$data['to_id'],
                'first_id' => (int)$first_id,
                'second_id' => (int)$second_id,
                'first_name' => $profile['first_name'],
                'last_name' => $profile['last_name'],
                'photo' => $profile['photo'],
                'last_active' => $profile['last_active'],
                'message' => $data['message'],
                'first_count_unread' => $first_count_unread,
                'second_count_unread' => $second_count_unread,
                'is_read' => $is_read,
                'credit' => round($profile['credit'], 2),
                'created_at' => date('Y-m-d H:i:s')
            ]);


            if (!$is_read) {
                $sender_name = $profile['first_name'] . ' ' . $profile['last_name'];
                $chatSql->sendTelegram($data['to_id'], $sender_name, $data['message']);
                $chatSql->sendPush($data['to_id'], $data['user_id'], $sender_name, $data['message']);
            } else {
                echo 'is_reading' . "\n";
            }

            $client->broadcast->to($data['to_id'])->emit('chat_message', [
                'id' => $message_id,
                'parent_id' => (int)$list_id,
                'user_id' => (int)$data['to_id'],
                'from_id' => (int)$data['user_id'],
                'to_id' => (int)$data['to_id'],
                'first_id' => (int)$first_id,
                'second_id' => (int)$second_id,
                'first_name' => $profile['first_name'],
                'last_name' => $profile['last_name'],
                'photo' => $profile['photo'],
                'last_active' => $profile['last_active'],
                'message' => $data['message'],
                'first_count_unread' => $first_count_unread,
                'second_count_id' => $second_count_unread,
                'is_read' => $is_read,
                'created_at' => date('Y-m-d H:i:s')
            ]);

            $chatSql->updateLastActive($data['user_id']);
            /*  $sock_ids = (isset($users[$data['to_id']]['sockets']) ? $users[$data['to_id']]['sockets'] : []);
              foreach ( $sock_ids as $sock_id) {
                  $io->to($sock_id, [
                      'id' => $message_id,
                      'parent_id' => $list_id,
                      'user_id' => $data['to_id'],
                      'from_id' => $data['user_id'],
                      'to_id' => $data['to_id'],
                      'message' => $data['message'],
                      'created_at' => time()
                  ]);
              }*/

        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    });

    $client->on('stop_server', function($data)use($io, $client){
        Worker::stopAll();
    });
});

Worker::runAll();
echo 'finish' . "\n";
$chatSql = null;