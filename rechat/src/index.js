import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";
import { sayHello } from './Helpers';

ReactDOM.render(<App />, document.getElementById("root"));
